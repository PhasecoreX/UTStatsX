<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240608013111 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE event_entity (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, match_info_id INTEGER NOT NULL, match_player_id INTEGER NOT NULL, time INTEGER NOT NULL, type VARCHAR(255) NOT NULL, entity_name VARCHAR(255) NOT NULL, CONSTRAINT FK_3CB1333A2A2D9E97 FOREIGN KEY (match_info_id) REFERENCES match_info (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_3CB1333A8057B974 FOREIGN KEY (match_player_id) REFERENCES match_player (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_3CB1333A2A2D9E97 ON event_entity (match_info_id)');
        $this->addSql('CREATE INDEX IDX_3CB1333A8057B974 ON event_entity (match_player_id)');
        $this->addSql('CREATE TABLE event_kill (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, match_info_id INTEGER NOT NULL, match_player_id INTEGER NOT NULL, victim_id INTEGER NOT NULL, time INTEGER NOT NULL, match_player_weapon VARCHAR(255) NOT NULL, victim_weapon VARCHAR(255) NOT NULL, damage_type VARCHAR(255) NOT NULL, player_score SMALLINT NOT NULL, player_team_score SMALLINT NOT NULL, CONSTRAINT FK_18E21AC22A2D9E97 FOREIGN KEY (match_info_id) REFERENCES match_info (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_18E21AC28057B974 FOREIGN KEY (match_player_id) REFERENCES match_player (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_18E21AC244972A0E FOREIGN KEY (victim_id) REFERENCES match_player (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_18E21AC22A2D9E97 ON event_kill (match_info_id)');
        $this->addSql('CREATE INDEX IDX_18E21AC28057B974 ON event_kill (match_player_id)');
        $this->addSql('CREATE INDEX IDX_18E21AC244972A0E ON event_kill (victim_id)');
        $this->addSql('CREATE TABLE event_kill_attribute (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, event_kill_id INTEGER NOT NULL, type VARCHAR(255) NOT NULL, CONSTRAINT FK_6786A820A43FE89 FOREIGN KEY (event_kill_id) REFERENCES event_kill (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_6786A820A43FE89 ON event_kill_attribute (event_kill_id)');
        $this->addSql('CREATE TABLE event_player (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, match_info_id INTEGER NOT NULL, match_player_id INTEGER NOT NULL, time INTEGER NOT NULL, type VARCHAR(255) NOT NULL, target_id SMALLINT DEFAULT NULL, target_str VARCHAR(255) DEFAULT NULL, player_score SMALLINT NOT NULL, player_team_score SMALLINT NOT NULL, CONSTRAINT FK_AA800D372A2D9E97 FOREIGN KEY (match_info_id) REFERENCES match_info (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_AA800D378057B974 FOREIGN KEY (match_player_id) REFERENCES match_player (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_AA800D372A2D9E97 ON event_player (match_info_id)');
        $this->addSql('CREATE INDEX IDX_AA800D378057B974 ON event_player (match_player_id)');
        $this->addSql('CREATE TABLE event_suicide (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, match_info_id INTEGER NOT NULL, match_player_id INTEGER NOT NULL, time INTEGER NOT NULL, match_player_weapon VARCHAR(255) NOT NULL, damage_type VARCHAR(255) NOT NULL, player_score SMALLINT NOT NULL, player_team_score SMALLINT NOT NULL, CONSTRAINT FK_58AAA61F2A2D9E97 FOREIGN KEY (match_info_id) REFERENCES match_info (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_58AAA61F8057B974 FOREIGN KEY (match_player_id) REFERENCES match_player (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_58AAA61F2A2D9E97 ON event_suicide (match_info_id)');
        $this->addSql('CREATE INDEX IDX_58AAA61F8057B974 ON event_suicide (match_player_id)');
        $this->addSql('CREATE TABLE event_team (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, match_info_id INTEGER NOT NULL, time INTEGER NOT NULL, type VARCHAR(255) NOT NULL, team SMALLINT NOT NULL, score SMALLINT NOT NULL, CONSTRAINT FK_DB2BEAB42A2D9E97 FOREIGN KEY (match_info_id) REFERENCES match_info (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_DB2BEAB42A2D9E97 ON event_team (match_info_id)');
        $this->addSql('CREATE TABLE game_type (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, scoreboard VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE INDEX gametype_slug_idx ON game_type (slug)');
        $this->addSql('CREATE TABLE map (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, hash VARCHAR(32) NOT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, author VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX map_hash_idx ON map (hash)');
        $this->addSql('CREATE TABLE match_info (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, server_id INTEGER NOT NULL, game_type_id INTEGER NOT NULL, map_id INTEGER NOT NULL, hash VARCHAR(32) NOT NULL, log_file_hash VARCHAR(32) NOT NULL, log_file_name VARCHAR(255) NOT NULL, dirty BOOLEAN NOT NULL, date DATETIME NOT NULL, timezone VARCHAR(255) NOT NULL, time_factor DOUBLE PRECISION NOT NULL, start_time INTEGER NOT NULL, end_time INTEGER NOT NULL, server_version VARCHAR(8) DEFAULT NULL, frag_limit INTEGER NOT NULL, time_limit INTEGER NOT NULL, team_count SMALLINT NOT NULL, goal_team_score INTEGER NOT NULL, assault_game_code VARCHAR(8) DEFAULT NULL, assault_attacker SMALLINT DEFAULT NULL, assault_defender SMALLINT DEFAULT NULL, match_number INTEGER NOT NULL, CONSTRAINT FK_AAE680251844E6B7 FOREIGN KEY (server_id) REFERENCES server (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_AAE68025508EF3BC FOREIGN KEY (game_type_id) REFERENCES game_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_AAE6802553C55F64 FOREIGN KEY (map_id) REFERENCES map (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_AAE680251844E6B7 ON match_info (server_id)');
        $this->addSql('CREATE INDEX IDX_AAE68025508EF3BC ON match_info (game_type_id)');
        $this->addSql('CREATE INDEX IDX_AAE6802553C55F64 ON match_info (map_id)');
        $this->addSql('CREATE INDEX match_info_hash_idx ON match_info (hash)');
        $this->addSql('CREATE INDEX match_info_log_file_hash_idx ON match_info (log_file_hash)');
        $this->addSql('CREATE TABLE match_mutator (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, match_info_id INTEGER NOT NULL, mutator_id INTEGER NOT NULL, parse_order SMALLINT NOT NULL, CONSTRAINT FK_DB1A40DB2A2D9E97 FOREIGN KEY (match_info_id) REFERENCES match_info (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_DB1A40DBAB9B6269 FOREIGN KEY (mutator_id) REFERENCES mutator (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_DB1A40DB2A2D9E97 ON match_mutator (match_info_id)');
        $this->addSql('CREATE INDEX IDX_DB1A40DBAB9B6269 ON match_mutator (mutator_id)');
        $this->addSql('CREATE TABLE match_player (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, match_info_id INTEGER NOT NULL, player_id INTEGER NOT NULL, team SMALLINT NOT NULL, score INTEGER NOT NULL, kills INTEGER NOT NULL, deaths INTEGER NOT NULL, suicides INTEGER NOT NULL, team_kills INTEGER NOT NULL, team_deaths INTEGER NOT NULL, won_match BOOLEAN NOT NULL, time INTEGER NOT NULL, CONSTRAINT FK_397683642A2D9E97 FOREIGN KEY (match_info_id) REFERENCES match_info (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_3976836499E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_397683642A2D9E97 ON match_player (match_info_id)');
        $this->addSql('CREATE INDEX IDX_3976836499E6F5DF ON match_player (player_id)');
        $this->addSql('CREATE TABLE match_player_pickup_total (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, match_player_id INTEGER NOT NULL, pickup_id INTEGER NOT NULL, total INTEGER NOT NULL, CONSTRAINT FK_60D4F2A18057B974 FOREIGN KEY (match_player_id) REFERENCES match_player (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_60D4F2A1C26E160B FOREIGN KEY (pickup_id) REFERENCES pickup (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_60D4F2A18057B974 ON match_player_pickup_total (match_player_id)');
        $this->addSql('CREATE INDEX IDX_60D4F2A1C26E160B ON match_player_pickup_total (pickup_id)');
        $this->addSql('CREATE TABLE mutator (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, display_name VARCHAR(255) NOT NULL, hidden BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX mutator_name_idx ON mutator (name)');
        $this->addSql('CREATE TABLE pickup (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, display_name VARCHAR(255) NOT NULL, hidden BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX pickup_name_idx ON pickup (name)');
        $this->addSql('CREATE TABLE player (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, last_match_date DATETIME NOT NULL, face VARCHAR(255) DEFAULT NULL, voice VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE INDEX player_name_idx ON player (name)');
        $this->addSql('CREATE TABLE server (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(255) DEFAULT NULL, admin_name VARCHAR(255) DEFAULT NULL, admin_email VARCHAR(255) DEFAULT NULL, version VARCHAR(255) DEFAULT NULL, last_updated DATETIME NOT NULL)');
        $this->addSql('CREATE INDEX server_name_idx ON server (name)');
        $this->addSql('CREATE TABLE team_score (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, match_info_id INTEGER NOT NULL, team SMALLINT NOT NULL, score INTEGER NOT NULL, rank SMALLINT NOT NULL, CONSTRAINT FK_F59F7E112A2D9E97 FOREIGN KEY (match_info_id) REFERENCES match_info (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_F59F7E112A2D9E97 ON team_score (match_info_id)');
        $this->addSql('CREATE TABLE messenger_messages (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, body CLOB NOT NULL, headers CLOB NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , available_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , delivered_at DATETIME DEFAULT NULL --(DC2Type:datetime_immutable)
        )');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE event_entity');
        $this->addSql('DROP TABLE event_kill');
        $this->addSql('DROP TABLE event_kill_attribute');
        $this->addSql('DROP TABLE event_player');
        $this->addSql('DROP TABLE event_suicide');
        $this->addSql('DROP TABLE event_team');
        $this->addSql('DROP TABLE game_type');
        $this->addSql('DROP TABLE map');
        $this->addSql('DROP TABLE match_info');
        $this->addSql('DROP TABLE match_mutator');
        $this->addSql('DROP TABLE match_player');
        $this->addSql('DROP TABLE match_player_pickup_total');
        $this->addSql('DROP TABLE mutator');
        $this->addSql('DROP TABLE pickup');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE server');
        $this->addSql('DROP TABLE team_score');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
