<?php

namespace App\Parse;

class PlayerInfo
{
    public $name;
    public $currentId;
    public $face;
    public $voice;
    public $ip;
    public $bot = false;
    public $titan = false;
    public $pingTotal = 0;
    public $pingCount = 0;

    public function ping($ping)
    {
        $ping = intval($ping);

        if ($ping >= 0 && $ping <= 10000) {
            $this->pingTotal += $ping;
            $this->pingCount++;
        }
    }
}
