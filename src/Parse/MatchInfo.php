<?php

namespace App\Parse;

class MatchInfo
{
    // Server
    public string $serverName;

    public string $shortName;

    public string $adminName;

    public string $adminEmail;

    // Map
    public string $mapName;

    public string $mapTitle;

    public string $mapAuthor;

    // Match
    public \DateTimeImmutable $matchDate;

    public string $timezone;

    public float $timeFactor;

    public ?string $serverVersion = null;

    public string $gameName;

    public string $gameClass;

    public $mutators = array();

    public int $fragLimit = 0;

    public int $timeLimit = 0;

    public int $teamCount = 0;

    public int $goalTeamScore = 0;

    public ?string $assaultGameCode = null;

    public ?int $assaultAttacker = null;

    public ?int $assaultDefender = null;

    public int $matchNumber = 1;

    public function updateTeamCount($team)
    {
        $this->teamCount = max($this->teamCount, intval($team) + 1);
    }

    public function getMatchHash()
    {
        $context = hash_init('murmur3f');
        hash_update($context, $this->matchDate->format('Uv'));
        hash_update($context, '|');
        hash_update($context, $this->serverName);
        hash_update($context, '|');
        hash_update($context, $this->getMapHash());
        return hash_final($context);
    }

    public function getMapHash()
    {
        $context = hash_init('murmur3f');
        hash_update($context, $this->mapName);
        hash_update($context, '|');
        hash_update($context, $this->mapTitle);
        hash_update($context, '|');
        hash_update($context, $this->mapAuthor);
        return hash_final($context);
    }
}
