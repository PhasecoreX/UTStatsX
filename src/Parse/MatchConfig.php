<?php

namespace App\Parse;

class MatchConfig
{
    public $killsToTeamScore = false;
    public $ignoreTeamKillScoreDecrement = false;
    public $lastManStanding = false;
    public $overwritePlayerScores = false;
    public $ignoreEndWeaponStats = false;
    public $flagBasedGametype = false;
    public $smartctfEnabled = false;
    public $killingMonstersAffectsKillCount = false;
}
