<?php

namespace App\Parse\Event;

use App\Enum\Weapon;
use App\Parse\Player;

class Suicide
{
    public int $time;

    public int $player;

    public int $playerTeam;

    public string $playerWeapon;

    public string $damageType;

    public int $playerScore = 0;

    public int $playerTeamScore = 0;

    public $attributes = array();

    public static function new(Player $player, $playerWeapon, $damageType, $playerScore, $playerTeamScore)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->playerWeapon = self::cleanWeaponName($playerWeapon);
        $instance->damageType = ucwords($damageType);
        $instance->playerScore = intval($playerScore);
        $instance->playerTeamScore = intval($playerTeamScore);
        return $instance;
    }

    private static function cleanWeaponName(string $weapon)
    {
        $cleanWeapon = Weapon::getWeapon($weapon);
        if (isset($cleanWeapon)) {
            return $cleanWeapon->value;
        }
        return ucwords($weapon);
    }
}
