<?php

namespace App\Parse\Event;

use App\Enum\DamageType;
use App\Enum\KillAttribute;
use App\Enum\Weapon;
use App\Parse\Exception\LogParseError;
use App\Parse\Player;

class Kill
{
    public int $time;

    public int $player;

    public int $playerTeam;

    public string $playerWeapon;

    public int $victim;

    public int $victimTeam;

    public string $victimWeapon;

    public string $damageType;

    public int $playerScore = 0;

    public int $playerTeamScore = 0;

    public $attributes = array();

    public static function new(Player $player, $playerWeapon, Player $victim, $victimWeapon, $damageType, $playerScore, $playerTeamScore, $attributes = null)
    {
        if ($player->id == $victim->id) {
            throw new LogParseError("{$player->getCurrentTime()}: Can't create new Kill event where killer and victim have same ID ({$player->id}).");
        }

        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->playerWeapon = self::correctKillerWeaponName($player, $playerWeapon, $damageType);
        $instance->victim = intval($victim->id);
        $instance->victimTeam = intval($victim->team);
        $instance->victimWeapon = self::cleanWeaponName($victimWeapon);
        $instance->damageType = ucwords($damageType);
        $instance->playerScore = intval($playerScore);
        $instance->playerTeamScore = intval($playerTeamScore);
        if (isset($attributes)) {
            $instance->attributes = $attributes;
        }
        return $instance;
    }

    public function addAttribute(KillAttribute $attribute)
    {
        $this->attributes[] = $attribute;
    }

    private static function correctKillerWeaponName(Player $player, string $weapon, string $damageType)
    {
        // First, clean up the weapon name
        if (strtolower($weapon) == 'none') {
            $weaponString = self::cleanWeaponName($player->lastWeaponHeld);
        } else {
            $weaponString = self::cleanWeaponName($weapon);
        }

        // Next, get the damage type
        $damageType = DamageType::getDamageType($damageType);
        if (!isset($damageType)) {
            // If we don't know what this damage type is, just return the cleaned up weapon name
            return $weaponString;
        }

        // WhoPushedMe mutator
        if (DamageType::Pushed == $damageType) {
            return 'Push';
        }

        // Relic of Vengeance
        if (DamageType::Eradicated == $damageType && isset($player->previousActiveItems['Relic of Vengeance'])) {
            return 'Vengeance (Relic)';
        }

        // For titans, we rename the weapon to a body part (or rock) that makes sense
        if ($player->info->titan) {
            switch ($damageType) {
                case DamageType::Crushed:
                    return "Rock ({$damageType->value})";
                case DamageType::Hacked:
                    return "Claw ({$damageType->value})";
                case DamageType::Stomped:
                    return "Foot ({$damageType->value})";
                case DamageType::Gibbed:
                    return "Body ({$damageType->value})";
                default:
                    break;
            }
        }

        // Player spawn killing another player
        if (DamageType::Gibbed == $damageType) {
            return 'Spawn';
        }

        // When a player is killed, the weapon that the killer is holding AT THAT TIME is what is logged.
        //
        // For example, when a player fires a redeemer (primary) and switches to another weapon,
        // the redeemer kills will be logged with whatever weapon was switched to.
        // Likewise, this is why 'None' shows up if a player kills another when they are dead already
        // (fire a ripper blade, get killed, ripper blade then kills another player, for example)
        $knownWeapon = Weapon::getWeapon($weaponString);
        if (isset($knownWeapon)) {
            // At this point, this is a weapon and a damage type we know about, so lets try to fix the non-instant shot ones
            switch ($damageType) {
                case DamageType::AltJolted:
                    return Weapon::ShockRifle->value;
                case DamageType::Corroded:
                    return Weapon::GESBioRifle->value;
                case DamageType::FlakDeath:
                    return Weapon::FlakCannon->value;
                case DamageType::GrenadeDeath:
                    return Weapon::RocketLauncher->value;
                case DamageType::Pulsed:
                    return Weapon::PulseGun->value;
                case DamageType::RedeemerDeath:
                    return Weapon::Redeemer->value;
                case DamageType::RedeemerSplashDamage:
                    return Weapon::Redeemer->value;
                case DamageType::RipperAltDeath:
                    return Weapon::Ripper->value;
                case DamageType::RocketDeath:
                    return Weapon::RocketLauncher->value;
                    // TODO shredded and decapitated, based on map weapons
                default:
                    break;
            }
        }

        // If no further corrections were needed, just return the cleaned up weapon name
        return $weaponString;
    }

    private static function cleanWeaponName(string $weapon)
    {
        $cleanWeapon = Weapon::getWeapon($weapon);
        if (isset($cleanWeapon)) {
            return $cleanWeapon->value;
        }
        return ucwords($weapon);
    }
}
