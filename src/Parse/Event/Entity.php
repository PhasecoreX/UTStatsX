<?php

namespace App\Parse\Event;

use App\Enum\EventEntityType;

class Entity
{
    public int $time;

    public EventEntityType $type;

    public int $player;

    public int $playerTeam;

    public string $entityName;

    public int $playerScore = 0;

    public int $playerTeamScore = 0;

    public static function playerKillEntity($player, $entityName, $modifyKillStat = false, $playerScore = null, $playerTeamScore = null)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        if ($modifyKillStat) {
            $instance->type = EventEntityType::PlayerKillEntity;
            $instance->playerScore = 1;
        } else {
            $instance->type = EventEntityType::PlayerKillIgnoredEntity;
        }
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->entityName = $entityName;
        if (isset($playerScore)) {
            $instance->playerScore = intval($playerScore);
        }
        if (isset($playerTeamScore)) {
            $instance->playerTeamScore = intval($playerTeamScore);
        }
        return $instance;
    }

    public static function entityKillPlayer($player, $entityName)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventEntityType::EntityKillPlayer;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->entityName = $entityName;
        return $instance;
    }
}
