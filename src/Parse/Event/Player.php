<?php

namespace App\Parse\Event;

use App\Enum\EventPlayerType;

class Player
{
    public int $time;

    public EventPlayerType $type;

    public int $player;

    public int $playerTeam;

    public ?int $targetId = null;

    public ?string $targetStr = null;

    public int $playerScore = 0;

    public int $playerTeamScore = 0;

    public static function connect($player)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::Connect;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        return $instance;
    }

    public static function teamChange($player)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::TeamChange;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        return $instance;
    }

    public static function disconnect($player)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::Disconnect;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        return $instance;
    }

    public static function flagCapture($player, $flag, $playerScore, $playerTeamScore)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::FlagCapture;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->targetId = intval($flag);
        $instance->playerScore = intval($playerScore);
        $instance->playerTeamScore = intval($playerTeamScore);
        return $instance;
    }

    public static function flagPickup($player, $flag)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::FlagPickup;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->targetId = intval($flag);
        return $instance;
    }

    public static function flagReturn($player, $flag)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::FlagReturn;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->targetId = intval($flag);
        return $instance;
    }

    public static function flagTake($player, $flag)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::FlagTake;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->targetId = intval($flag);
        return $instance;
    }

    public static function flagDrop($player, $flag)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::FlagDrop;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->targetId = intval($flag);
        return $instance;
    }

    public static function assaultObjective($player, $objective, $playerScore, $playerTeamScore)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::AssaultObjective;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->targetId = intval($objective);
        $instance->playerScore = intval($playerScore);
        $instance->playerTeamScore = intval($playerTeamScore);
        return $instance;
    }

    public static function domCapture($player, $controlPoint)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::DomCapture;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->targetStr = $controlPoint;
        return $instance;
    }

    public static function domScore($player, $playerScore)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::DomScoreUpdate;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->playerScore = intval($playerScore);
        return $instance;
    }

    public static function lmsOut($player)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::LmsOut;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        return $instance;
    }

    public static function jbCaptureTeam($player, $capturedTeam)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::JbCaptureTeam;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->targetId = intval($capturedTeam);
        return $instance;
    }

    public static function jbReleaseTeam($player, $releasedTeam)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::JbReleaseTeam;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->targetId = intval($releasedTeam);
        return $instance;
    }

    public static function jbOnWinningTeam($player, $playerScore)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::JbOnWinningTeam;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->playerScore = intval($playerScore);
        return $instance;
    }

    public static function jbArenaWin($player)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::JbArenaWin;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        return $instance;
    }

    public static function jbArenaTie($player)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::JbArenaTie;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        return $instance;
    }

    public static function jbArenaLose($player)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::JbArenaLose;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        return $instance;
    }

    public static function scoreCorrection($player, $playerScore)
    {
        $instance = new self();
        $instance->time = intval($player->getCurrentTime());
        $instance->type = EventPlayerType::ScoreCorrection;
        $instance->player = intval($player->id);
        $instance->playerTeam = intval($player->team);
        $instance->playerScore = intval($playerScore);
        return $instance;
    }
}
