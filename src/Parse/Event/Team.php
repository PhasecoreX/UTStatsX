<?php

namespace App\Parse\Event;

use App\Enum\EventTeamType;

class Team
{
    public int $time;

    public EventTeamType $type;

    public int $team;

    public int $score = 0;

    public static function domScore($time, $team, $score)
    {
        $instance = new self();
        $instance->time = intval($time);
        $instance->type = EventTeamType::DomScoreUpdate;
        $instance->team = intval($team);
        $instance->score = intval($score);
        return $instance;
    }

    public static function teamCapture($time, $team, $score)
    {
        $instance = new self();
        $instance->time = intval($time);
        $instance->type = EventTeamType::TeamCapture;
        $instance->team = intval($team);
        $instance->score = intval($score);
        return $instance;
    }

    public static function teamLmsEndTotal($time, $team, $score)
    {
        $instance = new self();
        $instance->time = intval($time);
        $instance->type = EventTeamType::TeamLmsEndTotal;
        $instance->team = intval($team);
        $instance->score = intval($score);
        return $instance;
    }

    public static function scoreCorrection($time, $team, $score)
    {
        $instance = new self();
        $instance->time = intval($time);
        $instance->type = EventTeamType::ScoreCorrection;
        $instance->team = intval($team);
        $instance->score = intval($score);
        return $instance;
    }
}
