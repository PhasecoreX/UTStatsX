<?php

namespace App\Parse;

use App\Enum\EventEntityType;
use App\Enum\KillAttribute;
use App\Enum\KillType;
use App\Parse\Exception\LogParseError;

class Player
{
    // Static info
    public int $id;

    public PlayerInfo $info;

    // Dynamic info
    public bool $connected = false;

    public int $startTime;

    public int $endTime;

    public int $lmsOutTime;

    public int $team = -1;

    public int $lives = 0;

    public string $lastWeaponHeld = 'None';

    // Events
    public $pickups = array();

    public $activate = array();

    public $activeItems = array();

    public $previousActiveItems = array();

    public $multi = array();

    public $spree = array();

    // End game weapon stats
    public $shotCount = array();

    public $hitCount = array();

    public $damageGiven = array();

    public $accuracy = array();

    // Temp variables
    private int $flagStartHold = 0;

    private int $flagHoldTime = 0;

    private $flagsHeld = array();

    private int $spreeStartTime;

    private int $spreeKillCount = 0;

    private int $lastKillTime;

    private int $multiKillCount = 0;

    private int $lastEntityDeathTime;

    private bool $teamCapturedSoSkipNextSuicide = false;

    private int $skippedSuicides = 0;

    private int $flagCoverTime;

    private int $flagCoverVictim;

    private int $flagReturnTime;

    // Match object
    private MatchData $match;

    // Previous player info
    public Player $previousPlayerConnection;

    public function __construct()
    {
        $this->info = new PlayerInfo();
    }

    public static function new($match, $id)
    {
        $instance = new self();
        $instance->match = $match;
        $instance->id = intval($id);
        $instance->info->currentId = intval($id);
        $instance->startTime = $instance->getCurrentTime();
        if ($match->config->lastManStanding) {  // Set LMS lives
            $instance->lives = $match->info->fragLimit;
        }
        return $instance;
    }

    public static function dupe($id, $oldPlayer)
    {
        $instance = Player::new($oldPlayer->match, $id);
        $instance->info = $oldPlayer->info;
        $instance->info->currentId = intval($id);
        $instance->previousPlayerConnection = $oldPlayer;
        return $instance;
    }

    public function linkExistingPlayer($oldPlayer)
    {
        $currentId = $this->id;
        $this->info = $oldPlayer->info;
        $this->info->currentId = $currentId;
        $this->previousPlayerConnection = $oldPlayer;
    }

    public function hasCurrentPlayerId()
    {
        return $this->id == $this->info->currentId;
    }

    public function valid()
    {
        if (!isset($this->match) || !isset($this->id) || !isset($this->startTime)) {
            throw new LogParseError('Player object not constructed correctly');
        }
        if (!is_numeric($this->id)) {
            $this->match->logWarning("Invalid player ID '{$this->id}'");
            return false;
        }
        return true;
    }

    public function isConnected()
    {
        return $this->valid() && $this->connected;
    }

    public function validConnected($action)
    {
        if (!$this->valid()) {
            return false;
        }
        if (!$this->connected && (!isset($this->endTime) || $this->getCurrentTime() - $this->endTime > 1)) {
            $this->match->logWarning("Player {$this->id} tried to {$action}, but is not connected at this time");
            return false;
        }
        return true;
    }

    public function getCurrentTime()
    {
        return $this->match->currentTime;
    }

    private function onATeam()
    {
        return $this->team >= 0 && $this->team <= 3;
    }

    public function rename($newName)
    {
        if (!$this->valid()) {
            return;
        }

        $this->info->name = $newName;
        $this->connected = true;
    }

    public function setTeam($newTeam)
    {
        if (!$this->valid()) {
            return;
        }

        if (!$this->match->isTeamGame()) {
            return;
        }

        $team = intval($newTeam);
        if ($team < 0 && $team > 3) {
            $this->match->logWarning("Player {$this->id} tried switching to team {$newTeam}, which isn't supported");
            return;
        }

        if ($this->team == $team) {
            return;
        }

        if ($this->team < 0) {
            // Initial join team change
            $this->team = $team;
            $this->connected = true;
            $this->match->info->updateTeamCount($team);
            $this->match->updatePlayerConnectEvent($this);
            return;
        }

        // Mid game team change
        $this->endCleanup();
        $newPlayer = Player::dupe($this->id, $this);
        $newPlayer->setTeam($team);
        $this->match->setPlayer($this->id, $newPlayer);
        $this->match->insertPlayerEvent(Event\Player::teamChange($newPlayer));
    }

    public function disconnect()
    {
        if (!$this->isConnected()) {
            return;
        }

        $this->connected = false;
        $this->endCleanup();

        $this->match->insertPlayerEvent(Event\Player::disconnect($this));
    }

    public function ping($ping)
    {
        if (!$this->isConnected()) {
            return;
        }

        $this->info->ping($ping);
    }

    public function itemPickup($item)
    {
        if (!$this->validConnected("pickup item '{$item}'")) {
            return;
        }

        if (!isset($this->pickups[$item])) {
            $this->pickups[$item] = 0;
        }
        $this->pickups[$item]++;
    }

    public function itemActivate($item)
    {
        if (!$this->validConnected("activate item '{$item}'")) {
            return;
        }

        if (!isset($this->activate[$item])) {
            $this->activate[$item] = 0;
        }
        $this->activate[$item]++;
        $this->activeItems[$item] = true;
    }

    public function itemDeactivate($item)
    {
        if (!$this->validConnected("deactivate item '{$item}'")) {
            return;
        }

        unset($this->activeItems[$item]);
    }

    public function kill($killerWeapon, $teamKill, $victimId, $victimWeapon, $damageType)
    {
        if (!$this->match->isTeamGame()) {
            $teamKill = false;
        }

        if ($this->handleLevelTitanKill($killerWeapon, $teamKill, $victimId, $victimWeapon, $damageType)) {
            return;
        }

        if ($this->handlePlayerTitanKillJoin($killerWeapon, $teamKill, $victimId, $victimWeapon, $damageType)) {
            return;
        }

        if (!$this->validConnected("kill player {$victimId}")) {
            return;
        }

        $victim = $this->match->getPlayer($victimId);
        $victim = $victim->handleLevelTitanDeath($victimWeapon, $teamKill, $this->id);
        $victim = $victim->handlePlayerTitanDeathJoin($victimWeapon, $teamKill, $this->id);

        if ($this->id == $victim->id) {
            throw new LogParseError("{$this->getCurrentTime()}: Player {$this->id} can't kill() themselves, use suicide() for that.");
        }
        $victim->validConnected("be killed by player {$this->id}");

        $playerScore = 0;
        $playerTeamScore = 0;
        $attributes = array();

        if ($this->match->isTeamGame() && $this->onATeam() && $this->team == $victim->team) {
            if (!$teamKill) {
                $this->match->logWarning("Log expected normal kill between player {$this->id} team {$this->team} and player {$victim->id} team {$victim->team}, parser thinks it should be a teamkill instead");
            }

            // Decrement player and team score, but only if kills count towards team score and we aren't ignoring them
            if ($this->match->config->killsToTeamScore && !$this->match->config->ignoreTeamKillScoreDecrement) {
                $playerScore = -1;
                $playerTeamScore = -1;
            }
        } else {
            if ($teamKill) {
                $this->match->logWarning("Log expected teamkill between player {$this->id} team {$this->team} and player {$victim->id} team {$victim->team}, parser thinks it should be a normal kill instead");
            }

            // Normal kill
            $playerScore = 1;

            // Check for flag cover
            if (isset($this->flagCoverTime, $this->flagCoverVictim)) {
                if ($victimId == $this->flagCoverVictim && $this->getCurrentTime() - $this->flagCoverTime <= 1) {
                    $attributes[] = KillAttribute::FlagCover;
                }
                unset($this->flagCoverTime, $this->flagCoverVictim);
            }

            // Check for flag kill
            if ($this->match->isFlagKill($victimId)) {
                $playerScore = 5;  // kill plus 4
                $attributes[] = KillAttribute::FlagKill;
            }

            // Increment team score
            if ($this->match->config->killsToTeamScore) {
                $playerTeamScore = 1;
            }

            if (stristr($damageType, 'decapitated')) {
                $attributes[] = KillAttribute::Headshot;
            }

            if (!isset($this->match->firstBlood)) {
                $this->match->firstBlood = $this->id;
            }

            if ($this->spreeKillCount == 0) {
                $this->spreeStartTime = $this->getCurrentTime();
            }
            $this->spreeKillCount++;

            if (isset($this->lastKillTime) && $this->getCurrentTime() - $this->lastKillTime <= 400) {
                $this->multiKillCount++;
            } else {
                $this->endMulti();
                $this->multiKillCount = 1;
            }
            $this->lastKillTime = $this->getCurrentTime();
        }

        $this->match->insertKillEvent(Event\Kill::new($this, $killerWeapon, $victim, $victimWeapon, $damageType, $playerScore, $playerTeamScore, $attributes));

        if (strtolower($killerWeapon) != 'none') {
            $this->lastWeaponHeld = $killerWeapon;
        }
        $victim->deadCleanup($victimWeapon);
    }

    public function suicide($victimWeapon, $damageType)
    {
        if (!$this->validConnected('suicide')) {
            return;
        }

        if (isset($this->lastEntityDeathTime) && $this->getCurrentTime() - $this->lastEntityDeathTime <= 1) {
            // Do nothing. MonsterHunt logs a suicide when a player is killed by a monster.
            // Nstats mk tag will catch this, updating the lastEntityDeathTime
            // so we don't log a suicide
        } elseif ($this->teamCapturedSoSkipNextSuicide) {
            // Do nothing. Jailbreak logs a suicide when dying during team capture.
            // Nstats mk tag is also skipped, in case an entity or monster did the killing in jail.
            $this->skippedSuicides++;
            $this->teamCapturedSoSkipNextSuicide = false;
        } else {
            // Decrement score
            $playerScore = -1;

            // Decrement team score
            $playerTeamScore = 0;
            if ($this->match->config->killsToTeamScore) {
                $playerTeamScore = -1;
            }

            $this->match->insertSuicideEvent(Event\Suicide::new($this, $victimWeapon, $damageType, $playerScore, $playerTeamScore));
        }

        $this->deadCleanup($victimWeapon);
    }

    public function killEntity($entity)
    {
        switch (strtolower($entity)) {
            case 'jailbreak.jbtfemale1bot':
            case 'jailbreak.jbtfemale2bot':
            case 'jailbreak.jbtmale1bot':
            case 'jailbreak.jbtmale2bot':
            case 'utdmt.utdmt':
                return;
            default:
                break;
        }

        if (!$this->validConnected("kill entity {$entity}")) {
            return;
        }

        $this->match->insertEntityEvent(Event\Entity::PlayerKillEntity($this, $entity, $this->match->config->killingMonstersAffectsKillCount));
    }

    public function deathFromEntity($entity)
    {
        switch (strtolower($entity)) {
            case 'none':
                return;
            default:
                break;
        }

        if (!$this->validConnected("die from entity {$entity}")) {
            return;
        }

        $this->deadCleanup();

        if ($this->teamCapturedSoSkipNextSuicide) {
            // If this entity kill happens during the jailbreak team capture, we ignore it to not mess with stats.
            return;
        }

        $this->lastEntityDeathTime = $this->getCurrentTime();

        $this->match->insertEntityEvent(Event\Entity::entityKillPlayer($this, $entity));
    }

    public function flagTaken($team)
    {
        if (!$this->validConnected("take flag {$team}") || !$this->onATeam()) {
            return;
        }

        $this->doFlagPickup($team);
    }

    public function flagPickedUp($team)
    {
        if (!$this->validConnected("pickup flag {$team}") || !$this->onATeam()) {
            return;
        }

        $this->doFlagPickup($team);
    }

    private function doFlagPickup(int $team)
    {
        $this->updateFlagMatchConfig();

        $this->match->insertPlayerEvent(Event\Player::flagTake($this, $team));

        $this->flagsHeld[] = $team;
        if (!$this->flagStartHold) {
            $this->flagStartHold = $this->getCurrentTime();
        }
    }

    public function flagDropped($team)
    {
        if (!$this->validConnected('drop flag(s)') || !$this->onATeam()) {
            return;
        }

        $this->doFlagDropAll();
    }

    private function doFlagDropAll()
    {
        $this->updateFlagMatchConfig();

        foreach ($this->flagsHeld as $flag) {
            $this->match->insertPlayerEvent(Event\Player::flagDrop($this, $flag));
        }

        $this->clearFlagsHeld();
    }

    private function clearFlagsHeld()
    {
        $this->flagsHeld = array();
        if ($this->flagStartHold) {
            $this->flagHoldTime += $this->getCurrentTime() - $this->flagStartHold;
            $this->flagStartHold = 0;
        }
    }

    public function flagReturned($team)
    {
        if (!$this->validConnected("return flag {$team}") || !$this->onATeam()) {
            return;
        }

        $this->updateFlagMatchConfig();

        $pos = array_search($team, $this->flagsHeld);
        if ($pos !== false) {
            unset($this->flagsHeld[$pos]);
            $this->flagsHeld = array_values($this->flagsHeld);
            $this->match->insertPlayerEvent(Event\Player::flagReturn($this, $team));
            $this->flagReturnTime = $this->getCurrentTime();
        }
    }

    public function flagCovered($victim)
    {
        if (!$this->validConnected('cover flag') || !$this->onATeam()) {
            return;
        }

        $this->updateFlagMatchConfig();

        $this->flagCoverTime = $this->getCurrentTime();
        $this->flagCoverVictim = intval($victim);
    }

    public function flagCaptured($team)
    {
        if (!$this->validConnected('capture flag(s)') || !$this->onATeam()) {
            return;
        }

        $this->updateFlagMatchConfig();

        // Assumption made: If you capture a flag, you capture all that you hold at once
        // CTF4 does one flagCaptured for all flags, so this handles that
        // If another flag based gametype sends flagCaptured for each, this handles that too
        // If you aren't holding any flags, nothing happens

        // Verify captured flag is actually being held by player, or was their own team flag they just returned
        if (!in_array($team, $this->flagsHeld) && (!isset($this->flagReturnTime) || $this->flagReturnTime != $this->getCurrentTime())) {
            $prettyArray = print_r($this->flagsHeld, 1);
            throw new LogParseError("{$this->getCurrentTime()}: {$this->id} tried to capture flag {$team}, but is currently holding {$prettyArray}");
        }

        // Log captures
        $score = 7;
        foreach ($this->flagsHeld as $victimTeam) {
            $this->match->info->updateTeamCount($victimTeam);
            if ($victimTeam != $this->team) {
                $this->match->insertPlayerEvent(Event\Player::flagCapture($this, $victimTeam, $score, 1));
                switch ($score) {
                    case 7:
                        $score = 20 - 7;  // 2 flags is 20 points, ignore the 7 points from the first flag
                        break;
                    case 20 - 7:
                        $score = 50 - 20;  // 3 flags is 50 points, ignore the 20 points from the first 2 flags
                        break;
                    default:
                        break;
                }
            }
        }

        // TODO check for hat trick

        $this->clearFlagsHeld();
    }

    private function updateFlagMatchConfig()
    {
        $this->match->config->flagBasedGametype = true;
        if ($this->match->config->smartctfEnabled) {
            $this->match->config->overwritePlayerScores = true;
        }
    }

    public function captureControlPoint($pointName)
    {
        if (!$this->validConnected("capture control point {$pointName}") || !$this->onATeam()) {
            return;
        }

        // TODO start a timer for tracking held time?
        $this->match->insertPlayerEvent(Event\Player::domCapture($this, $pointName));
    }

    public function domScoreUpdate($newScore)
    {
        if (!$this->isConnected()) {
            return;
        }

        $currentScore = $this->getCurrentScore();
        $difference = intval($newScore - $currentScore);

        if ($difference < -1) {  // -1 because of dom partial points and rounding towards zero
            throw new LogParseError("{$this->getCurrentTime()}: DomScoreUpdate for player {$this->id} would have been {$difference}.");
        } elseif ($difference > 0) {
            $this->match->insertPlayerEvent(Event\Player::domScore($this, $difference));
        }
    }

    public function jailbreakTeamWasCaptured()
    {
        if (!$this->validConnected('be on captured JB team')) {
            return;
        }

        // With this being true, the next suicide (and optionally entity kill before it) will be ignored
        // Jailbreak team captures (I think) always end with a suicide logged
        $this->teamCapturedSoSkipNextSuicide = true;
    }

    public function objectiveDestroyed($objectiveId, $finalObjective)
    {
        if (!$this->validConnected("destroy objective {$objectiveId}")) {
            return;
        }

        $playerScore = 10;
        $playerTeamScore = 0;
        if ($finalObjective) {
            $playerTeamScore = 1;
            $playerScore += 100;
        }

        $this->match->insertPlayerEvent(Event\Player::assaultObjective($this, $objectiveId, $playerScore, $playerTeamScore));
    }

    public function scoreCorrection($newScore)
    {
        $difference = intval($newScore - $this->getInGameScore());
        if ($difference != 0) {
            $this->match->insertPlayerEvent(Event\Player::scoreCorrection($this, $difference));
        }
    }

    public function endCleanup()
    {
        $this->commonCleanup();
        $this->endTime = $this->getCurrentTime();
    }

    public function deadCleanup(string $currentlyHeldWeapon = null)
    {
        if (isset($currentlyHeldWeapon)) {
            $this->lastWeaponHeld = $currentlyHeldWeapon;
        }

        $this->previousActiveItems = $this->activeItems;
        $this->activeItems = array();

        $this->commonCleanup();
        if ($this->match->config->lastManStanding && $this->getLivesLeft() <= 0) {
            $this->match->insertPlayerEvent(Event\Player::lmsOut($this));
            $this->lmsOutTime = $this->getCurrentTime();
        }
    }

    private function commonCleanup()
    {
        $this->endSpree();
        $this->endMulti();
        $this->clearFlagsHeld();
    }

    private function endSpree()
    {
        $spreelvl = intval(floor($this->spreeKillCount / 5));
        if ($spreelvl > 0) {
            $this->spree[] = [$this->getCurrentTime(), $this->spreeStartTime, $spreelvl, $this->spreeKillCount];
        }
        $this->spreeKillCount = 0;
    }

    private function endMulti()
    {
        if ($this->multiKillCount > 1) {
            if (!isset($this->multi[$this->multiKillCount])) {
                $this->multi[$this->multiKillCount] = 0;
            }
            $this->multi[$this->multiKillCount]++;
        }
        $this->multiKillCount = 0;
    }

    private function countKillEvents(KillType $type)
    {
        $kills = 0;
        $deaths = 0;
        $teamKills = 0;
        $teamDeaths = 0;
        $suicides = 0;

        if (KillType::Suicide == $type || KillType::Frag == $type) {
            foreach ($this->match->getSuicideEvents() as $suicide) {
                if ($suicide->time >= $this->startTime && (!isset($this->endTime) || $suicide->time <= $this->endTime)) {  // Event happened while this player was connected
                    if ($this->id == $suicide->player && $this->team == $suicide->playerTeam) {
                        $suicides++;
                    }
                }
            }

            if (KillType::Suicide == $type) {
                return $suicides;
            }
        }

        foreach ($this->match->getKillEvents() as $kill) {
            if ($kill->time >= $this->startTime && (!isset($this->endTime) || $kill->time <= $this->endTime)) {  // Event happened while this player was connected
                if ($this->id == $kill->player && $this->team == $kill->playerTeam) {
                    if ($this->team >= 0 && $this->team == $kill->victimTeam) {  // Is a team kill
                        $teamKills++;
                    } else {  // Not team kill
                        $kills++;
                    }
                } elseif ($this->id == $kill->victim && $this->team == $kill->victimTeam) {
                    if ($this->team >= 0 && $this->team == $kill->playerTeam) {  // Is a team kill
                        $teamDeaths++;
                    } else {  // Not team kill
                        $deaths++;
                    }
                }
            }
        }

        switch ($type) {
            case KillType::Kill:
                return $kills;
            case KillType::Death:
                return $deaths;
            case KillType::TeamKill:
                return $teamKills;
            case KillType::TeamDeath:
                return $teamDeaths;
            case KillType::Frag:
                return $kills - $suicides;
            default:
                throw new \Exception("{$type->name} is not supported...");
        }
    }

    public function countEntityEvents(EventEntityType $type)
    {
        $totals = array();

        foreach ($this->match->getEntityEvents() as $event) {
            if ($this->id != $event->player || $this->team != $event->playerTeam) {
                continue;
            }
            if ($event->time >= $this->startTime && (!isset($this->endTime) || $event->time <= $this->endTime)) {  // Event happened while this player was connected
                if (!isset($totals[$event->type->value])) {
                    $totals[$event->type->value] = 0;
                }
                $totals[$event->type->value]++;
            }
        }

        if (!isset($totals[$type->value])) {
            return 0;
        }
        return $totals[$type->value];
    }

    private function getCurrentScore()
    {
        // Only used for DomScoreUpdate, as it doesn't return previous player connection total
        $total = 0;
        foreach ([$this->match->getEntityEvents(), $this->match->getKillEvents(), $this->match->getPlayerEvents(), $this->match->getSuicideEvents()] as $eventGenerator) {
            foreach ($eventGenerator as $event) {
                if ($event->time >= $this->startTime && (!isset($this->endTime) || $event->time <= $this->endTime)) {  // Event happened while this player was connected
                    if ($this->id == $event->player && $this->team == $event->playerTeam) {
                        $total += $event->playerScore;
                    }
                }
            }
        }

        // If the player switched teams/reconnected with same ID, add their previous total too.
        if (isset($this->previousPlayerConnection) && $this->previousPlayerConnection->id == $this->id) {
            $total += $this->previousPlayerConnection->getCurrentScore();
        }
        return $total;
    }

    public function getInGameScore()
    {
        if ($this->match->config->lastManStanding) {
            return $this->getLivesLeft();
        }
        return $this->getTotalScore(null);
    }

    public function getTotalScore($team = null)
    {
        if (isset($team) && $this->team != $team) {
            if (isset($this->previousPlayerConnection)) {
                return $this->previousPlayerConnection->getTotalScore($team);
            }
            return 0;
        }

        $total = 0;
        foreach ([$this->match->getEntityEvents(), $this->match->getPlayerEvents(), $this->match->getSuicideEvents()] as $eventGenerator) {
            foreach ($eventGenerator as $event) {
                if ($event->time >= $this->startTime && (!isset($this->endTime) || $event->time <= $this->endTime)) {  // Event happened while this player was connected
                    if ($this->id == $event->player && $this->team == $event->playerTeam) {
                        $total += $event->playerScore;
                    }
                }
            }
        }
        foreach ($this->match->getKillEvents() as $event) {
            if ($event->time >= $this->startTime && (!isset($this->endTime) || $event->time <= $this->endTime)) {  // Event happened while this player was connected
                if ($this->id == $event->player && $this->team == $event->playerTeam) {
                    $total += $event->playerScore;
                }
            }
        }

        // If the player switched teams/reconnected, add their previous total too.
        if (isset($this->previousPlayerConnection)) {
            $total += $this->previousPlayerConnection->getTotalScore($team);
        }
        return $total;
    }

    public function getTotalFrags($team = null, $loggerType = null)
    {
        if (isset($team) && $this->team != $team) {
            if (isset($this->previousPlayerConnection)) {
                return $this->previousPlayerConnection->getTotalFrags($team, $loggerType);
            }
            return 0;
        }

        $total = $this->countKillEvents(KillType::Frag);

        if (!isset($loggerType)) {
            // This is the more accurate total that will be shown on the website

            // Include the entity kills
            $total += $this->countEntityEvents(EventEntityType::PlayerKillEntity);

            // If the player switched teams/reconnected, add their previous total too.
            if (isset($this->previousPlayerConnection)) {
                $total += $this->previousPlayerConnection->getTotalFrags($team, $loggerType);
            }
        } elseif ($loggerType == 'utstats') {
            // This is the total that UTStatsBeta4_2.u will generate. Used exclusively for checking that our own calculations are accurate.

            // Remove deaths from the environment (they were counted as suicides in UTStats)
            $total -= $this->countEntityEvents(EventEntityType::EntityKillPlayer);

            // For jailbreak, we skip the suicide that you get when your whole team is captured. UTStats still logs it.
            $total -= $this->skippedSuicides;

            // If the player switched teams/reconnected, add their previous total too (but only if they have the same ID as before).
            if (isset($this->previousPlayerConnection) && $this->previousPlayerConnection->id == $this->id) {
                $total += $this->previousPlayerConnection->getTotalFrags($team, $loggerType);
            } else {
                // We only want to add this titan stuff to the player once, so it goes here

                // Add all player titans stats if we are the first non-titan player
                $total += $this->getPlayerTitanKillEventTotal(KillType::Frag);
            }
        } else {
            throw new \Exception("{$loggerType} is not supported...");
        }

        return $total;
    }

    public function getTotalKills($team = null, $loggerType = null)
    {
        if (isset($team) && $this->team != $team) {
            if (isset($this->previousPlayerConnection)) {
                return $this->previousPlayerConnection->getTotalKills($team, $loggerType);
            }
            return 0;
        }

        $total = $this->countKillEvents(KillType::Kill);

        if (!isset($loggerType)) {
            // This is the more accurate total that will be shown on the website

            // Include the entity kills
            $total += $this->countEntityEvents(EventEntityType::PlayerKillEntity);

            // If the player switched teams/reconnected, add their previous total too.
            if (isset($this->previousPlayerConnection)) {
                $total += $this->previousPlayerConnection->getTotalKills($team, $loggerType);
            }
        } elseif ($loggerType == 'utstats') {
            // This is the total that UTStatsBeta4_2.u will generate. Used exclusively for checking that our own calculations are accurate.

            // If the player switched teams/reconnected, add their previous total too (but only if they have the same ID as before).
            if (isset($this->previousPlayerConnection) && $this->previousPlayerConnection->id == $this->id) {
                $total += $this->previousPlayerConnection->getTotalKills($team, $loggerType);
            } else {
                // We only want to add this titan stuff to the player once, so it goes here

                // Add all player titans stats if we are the first non-titan player
                $total += $this->getPlayerTitanKillEventTotal(KillType::Kill);
            }
        } else {
            throw new \Exception("{$loggerType} is not supported...");
        }

        return $total;
    }

    public function getTotalDeaths($team = null, $loggerType = null)
    {
        if (isset($team) && $this->team != $team) {
            if (isset($this->previousPlayerConnection)) {
                return $this->previousPlayerConnection->getTotalDeaths($team, $loggerType);
            }
            return 0;
        }

        $total = $this->countKillEvents(KillType::Death);

        if (!isset($loggerType)) {
            // This is the more accurate total that will be shown on the website

            // Include the entity deaths, as they were counted as suicides in UTStats
            $total += $this->countEntityEvents(EventEntityType::EntityKillPlayer);

            // If the player switched teams/reconnected, add their previous total too.
            if (isset($this->previousPlayerConnection)) {
                $total += $this->previousPlayerConnection->getTotalDeaths($team, $loggerType);
            }
        } elseif ($loggerType == 'utstats') {
            // This is the total that UTStatsBeta4_2.u will generate. Used exclusively for checking that our own calculations are accurate.

            // If the player switched teams/reconnected, add their previous total too (but only if they have the same ID as before).
            if (isset($this->previousPlayerConnection) && $this->previousPlayerConnection->id == $this->id) {
                $total += $this->previousPlayerConnection->getTotalDeaths($team, $loggerType);
            } else {
                // We only want to add this titan stuff to the player once, so it goes here

                // Add all player titans stats if we are the first non-titan player
                $total += $this->getPlayerTitanKillEventTotal(KillType::Death);
            }
        } else {
            throw new \Exception("{$loggerType} is not supported...");
        }

        return $total;
    }

    public function getTotalSuicides($team = null, $loggerType = null)
    {
        if (isset($team) && $this->team != $team) {
            if (isset($this->previousPlayerConnection)) {
                return $this->previousPlayerConnection->getTotalSuicides($team, $loggerType);
            }
            return 0;
        }

        $total = $this->countKillEvents(KillType::Suicide);

        if (!isset($loggerType)) {
            // This is the more accurate total that will be shown on the website

            // If the player switched teams/reconnected, add their previous total too.
            if (isset($this->previousPlayerConnection)) {
                $total += $this->previousPlayerConnection->getTotalSuicides($team, $loggerType);
            }
        } elseif ($loggerType == 'utstats') {
            // This is the total that UTStatsBeta4_2.u will generate. Used exclusively for checking that our own calculations are accurate.

            // Include the entity deaths, as they were counted as suicides in UTStats
            $total += $this->countEntityEvents(EventEntityType::EntityKillPlayer);

            // For jailbreak, we skip the suicide that you get when your whole team is captured. UTStats still logs it.
            $total += $this->skippedSuicides;

            // If the player switched teams/reconnected, add their previous total too (but only if they have the same ID as before).
            if (isset($this->previousPlayerConnection) && $this->previousPlayerConnection->id == $this->id) {
                $total += $this->previousPlayerConnection->getTotalSuicides($team, $loggerType);
            } else {
                // We only want to add this titan stuff to the player once, so it goes here

                // Add all player titans stats if we are the first non-titan player
                $total += $this->getPlayerTitanKillEventTotal(KillType::Suicide);
            }
        } else {
            throw new \Exception("{$loggerType} is not supported...");
        }

        return $total;
    }

    public function getTotalTeamKills($team = null, $loggerType = null)
    {
        if (isset($team) && $this->team != $team) {
            if (isset($this->previousPlayerConnection)) {
                return $this->previousPlayerConnection->getTotalTeamKills($team, $loggerType);
            }
            return 0;
        }

        $total = $this->countKillEvents(KillType::TeamKill);

        if (!isset($loggerType)) {
            // This is the more accurate total that will be shown on the website

            // If the player switched teams/reconnected, add their previous total too.
            if (isset($this->previousPlayerConnection)) {
                $total += $this->previousPlayerConnection->getTotalTeamKills($team, $loggerType);
            }
        } elseif ($loggerType == 'utstats') {
            // This is the total that UTStatsBeta4_2.u will generate. Used exclusively for checking that our own calculations are accurate.

            // If the player switched teams/reconnected, add their previous total too (but only if they have the same ID as before).
            if (isset($this->previousPlayerConnection) && $this->previousPlayerConnection->id == $this->id) {
                $total += $this->previousPlayerConnection->getTotalTeamKills($team, $loggerType);
            } else {
                // We only want to add this titan stuff to the player once, so it goes here

                // Add all player titans stats if we are the first non-titan player
                $total += $this->getPlayerTitanKillEventTotal(KillType::TeamKill);
            }
        } else {
            throw new \Exception("{$loggerType} is not supported...");
        }

        return $total;
    }

    public function getTotalTeamDeaths($team = null, $loggerType = null)
    {
        if (isset($team) && $this->team != $team) {
            if (isset($this->previousPlayerConnection)) {
                return $this->previousPlayerConnection->getTotalTeamDeaths($team, $loggerType);
            }
            return 0;
        }

        $total = $this->countKillEvents(KillType::TeamDeath);

        if (!isset($loggerType)) {
            // This is the more accurate total that will be shown on the website

            // If the player switched teams/reconnected, add their previous total too.
            if (isset($this->previousPlayerConnection)) {
                $total += $this->previousPlayerConnection->getTotalTeamDeaths($team, $loggerType);
            }
        } elseif ($loggerType == 'utstats') {
            // This is the total that UTStatsBeta4_2.u will generate. Used exclusively for checking that our own calculations are accurate.

            // If the player switched teams/reconnected, add their previous total too (but only if they have the same ID as before).
            if (isset($this->previousPlayerConnection) && $this->previousPlayerConnection->id == $this->id) {
                $total += $this->previousPlayerConnection->getTotalTeamDeaths($team, $loggerType);
            } else {
                // We only want to add this titan stuff to the player once, so it goes here

                // Add all player titans stats if we are the first non-titan player
                $total += $this->getPlayerTitanKillEventTotal(KillType::TeamDeath);
            }
        } else {
            throw new \Exception("{$loggerType} is not supported...");
        }

        return $total;
    }

    public function getEfficiency($team = null, $loggerType = null)
    {
        // Kills / (Kills + Deaths + Suicides [+Team Kills])
        $kills = $this->getTotalKills($team, $loggerType);
        $deaths = $this->getTotalDeaths($team, $loggerType);
        $teamDeaths = $this->getTotalTeamDeaths($team, $loggerType);
        $suicides = $this->getTotalSuicides($team, $loggerType);
        $teamKills = $this->getTotalTeamKills($team, $loggerType);
        $calculated = 0.0;
        $denominator = $kills + $deaths + $teamDeaths + $suicides + $teamKills;
        if ($denominator > 0) {
            $calculated = $kills / $denominator * 100;
        }
        return $calculated;
    }

    public function getCentisecondsOnServer($team = null)
    {
        if (isset($team) && $this->team != $team) {
            if (isset($this->previousPlayerConnection)) {
                return $this->previousPlayerConnection->getCentisecondsOnServer($team);
            }
            return 0;
        }

        $startTime = 0;
        if (isset($this->match->startTime)) {
            $startTime = max($startTime, $this->match->startTime);
        }
        if (isset($this->startTime)) {
            $startTime = max($startTime, $this->startTime);
        }

        $endTime = $this->getCurrentTime();
        if (isset($this->match->endTime)) {
            $endTime = min($endTime, $this->match->endTime);
        }
        if (isset($this->endTime)) {
            $endTime = min($endTime, $this->endTime);
        }
        if (isset($this->lmsOutTime)) {
            $endTime = min($endTime, $this->lmsOutTime);
        }

        $total = $endTime - $startTime;
        if (isset($this->previousPlayerConnection)) {
            $total += $this->previousPlayerConnection->getCentisecondsOnServer();
        }
        return $total;
    }

    public function getPickupTotals($team = null)
    {
        if (isset($team) && $this->team != $team) {
            if (isset($this->previousPlayerConnection)) {
                return $this->previousPlayerConnection->getPickupTotals($team);
            }
            return array();
        }

        $result = $this->pickups;
        if (isset($this->previousPlayerConnection)) {
            foreach ($this->previousPlayerConnection->getPickupTotals($team) as $pickupName => $total) {
                if (!isset($result[$pickupName])) {
                    $result[$pickupName] = 0;
                }
                $result[$pickupName] += $total;
            }
        }
        return $result;
    }

    public function getLivesLeft()
    {
        return $this->lives - $this->getTotalDeaths() - $this->getTotalTeamDeaths() - $this->getTotalSuicides();
    }

    private function getPlayerTitanKillEventTotal(KillType $killEventType)
    {
        $playerTitanFrags = 0;
        foreach ($this->match->player as $id => $playerData) {
            if ($playerData->info->titan) {
                continue;
            }
            // We found the first non-titan player, check if it's us
            if ($id == $this->id) {
                // We are the first non-titan player, so add all the titan stats to our counts
                foreach ($this->match->player as $id => $titanData) {
                    if ($titanData->info->titan) {
                        $playerTitanFrags += $titanData->countKillEvents($killEventType);
                    }
                }
            }
            break;
        }
        return $playerTitanFrags;
    }

    private function handlePlayerTitanKillJoin($titanWeapon, $teamKill, $victimId, $victimWeapon, $titanDamageType)
    {
        $titan = $this->determinePlayerTitanFromKill($titanWeapon, $teamKill, $victimId, $titanDamageType);
        if (isset($titan)) {
            $titan->kill($titanWeapon, $teamKill, $victimId, $victimWeapon, $titanDamageType);
            return true;
        }
        return false;
    }

    private function handlePlayerTitanDeathJoin($titanWeapon, $teamKill, $killerId)
    {
        $result = $this->determinePlayerTitanFromKill($titanWeapon, $teamKill, $killerId);
        if (isset($result)) {
            return $result;
        }
        return $this;
    }

    private function determinePlayerTitanFromKill($titanWeapon, $teamKill, $opponent, $titanDamageType = 'crushed')
    {
        if ($this->valid() && !$this->connected && $this->isTitanDamage($titanWeapon, $titanDamageType)) {
            // Check if this is a level that has player titans
            $opponentPlayer = $this->match->getPlayer($opponent);
            // $redTeam = (!$teamKill && $opponentPlayer->team == 1) || ($teamKill && $opponentPlayer->team == 0);
            $blueTeam = (!$teamKill && $opponentPlayer->team == 0) || ($teamKill && $opponentPlayer->team == 1);
            return $this->match->titanManager->getPlayerTitan($this->id, $blueTeam ? 1 : 0);
        }
        return null;
    }

    private function handleLevelTitanKill($titanWeapon, $teamKill, $victimId, $victimWeapon, $titanDamageType)
    {
        $titan = $this->determineLevelTitanFromKill($titanWeapon, $teamKill, $victimId, $titanDamageType);
        if (isset($titan)) {
            $titan->kill($titanWeapon, $teamKill, $victimId, $victimWeapon, $titanDamageType);
            return true;
        }
        return false;
    }

    private function handleLevelTitanDeath($titanWeapon, $teamKill, $killerId)
    {
        $result = $this->determineLevelTitanFromKill($titanWeapon, $teamKill, $killerId);
        if (isset($result)) {
            return $result;
        }
        return $this;
    }

    private function determineLevelTitanFromKill($titanWeapon, $teamKill, $opponent, $titanDamageType = 'crushed')
    {
        if ($this->valid() && $this->id == 0 && $this->isTitanDamage($titanWeapon, $titanDamageType)) {
            // Check if this is a level that has level titans
            $opponentPlayer = $this->match->getPlayer($opponent);
            // $redTeam = (!$teamKill && $opponentPlayer->team == 1) || ($teamKill && $opponentPlayer->team == 0);
            $blueTeam = (!$teamKill && $opponentPlayer->team == 0) || ($teamKill && $opponentPlayer->team == 1);
            return $this->match->titanManager->getLevelTitan($blueTeam ? 1 : 0);
        }
        return null;
    }

    private function isTitanDamage($weapon, $damageType = 'crushed')
    {
        $damage = strtolower($damageType);
        return strtolower($weapon) == 'none' && ($damage == 'crushed' || $damage == 'hacked' || $damage == 'stomped' || $damage == 'gibbed' || $damage == 'specialdamage');
    }
}
