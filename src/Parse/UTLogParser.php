<?php

namespace App\Parse;

use App\Enum\EventPlayerType;
use App\Parse\Exception\LogParseError;

define('TIME_FACTOR_UT99', 0.9096422);  // Did a 1 hour match, 3600 seconds / (3980.69 game_end timelimit - 23.09 game_start) = 0.9096422073984233

class UTLogParser
{
    private string $sourceCodeHash;
    private static \DateTimeZone $utc;

    private static function getUtc(): \DateTimeZone
    {
        return self::$utc ??= new \DateTimeZone('UTC');
    }

    public function hashLog($file)
    {
        if (!isset($this->sourceCodeHash)) {
            $context = hash_init('murmur3f');

            $sourceCodeFiles = array();
            $srcDir = dirname(__DIR__);
            $srcDirs = [$srcDir . '/Entity', $srcDir . '/Enum', $srcDir . '/Import', $srcDir . '/Parse'];
            foreach ($srcDirs as $dir) {
                $directory = new \RecursiveDirectoryIterator($dir);
                $iterator = new \RecursiveIteratorIterator($directory);
                foreach ($iterator as $info) {
                    $filename = $info->getPathname();
                    if (str_ends_with(strtolower($filename), '.php')) {
                        $sourceCodeFiles[] = $filename;
                    }
                }
            }
            sort($sourceCodeFiles);
            foreach ($sourceCodeFiles as $sourceCodeFile) {
                hash_update($context, hash_file('murmur3f', $sourceCodeFile));
            }

            $this->sourceCodeHash = hash_final($context);
        }

        return hash('murmur3f', $this->sourceCodeHash . hash_file('murmur3f', $file));
    }

    public function parseLog($file)
    {
        $match = new MatchData();
        $match->logFileHash = $this->hashLog($file);
        $match->logFileName = basename($file);
        $parsedLines = array();  // Array where the key is the line, so we can do quick lookups to see if a line was parsed instead of using in_array()
        $fastForward = false;

        $previousTag = null;
        $previousData = null;

        foreach (file($file) as $line) {
            $line = preg_replace('/[\x00]/', '', $line);
            $line = mb_convert_encoding($line, 'UTF-8', mb_detect_encoding($line, 'UTF-8, ISO-8859-1', true));
            $data = array_map('trim', explode("\t", $line));
            if (count($data) == 0) {
                continue;
            }
            if (empty($data[0]) || empty($data[1])) {
                continue;
            }
            if (!is_numeric($data[0])) {
                continue;
            }
            $time = intval(floatval($data[0]) * 100);
            $tag = strtolower($data[1]);
            $data = array_slice($data, 2);

            if ($time < $match->currentTime) {
                // If we encounter a line with a time less than our current time, there was a log glitch.
                // Fast forward the log until we get to the next non-duplicate line.
                $fastForward = true;
                continue;
            }

            // If we are fast forwarding due to a log glitch, keep going until we find a line we haven't seen yet.
            if ($fastForward && isset($parsedLines[$line])) {
                continue;
            } else {
                $fastForward = false;
            }

            $parsedLines[$line] = true;

            if (isset($previousTag)) {
                $this->parseLine($match, $previousTag, $previousData, $time, $tag, $data);
            }

            $match->setCurrentTime($time);
            $previousTag = $tag;
            $previousData = $data;
        }

        if (isset($previousTag)) {
            $this->parseLine($match, $previousTag, $previousData);
        }

        $match->postProcess();
        return $match;
    }

    private function parseLine(MatchData $match, $tag, $data, $nextTime = null, $nextTag = null, $nextData = null)
    {
        switch ($tag) {
            case 'info':
                $this->parseUTInfo($match, $data);
                break;
            case 'map':
                $this->parseUTMap($match, $data);
                break;
            case 'game':
                $this->parseUTGame($match, $data);
                break;
            case 'player':
                $this->parseUTPlayer($match, $data, $nextTime, $nextTag, $nextData);
                break;
            case 'game_start':
                $match->start();
                break;
            case 'item_get':
                $match->getPlayer($data[1])->itemPickup($data[0]);
                break;
            case 'item_activate':
                $match->getPlayer($data[1])->itemActivate($data[0]);
                break;
            case 'item_deactivate':
                $match->getPlayer($data[1])->itemDeactivate($data[0]);
                break;
            case 'kill':
                $this->parseUTKill($match, $data, false, $nextTime, $nextTag, $nextData);
                break;
            case 'teamkill':
                $this->parseUTKill($match, $data, true, $nextTime, $nextTag, $nextData);
                break;
            case 'suicide':
                $match->getPlayer($data[0])->suicide($data[1], $data[2]);
                break;
            case 'first_blood':
            case 'headshot':
                // Handled in kill/teamKill
                break;
            case 'flag_taken':
                $match->getPlayer($data[0])->flagTaken($data[1]);
                break;
            case 'flag_dropped':
                $match->getPlayer($data[0])->flagDropped($data[1]);
                break;
            case 'flag_pickedup':
                $match->getPlayer($data[0])->flagPickedUp($data[1]);
                break;
            case 'flag_returned':
            case 'flag_returned_mid':
            case 'flag_return_mid':
            case 'flag_return_closesave':
            case 'flag_return_base':
            case 'flag_return_enemybase':
                $match->getPlayer($data[0])->flagReturned($data[1]);
                break;
            case 'flag_returned_timeout':
                // $match->flagReturnedTimeout($data[0]);
                break;
            case 'flag_captured':
                $match->getPlayer($data[0])->flagCaptured($data[1]);
                break;
            case 'flag_cover':  // killer, victim, killerTeam
                $match->getPlayer($data[0])->flagCovered($data[1]);
                break;
            case 'flag_kill':
                // Handled in kill/teamKill
                break;
            case 'dom_playerscore_update':
                // Only take the last dom_playerscore_update for a given player
                if (isset($nextTime, $nextTag, $nextData) && $nextTime - $match->currentTime <= 1 && strtolower($nextTag) == $tag && strtolower($nextData[0]) == strtolower($data[0])) {
                    break;
                }
                $match->getPlayer($data[0])->domScoreUpdate($data[1]);
                break;
            case 'dom_score_update':
                $match->domScoreUpdate($data[0], $data[1]);
                break;
            case 'controlpoint_capture':
                $match->getPlayer($data[1])->captureControlPoint($data[0]);
                break;
            case 'team_captured':
                $match->jailbreakCapture($data[0]);
                break;
            case 'team_released':
                $match->jailbreakRelease($data[1]);
                break;
            case 'arena_started':
                $match->jailbreakArenaStart($data[0], $data[1]);
                break;
            case 'arena_won':
                $match->jailbreakArenaWon($data[0]);
                break;
            case 'arena_tie':
                $match->jailbreakArenaTie();
                break;
            case 'llama_start':
            case 'llama_terminate':
                break;
            case 'assault_timelimit':
                $match->info->timeLimit = intval($data[0]);
                break;
            case 'assault_gamecode':
                switch (count($data)) {
                    case 1:
                        $match->info->matchNumber = intval($data[0]);
                        break;
                    case 2:
                        $match->info->assaultGameCode = $data[0];
                        $match->info->matchNumber = intval($data[1]);
                        break;
                    default:
                        $prettyData = print_r($data, 1);
                        throw new LogParseError("Unsupported assault_gamecode: {$match->currentTime} {$tag} {$prettyData}");
                }
                break;
            case 'assault_objname':
                $match->assaultObjectives[$data[0]] = $data[1];
                break;
            case 'assault_defender':
                $match->info->assaultDefender = intval($data[0]);
                break;
            case 'assault_attacker':
                $match->info->assaultAttacker = intval($data[0]);
                break;
            case 'assault_obj':
                $finalObjective = strtolower($data[1]) == 'true';
                $match->getPlayer($data[0])->objectiveDestroyed($data[2], $finalObjective);
                break;
            case 'spree':
                // Handled in kill/teamKill
                break;
            case 'game_end':
                $match->end($data[0]);
                break;
            case 'teamscore':
                $this->verifyTeamScore($match, $data[0], $data[1]);
                break;
            case 'stat_player':
                $this->parseStatPlayer($match, $data);
                break;
            case 'weap_shotcount':
                if (!$match->config->ignoreEndWeaponStats) {
                    $match->getPlayer($data[1])->shotCount[$data[0]] = $data[2];
                }
                break;
            case 'weap_hitcount':
                if (!$match->config->ignoreEndWeaponStats) {
                    $match->getPlayer($data[1])->hitCount[$data[0]] = $data[2];
                }
                break;
            case 'weap_damagegiven':
                if (!$match->config->ignoreEndWeaponStats) {
                    $match->getPlayer($data[1])->damageGiven[$data[0]] = $data[2];
                }
                break;
            case 'weap_accuracy':
                if (!$match->config->ignoreEndWeaponStats) {
                    $match->getPlayer($data[1])->accuracy[$data[0]] = $data[2];
                }
                break;
            case 'ip':
                // I think this is a logging error, as it normally would be in player
                break;
            case 'spawnkill':
                // Not sure what to do with this. UTStatsBeta4_2.u and UTStatsDB don't take this into account.
                break;
            case 'throw_translocator':
            case 'translocate':
            case 'translocate_fail':
            case 'translocate_gib':
            case 'typing':
            case 'longrangekill':
            case 'cover_bonus':
                // Ignore these
                break;
            case 'nstats':
                $this->parseNstats($match, $data, $nextTime, $nextTag, $nextData);
                break;
            default:
                $prettyData = print_r($data, 1);
                throw new LogParseError("Line not mapped: {$match->currentTime} {$tag} {$prettyData}");
        }
    }

    private function parseUTInfo($match, $data)
    {
        switch (strtolower($data[0])) {
            case 'absolute_time':  // 2022.11.21.20.32.28.384.-5.0
                {
                    $dateParts = explode('.', $data[1]);
                    if (count($dateParts) != 9) {
                        throw new LogParseError("Unsupported {$data[0]} format: {$data[1]}");
                    }

                    $timezone = new \DateTimeZone($dateParts[7] . ':' . $dateParts[8]);
                    $match->info->timezone = $timezone->getName();
                    $match->info->matchDate = \DateTimeImmutable::createFromFormat('!x.m.d.H.i.s.v.+', $data[1], $timezone)->setTimezone(self::getUtc());
                    $match->info->timeFactor = TIME_FACTOR_UT99;
                    break;
                }
            case 'game_version':  // 469
                {
                    $match->info->serverVersion = $data[1];
                    break;
                }
            case 'server_servername':
                {
                    $match->info->serverName = $data[1];
                    $match->info->shortName = '';
                    break;
                }
            case 'server_adminname':
                {
                    $match->info->adminName = $data[1];
                    break;
                }
            case 'server_adminemail':
                {
                    $match->info->adminEmail = $data[1];
                    break;
                }
            case 'log_standard':
            case 'log_version':
            case 'game_name':
            case 'utglenabled':
            case 'true_server_ip':
            case 'server_region':
            case 'server_motdline1':
            case 'server_motdline2':
            case 'server_motdline3':
            case 'server_motdline4':
            case 'server_ip':
            case 'server_port':
            case 'log_info_url':
            case 'game_creator':
            case 'game_creator_url':
            case 'game_decoder_ring_url':
                // Ignored
                break;
            default:
                $prettyData = print_r($data, 1);
                throw new LogParseError("Info tag not mapped: '{$prettyData}'");
        }
    }

    private function parseUTMap($match, $data)
    {
        switch (strtolower($data[0])) {
            case 'name':  // DM-Q317][.unr
                {
                    $mpfn = $data[1];
                    if (!strcasecmp(substr($mpfn, -4), '.unr')) {
                        $mpfn = substr($mpfn, 0, -4);
                    }
                    $match->info->mapName = $mpfn;
                    break;
                }
            case 'title':  // Quake III Arena: The Longest Yard, UT-style!
                {
                    $match->info->mapTitle = $data[1];
                    break;
                }
            case 'author':  // DAVID M.+ {LoD}QuazBotch, Ultron & Dr. Doom
                {
                    $match->info->mapAuthor = $data[1];
                    break;
                }
            case 'idealplayercount':
            case 'levelentertext':
                // Ignored
                break;
            default:
                $prettyData = print_r($data, 1);
                throw new LogParseError("Map tag not mapped: '{$prettyData}'");
        }
    }

    private function parseUTGame($match, $data)
    {
        switch (strtolower($data[0])) {
            case 'realstart':
                {
                    $match->start();
                    break;
                }
            case 'goodmutator':  // Team Beacon
            case 'gamemutator':  // Class Botpack.DMMutator
                {
                    $mutator = $data[1];
                    if (!strncasecmp($mutator, 'Class ', 6)) {
                        $mutator = substr($mutator, 6);
                    }
                    $match->info->mutators[] = $mutator;

                    $mutator = strtolower($mutator);
                    if (strstr($mutator, 'smartsb')) {
                        $match->config->smartctfEnabled = true;
                    }
                    if (strstr($mutator, 'smartctf')) {
                        $match->config->smartctfEnabled = true;
                    }
                    break;
                }
            case 'gamename':  // Tournament DeathMatch
                {
                    $match->info->gameName = $data[1];
                    break;
                }
            case 'gameclass':  // Botpack.TeamGamePlus
                {
                    $match->info->gameClass = $data[1];
                    $gameClassLower = strtolower($data[1]);
                    $gameClass = preg_replace('/^excessive\w*-(\w+)\.excessive/', '${1}.', $gameClassLower);
                    $gameClass = preg_replace('/^excessive\w*\.excessive/', 'botpack.', $gameClass);
                    if ($gameClass != $gameClassLower) {
                        $match->config->ignoreTeamKillScoreDecrement = true;  // Excessive Overkill games don't count team kills against the score ever.
                    }
                    switch ($gameClass) {
                        case 'botpack.teamgameplus':
                            {
                                $match->config->killsToTeamScore = true;
                            }
                            break;
                        case 'botpack.lastmanstanding':
                        case 'tlastmanstanding.tlastmanstanding':
                        case 'teamlmslite.teamlmslite':
                            {
                                $match->config->lastManStanding = true;
                            }
                            break;
                        case 'monsterhunt.monsterhunt':
                        case 'monsterhunt.monsterhuntarena':
                        case 'monsterhunt.monsterhuntdefence':
                            {
                                $match->config->overwritePlayerScores = true;  // Score per monster type can be changed
                                $match->config->ignoreEndWeaponStats = true;  // UTStatsBeta4_2.u doesn't log kills against monsters
                                $match->config->killingMonstersAffectsKillCount = true;  // Killing a monster adds to the players kill count
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                }
            case 'gameversion':  // 469
                {
                    $match->info->serverVersion = $data[1];
                    break;
                }
            case 'fraglimit':  // 10
                {
                    $match->info->fragLimit = intval($data[1]);
                    break;
                }
            case 'timelimit':  // 0
                {
                    $match->info->timeLimit = intval($data[1]);
                    break;
                }
            case 'teamgame':  // True / False
                {
                    if (strtolower($data[1]) == 'true') {
                        $match->info->updateTeamCount(0);
                    }
                    break;
                }
            case 'goalteamscore':
                {
                    $match->info->goalTeamScore = intval($data[1]);
                    break;
                }
            case 'insta':
            case 'minnetversion':
            case 'nomonsters':
            case 'mutespectators':
            case 'humansonly':
            case 'weaponsstay':
            case 'classicdeathmessages':
            case 'lowgore':
            case 'verylowgore':
            case 'gamespeed':
            case 'maxspectators':
            case 'maxplayers':
            case 'multiplayerbots':
            case 'hardcore':
            case 'megaspeed':
            case 'aircontrol':
            case 'jumpmatch':
            case 'usetranslocator':
            case 'tournamentmode':
            case 'netmode':
            case 'friendlyfirescale':
                // Ignored
                break;
            case 'teambalance':
            case 'pickupsrespawnhealth':
            case 'pickupsrespawnweapon':
            case 'pickupsrespawnammo':
            case 'pickupsrespawnarmor':
            case 'pickupsrespawnpowerup':
            case 'maxcaptime':
            case 'releasereset':
            case 'hopdelay':
            case 'protection':
            case 'protectionshoot':
            case 'protectiontimemax':
            case 'protectiontimepickup':
                // Jailbreak ignored
                break;
            case 'protectionllamaize':
                // Jailbreak doesn't ever log the gameName or gameClass, so we set it here, where probably no other gametype would use this tag
                $match->info->gameName = 'Jailbreak';
                $match->info->gameClass = 'JailBreak.JailBreak';
                $match->info->updateTeamCount(0);
                $match->config->overwritePlayerScores = true;  // Kills near button are worth more, everyone you release is worth a point, etc.
                $match->config->ignoreEndWeaponStats = true;  // UTStatsBeta4_2.u doesn't log anything for bots? I don't know why...
                break;
            default:
                $prettyData = print_r($data, 1);
                throw new LogParseError("Game tag not mapped: '{$prettyData}'");
        }
    }

    private function parseUTPlayer($match, $data, $nextTime, $nextTag, $nextData)
    {
        switch (strtolower($data[0])) {
            case 'rename':  // PhasecoreX	0
                {
                    $match->getPlayer($data[2])->rename($data[1]);
                    break;
                }
            case 'teamchange':  // 0	255
            case 'team':  // 0	255
                {
                    $match->getPlayer($data[1])->setTeam($data[2]);
                    break;
                }
            case 'connect':  // PhasecoreX	0	False
                {
                    $match->playerConnect($data[2], $data[1]);
                    break;
                }
            case 'disconnect':
                {
                    $match->getPlayer($data[1])->disconnect();
                    break;
                }
            case 'ping':  // 0	15
                {
                    // Only take the last ping for a given player
                    if (isset($nextTime, $nextTag, $nextData) && $nextTime - $match->currentTime <= 1 && strtolower($nextTag) == 'player' && strtolower($data[0]) == strtolower($nextData[0]) && $data[1] == $nextData[1]) {
                        break;
                    }

                    $match->getPlayer($data[1])->ping($data[2]);
                    break;
                }
            case 'isabot':  // 0	False
                {
                    if (strtolower($data[2]) == 'true') {
                        $match->getPlayer($data[1])->info->bot = true;
                    }
                    break;
                }
            case 'skill':  // 0 1.000000
                {
                    // $player = $match->getPlayer($data[1]);
                    // if (!$player)
                    //    break;
                    //
                    // $bot_skill = intval($data[2]);
                    // bot_add($plr,$bot_skill,$bot_alertness,$bot_accuracy,$bot_aggressive,$bot_strafing,$bot_style,$bot_tactics,$bot_transloc,$bot_reaction,$bot_jumpiness,$bot_favorite);
                    break;
                }
            case 'ip':
                {
                    $match->getPlayer($data[1])->info->ip = $data[2];
                    break;
                }
            case 'teamname':
            case 'teamid':
            case 'novice':
            case 'netspeed':
            case 'fov':
            case 'skin':
                // Ignored
                break;
            case 'voicetype':
            case 'face':
                // Nstats handles this better (and correctly) if installed
                break;
            default:
                $prettyData = print_r($data, 1);
                throw new LogParseError("Player tag not mapped: '{$prettyData}'");
        }
    }

    private function parseUTKill($match, $data, $teamKill, $nextTime, $nextTag, $nextData)
    {
        $killerId = $data[0];
        $killerWeapon = $data[1];
        $victimId = $data[2];
        $victimWeapon = $data[3];
        $damageType = $data[4];

        if (isset($nextTime, $nextTag, $nextData) && strtolower($damageType) == 'gibbed') {
            if ($nextTime - $match->currentTime <= 1 && $nextTag == 'player' && in_array(strtolower($nextData[0]), ['rename', 'teamchange', 'connect'])) {
                // If the next log line is a player joining at the exact time of this kill
                return;
            }
        }

        $match->getPlayer($killerId)->kill($killerWeapon, $teamKill, $victimId, $victimWeapon, $damageType);
    }

    private function verifyTeamScore($match, $team, $score)
    {
        $match->info->updateTeamCount($team);
        $calculated = $match->getTeamScore($team);
        if ($score != $calculated) {
            // Check if we were in flag subtraction mode
            $subtractionTeamScore = 0;
            foreach ($match->getPlayerEvents() as $playerEvent) {
                if ($playerEvent->type == EventPlayerType::FlagCapture) {
                    if ($playerEvent->playerTeam == $team) {
                        $subtractionTeamScore++;
                    }
                    if ($playerEvent->targetId == $team) {
                        $subtractionTeamScore--;
                    }
                }
            }
            if ($score == $subtractionTeamScore) {
                return;
            }

            $match->logWarning("Team {$team} score of {$calculated} does not match expected value of {$score}");
        }
    }

    private function parseStatPlayer(MatchData $match, $data)
    {
        $player = $match->getPlayer($data[1]);
        $statValue = $data[2];

        if (strtolower($data[0]) == 'score') {
            $calculated = $player->getInGameScore();
            if ($statValue != $calculated) {
                if ($match->config->overwritePlayerScores) {
                    $player->scoreCorrection($statValue);
                } else {
                    $match->logWarning("Player {$data[1]} {$data[0]} of {$calculated} does not match expected value of {$data[2]}");
                }
            }
            return;
        }

        switch (strtolower($data[0])) {
            case 'accuracy':
                // Ignoring overall accuracy in favor of individual weapon accuracy
                break;
            case 'frags':
                {
                    $calculated = $player->getTotalFrags(null, 'utstats');
                    if ($statValue != $calculated) {
                        $match->logWarning("Player {$data[1]} {$data[0]} of {$calculated} does not match expected value of {$data[2]}");
                    }
                    break;
                }
            case 'kills':
                {
                    $calculated = $player->getTotalKills(null, 'utstats');
                    if ($statValue != $calculated) {
                        $match->logWarning("Player {$data[1]} {$data[0]} of {$calculated} does not match expected value of {$data[2]}");
                    }
                    break;
                }
            case 'deaths':
                {
                    $calculated = $player->getTotalDeaths(null, 'utstats') + $player->getTotalTeamDeaths(null, 'utstats');
                    if ($statValue != $calculated) {
                        $match->logWarning("Player {$data[1]} {$data[0]} of {$calculated} does not match expected value of {$data[2]}");
                    }
                    break;
                }
            case 'suicides':
                {
                    $calculated = $player->getTotalSuicides(null, 'utstats');
                    if ($statValue != $calculated) {
                        $match->logWarning("Player {$data[1]} {$data[0]} of {$calculated} does not match expected value of {$data[2]}");
                    }
                    break;
                }
            case 'teamkills':
                {
                    $calculated = $player->getTotalTeamKills(null, 'utstats');
                    if ($statValue != $calculated) {
                        $match->logWarning("Player {$data[1]} {$data[0]} of {$calculated} does not match expected value of {$data[2]}");
                    }
                    break;
                }
            case 'efficiency':
                {
                    $calculated = $player->getEfficiency(null, 'utstats');
                    $rounded = round($calculated, 3);
                    $statValue = round($statValue, 3);
                    if ($statValue != $rounded) {
                        $match->logWarning("Player {$data[1]} {$data[0]} of {$calculated} does not match expected value of {$data[2]}");
                    }
                    break;
                }
            case 'time_on_server':
            case 'ttl':
                // UTStatsBeta4_2.u is inaccurate with these, so we ignore them
                break;
            default:
                $prettyData = print_r($data, 1);
                throw new LogParseError("Stat_player tag not mapped: '{$prettyData}'");
        }
    }

    private function parseNstats($match, $data, $nextTime, $nextTag, $nextData)
    {
        switch (strtolower($data[0])) {
            case 'spawn_point':
            case 'weapon_location':
            case 'pickup_location':
            case 'ammo_location':
            case 'dom_point':
            case 'flag_location':
                // Inital map setup info
                break;
            case 'face':
                $match->getPlayer($data[1])->info->face = $data[2];
                break;
            case 'voice':
                $match->getPlayer($data[1])->info->voice = $data[2];
                break;
            case 'spawn_loc':
                // Each (re)spawn
                break;
            case 'kill_distance':
            case 'kill_location':
                // Each kill
                break;
            case 'flag_kill':
                // Handled in kill/teamKill
                break;
            case 'monsterkill':  // Player killed entity
                $match->getPlayer($data[1])->killEntity($data[2]);
                break;
            case 'mk':  // Entity killed player
                $match->getPlayer($data[2])->deathFromEntity($data[1]);
                break;
            case 'p_s':
                {
                    // Player Score update (if different than last time)

                    // UTDMT titans and other stuff get logged as player 0 before the actual amount
                    // Assumption made: the real player 0 is always the last listed one in the log
                    if (isset($nextTime, $nextTag, $nextData) && $nextTime - $match->currentTime <= 1 && strtolower($nextTag) == 'nstats' && strtolower($data[0]) == strtolower($nextData[0]) && $data[1] == $nextData[1]) {
                        return;
                    }

                    if (!$match->config->overwritePlayerScores && strtolower($match->info->gameClass) == 'botpack.domination') {
                        break;
                    }  // Sometimes domination games log the dom_playerscore_update after this nstats p_s one is logged

                    $player = $match->getPlayer($data[1]);
                    if (!$player->connected) {
                        return;
                    }

                    $calculated = $player->getInGameScore();
                    $statValue = $data[2];

                    if ($statValue != $calculated) {
                        if ($match->config->overwritePlayerScores) {
                            $player->scoreCorrection($statValue);
                        } else {
                            $match->logWarning("Player {$data[1]} score of {$calculated} does not match expected value of {$data[2]}");
                        }
                    }
                    break;
                }
            case 'fdl':
                // I dunno what these are
                break;
            default:
                $prettyData = print_r($data, 1);
                throw new LogParseError("Nstats tag not mapped: '{$prettyData}'");
        }
    }
}
