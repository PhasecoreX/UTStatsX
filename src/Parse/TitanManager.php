<?php

namespace App\Parse;

use App\Parse\Exception\LogParseError;

class TitanManager
{
    private $match;

    private $levelTitan = array();

    private $playerTitan = array();

    private $teams = false;

    private $titanData = [
        'ctfm-ancienttitan' => [
            'name' => 'Big Bro',
        ],
        'ctfm-ancienttitanv110' => [
            'name' => 'Big Bro',
        ],
        'ctfm-cryptitan' => [
            'name' => 'Titan Da Beaker & Titan Da Musk Rat',  // blue = beaker, gold = musk rat
        ],
        'ctf-titania' => [
            'teamName' => ['Atlas', 'Daedalus'],
        ],
        'dm-mazeoftitania' => [
            'name' => 'Minos',
        ],
        'dm-titania][' => [
            'name' => 'Mr.Titan',
        ],
    ];

    private $playerTitanData = [
        'jb-ancienttitan-iii' => [
            'name' => 'Big Bro',
        ],
        'jb-skytitan' => [
            'name' => 'Big Bro',
        ],
        'jb-sprintitan' => [
            'teamName' => ['-= Slap Nuts =-', '-= Rock Whacker =-'],
        ],
    ];

    public function __construct(MatchData $match, string $mapName)
    {
        $this->match = $match;
        $mapNameLower = strtolower($mapName);

        if (isset($this->titanData[$mapNameLower])) {
            $titanData = $this->titanData[$mapNameLower];
            if (isset($titanData['teamName'])) {
                $this->teams = true;
                foreach ($titanData['teamName'] as $teamId => $name) {
                    // For team games, player ID = -1 - teamId
                    $this->levelTitan[] = $this->match->titanConnect(-1 - $teamId, $name, $teamId);
                }
            } else {
                $this->levelTitan[] = $this->match->titanConnect(-1, $titanData['name']);
            }
        } elseif (isset($this->playerTitanData[$mapNameLower])) {
            $titanData = $this->playerTitanData[$mapNameLower];
            if (isset($titanData['teamName'])) {
                $this->teams = true;
                foreach ($titanData['teamName'] as $name) {
                    $this->playerTitan[] = $name;
                }
            } else {
                $this->playerTitan[] = $titanData['name'];
            }
        }
    }

    public function __destruct()
    {
        // Any player titans that somehow didn't do any events (getPlayerTitan was never called) will be force added now
        foreach ($this->playerTitan as $teamId => $value) {
            if (is_string($value)) {
                $testId = -1;
                while (isset($this->match->player[$testId])) {
                    $testId--;
                }
                $this->getPlayerTitan($testId, $teamId);
            }
        }
    }

    public function getLevelTitan(int $team = -1)
    {
        $titanCount = sizeof($this->levelTitan);
        if ($titanCount == 0) {
            return null;
        } elseif ($titanCount == 1) {
            return $this->levelTitan[0];
        } elseif ($team < 0) {
            return null;
        } else {
            return $this->levelTitan[$team];
        }
    }

    public function getPlayerTitan(int $playerId, int $team = -1)
    {
        $titanCount = sizeof($this->playerTitan);
        if ($titanCount == 0) {
            return null;
        } elseif ($titanCount == 1) {
            $team = 0;
        } elseif ($team < 0) {
            return null;
        }

        if (is_string($this->playerTitan[$team])) {
            $this->playerTitan[$team] = $this->match->titanConnect($playerId, $this->playerTitan[$team], $this->teams ? $team : null);
        }
        return $this->playerTitan[$team];
    }
}
