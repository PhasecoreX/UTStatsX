<?php

namespace App\Parse;

use App\Enum\EventPlayerType;
use App\Parse\Exception\LogParseError;

class MatchData
{
    public MatchConfig $config;
    public MatchInfo $info;
    public string $logFileHash;
    public string $logFileName;
    public $parseWarnings = array();
    public int $currentTime = 0;
    public string $endReason = '';
    public string $endReasonRaw = '';
    public string $endReasonDisplay = '';
    public \DateTimeImmutable $startDate;
    public int $startTime;
    public int $endTime;
    public int $firstBlood;
    public $assaultObjectives = array();
    public $arenaPlayers = array();
    public $player = array();
    private ?Player $connectingPlayer;
    public $titanManager;
    private $entityEvent = array();
    private $killEvent = array();
    private $playerEvent = array();
    private $suicideEvent = array();
    private $teamEvent = array();

    public function __construct()
    {
        $this->config = new MatchConfig();
        $this->info = new MatchInfo();
    }

    public function logWarning(string $warning): static
    {
        $this->parseWarnings[] = "{$this->currentTime}: {$warning}";

        return $this;
    }

    public function setCurrentTime(int $time): static
    {
        $this->currentTime = $time;

        return $this;
    }

    public function start()
    {
        if ($this->endReason) {
            return;
        }

        if (isset($this->startTime) && $this->startTime > 0) {
            return;
        }

        if (!isset($this->startTime)) {
            $this->titanManager = new TitanManager($this, $this->info->mapName);
        }

        $this->startTime = $this->currentTime;
        $this->startDate = $this->info->matchDate->modify('+ ' . intval($this->startTime * $this->info->timeFactor * 10) . ' milliseconds');
    }

    public function end($reason)
    {
        if ($this->endReason) {
            return;
        }

        $this->endReasonRaw = strtolower($reason);
        $this->endTime = $this->currentTime;

        // Clean up players, and add up LMS lives if we are doing that
        $lmsTeamScore = array();
        foreach ($this->getConnectedPlayers() as $player) {
            $player->endCleanup();
            if ($this->isTeamGame() && $this->config->lastManStanding && $player->team >= 0 && $player->team <= 3) {
                if (!isset($lmsTeamScore[$player->team])) {
                    $lmsTeamScore[$player->team] = 0;
                }
                $lmsTeamScore[$player->team] += $player->getLivesLeft();
            }
        }
        foreach ($lmsTeamScore as $team => $score) {
            $this->insertTeamEvent(Event\Team::teamLmsEndTotal($this->currentTime, $team, $score));
        }

        $teamRankings = $this->getTeamRankings();

        // Detect CTF4 Distribution mode, update goalTeamScore accordingly
        // Distribution mode is where the goalTeamScore defines how many times you must capture EACH teams flags
        // So, a goalTeamScore of 2 would mean you have to capture each teams flag 2 times
        // For a 4 team game, that would be 3 opposing teams * 2 goalTeamScore = new goalTeamScore of 6
        if ($this->config->flagBasedGametype && $teamRankings) {
            $winningTeamScore = $teamRankings[0]['score'];
            $scoreMoreThanLimit = $winningTeamScore > $this->info->goalTeamScore;
            $scoreIsNotTeamScoreLimit = $winningTeamScore >= $this->info->goalTeamScore && $this->endReasonRaw != 'teamscorelimit';
            $distributionTeamScoreLimit = $this->info->goalTeamScore * ($this->info->teamCount - 1);
            if (($scoreMoreThanLimit || $scoreIsNotTeamScoreLimit) && $winningTeamScore <= $distributionTeamScoreLimit) {
                $this->info->goalTeamScore = $distributionTeamScoreLimit;
            }
        }

        switch ($this->endReasonRaw) {
            case 'fraglimit':
                $this->endReason = 'complete';
                $this->endReasonDisplay = 'Frag Limit Reached';
                $winningPlayer = $this->getWinningPlayer(true);
                if (isset($winningPlayer)) {
                    $highestFragCount = $winningPlayer->getTotalFrags();
                    if ($this->info->fragLimit == 0) {
                        $this->info->fragLimit = $highestFragCount;
                    } elseif ($this->info->fragLimit != $highestFragCount) {
                        $this->logWarning("Game fraglimit is {$this->info->fragLimit}, but winning player {$winningPlayer->info->name} got {$highestFragCount} frags");
                    }
                }
                break;
            case 'timelimit':
                if (isset($this->info->assaultAttacker)) {
                    $this->end('Assault failed!');
                    return;
                }
                $this->endReason = 'complete';
                $this->endReasonDisplay = 'Time Limit Reached';
                if ($this->info->timeLimit == 0) {
                    // JailBreak doesn't log any useful information
                    // (doesn't call super in its LogGameParameters() function)
                    // so I must make up for it here
                    $totalMatchTimeCentiseconds = ($this->currentTime - $this->startTime) * $this->info->timeFactor;
                    $this->info->timeLimit = intval(round($totalMatchTimeCentiseconds / 100 / 60));  // centiseconds to seconds, seconds to minutes
                }
                break;
            case 'teamscorelimit':
                $this->endReason = 'complete';
                $this->endReasonDisplay = 'Team Score Limit Reached';
                if (!$this->config->lastManStanding && $teamRankings) {
                    $winningTeamScore = $teamRankings[0]['score'];
                    if ($this->info->goalTeamScore == 0) {
                        // See above timelimit JailBreak quip
                        $this->info->goalTeamScore = $winningTeamScore;
                    } elseif ($this->info->goalTeamScore != $winningTeamScore) {
                        $this->logWarning("Game teamscorelimit is {$this->info->goalTeamScore}, but winning team got a score of {$winningTeamScore}");
                    }
                }
                break;
            case 'goalscorelimit':
                $this->endReason = 'complete';
                $this->endReasonDisplay = 'Goal Score Limit Reached';
                break;
            case 'roundlimit':
                $this->endReason = 'complete';
                $this->endReasonDisplay = 'Round Limit Reached';
                break;
            case 'lastmanstanding':
                $this->endReason = 'complete';
                $this->endReasonDisplay = 'Last Man Standing';
                break;
            case 'assault succeeded!':
                $this->endReason = 'complete';
                $this->endReasonDisplay = 'Assault Succeeded';
                if ($this->getTeamScore($this->info->assaultAttacker) == 0) {
                    $this->teamScoreCorrection($this->info->assaultAttacker, 1);
                }
                break;
            case 'assault failed!':
                $this->endReason = 'complete';
                $this->endReasonDisplay = 'Assault Failed';
                if ($this->getTeamScore($this->info->assaultDefender) == 0) {
                    $this->teamScoreCorrection($this->info->assaultDefender, 1);
                }
                break;
            case 'mapchange':
                $this->endReason = 'mapchange';
                $this->endReasonDisplay = 'Map Change';
                break;
            case 'serverquit':
                $this->endReason = 'serverquit';
                $this->endReasonDisplay = 'Server Quit';
                break;
            default:
                $this->endReason = 'unknown';
                $this->endReasonDisplay = "Unknown ({$reason})";
        }
    }

    public function playerConnect($id, $name, $team = null)
    {
        $player = $this->getPlayer($id);
        $player->rename($name);
        if (isset($team)) {
            $player->setTeam($team);
        }
        $this->insertPlayerEvent(Event\Player::connect($player));
    }

    public function titanConnect($id, $name, $team = null)
    {
        $titan = $this->getPlayer($id);

        if ($titan->isConnected()) {
            return $titan;
        }

        $titan->rename($name);
        if (isset($team)) {
            $titan->setTeam($team);
        }

        $titan->startTime = 0;
        $titan->info->titan = true;

        $event = Event\Player::connect($titan);
        $event->time = 0;
        $this->insertPlayerEvent($event);

        return $this->getPlayer($id);  // Forces the connecting player to be written to player array
    }

    public function getPlayer($id)
    {
        // Verify numeric values
        if (!is_numeric($id)) {
            throw new LogParseError("Player ID '{$id}' not supported");
        }
        $plr = intval($id);

        // If we had a connecting player, and they are now connected, add them to the player list
        if (isset($this->connectingPlayer) && $this->connectingPlayer->connected) {
            $this->setPlayer($this->connectingPlayer->id, $this->connectingPlayer);
        }

        $this->connectingPlayer = null;

        // If we haven't seen this player ID yet, make a new player object and set it to connecting
        if (!isset($this->player[$plr])) {
            $this->connectingPlayer = Player::new($this, $plr);
            return $this->connectingPlayer;
        }

        return $this->player[$plr];
    }

    public function setPlayer($id, Player $player): static
    {
        // In the old days (think ngLog days, UTv436 probably) rejoining would give the same ID.
        // Bots would fill in with the same ID as well, though their name might change.
        // This loop won't do anything.
        // TODO have it handle reconnect better? To keep track of total player time more accurately.

        // Nowadays (UTv451+ probably) every conection gets an increasing ID, even if relogging.
        // This loop will find the user with the same name already in our player array who is not connected.
        // (UT99 source determines a "same player" by checking the name and password, but passwords aren't even sent anymore, nor logged)

        // Regardless of how old your logs are, the player->setTeam will handle linking players when switching teams.
        for (end($this->player); key($this->player) !== null; prev($this->player)) {
            $existingPlayer = current($this->player);
            if ($existingPlayer->info->name == $player->info->name && $existingPlayer->hasCurrentPlayerId() && !$existingPlayer->connected) {
                $player->linkExistingPlayer($existingPlayer);
                break;
            }
        }
        reset($this->player);

        $this->player[intval($id)] = $player;
        ksort($this->player);

        return $this;
    }

    public function domScoreUpdate($team, $newScore)
    {
        $difference = intval($newScore - $this->getTeamScore($team));
        if ($difference != 0) {
            $this->insertTeamEvent(Event\Team::domScore($this->currentTime, $team, $difference));
        }
    }

    public function jailbreakCapture($capturedTeam)
    {
        if (!$this->isTeamGame()) {
            return;
        }

        // Jailbreak doesn't log who caused the capture, for some reason
        $capturedTeam = intval($capturedTeam);
        if ($capturedTeam < 0 || $capturedTeam > 1) {
            throw new LogParseError('Only Jailbreak matches with 2 teams is supported');
        }
        $player = null;
        foreach ($this->getKillEvents(true) as $currentKill) {
            if ($currentKill->victimTeam == $capturedTeam) {
                $player = $this->getPlayer($currentKill->player);
                break;
            }
        }
        if (isset($player)) {
            $this->insertPlayerEvent(Event\Player::jbCaptureTeam($player, $capturedTeam));
        }
        // Everyone on the winning team gets a point, everyone on the losing team skips the impending suicide
        $winningTeam = abs($capturedTeam - 1);
        foreach ($this->getConnectedPlayers() as $teamPlayer) {
            if ($teamPlayer->team == $winningTeam) {
                $this->insertPlayerEvent(Event\Player::jbOnWinningTeam($teamPlayer, 1));
            } else {
                $teamPlayer->jailbreakTeamWasCaptured();
            }
        }
        $this->insertTeamEvent(Event\Team::teamCapture($this->currentTime, $winningTeam, 1));
    }

    public function jailbreakRelease($plr)
    {
        if (!$this->isTeamGame()) {
            return;
        }

        $player = $this->getPlayer($plr);
        $this->insertPlayerEvent(Event\Player::jbReleaseTeam($player, $player->team));
    }

    public function jailbreakArenaStart($plr1, $plr2)
    {
        if (!$this->isTeamGame()) {
            return;
        }

        $this->arenaPlayers = [intval($plr1), intval($plr2)];
        // TODO arena start event
    }

    public function jailbreakArenaWon($plr)
    {
        if (!$this->isTeamGame()) {
            return;
        }

        // Jailbreak doesn't log who the loser was, so we take figure it out from arena_started event
        // Two assumptions made:
        // 1. It's always 2 players ($this->arenaPlayers is always length 2, from jailbreakArenaStart)
        // 2. Only one arena match at a time (pretty sure this is true, as there's only one spot in the map for an arena)
        $winner = intval($plr);
        $loser = intval($this->arenaPlayers[0]);
        if ($winner == $loser) {
            $loser = intval($this->arenaPlayers[1]);
        }
        $this->insertPlayerEvent(Event\Player::jbArenaWin($this->getPlayer($winner)));
        $this->insertPlayerEvent(Event\Player::jbArenaLose($this->getPlayer($loser)));
        $this->arenaPlayers = array();
        // TODO arena finished event
    }

    public function jailbreakArenaTie()
    {
        if (!$this->isTeamGame()) {
            return;
        }

        if (count($this->arenaPlayers) != 2) {
            return;
        }

        $this->insertPlayerEvent(Event\Player::jbArenaTie($this->getPlayer($this->arenaPlayers[0])));
        $this->insertPlayerEvent(Event\Player::jbArenaTie($this->getPlayer($this->arenaPlayers[1])));
        // TODO arena finished event
    }

    public function isFlagKill($victimId)
    {
        $result = false;
        foreach ($this->getPlayerEvents(true) as $event) {
            if ($this->currentTime - $event->time > 1) {
                break;
            }
            if ($event->time > $this->currentTime) {
                continue;  // Can this ever happen? Probably not, but just in case...
            }
            if ($event->type == EventPlayerType::FlagDrop && $event->player == $victimId) {
                $result = true;
                break;
            }
        }
        return $result;
    }

    public function isTeamGame()
    {
        return $this->info->teamCount > 0;
    }

    public function teamScoreCorrection($team, $newScore)
    {
        $difference = intval($newScore - $this->getTeamScore($team));
        if ($difference != 0) {
            $this->insertTeamEvent(Event\Team::scoreCorrection($this->currentTime, $team, $difference));
        }
    }

    public function getTeamScore($team)
    {
        $total = 0;
        foreach ([$this->entityEvent, $this->killEvent, $this->playerEvent, $this->suicideEvent] as $eventList) {
            foreach ($eventList as $event) {
                if ($team == $event->playerTeam) {
                    $total += $event->playerTeamScore;
                }
            }
        }
        foreach ($this->teamEvent as $event) {
            if ($team == $event->team) {
                $total += $event->score;
            }
        }
        return intval($total);
    }

    public function getTeamPlayerScoreTotal($team)
    {
        $total = 0;
        foreach ([$this->entityEvent, $this->killEvent, $this->playerEvent, $this->suicideEvent] as $eventList) {
            foreach ($eventList as $event) {
                if ($team == $event->playerTeam) {
                    $total += $event->playerScore;
                }
            }
        }
        return intval($total);
    }

    public function getConnectedPlayers()
    {
        foreach ($this->player as $player) {
            if ($player->isConnected()) {
                yield $player;
            }
        }
    }

    public function getPlayerByName($playerName)
    {
        foreach ($this->player as $player) {
            if ($player->info->name == $playerName && $player->hasCurrentPlayerId()) {
                return $player;
            }
        }
    }

    public function getWinningPlayer($byFrags = false)
    {
        $bestPlayer = null;
        $bestPlayerScore = null;
        foreach ($this->player as $player) {
            if (!$player->isConnected()) {
                continue;
            }
            if ($this->config->lastManStanding) {
                $playerScore = $player->getLivesLeft();
            } elseif ($byFrags) {
                $playerScore = $player->getTotalFrags();
            } else {
                $playerScore = $player->getTotalScore();
            }
            if (!isset($bestPlayer) || $playerScore > $bestPlayerScore) {
                $bestPlayer = $player;
                $bestPlayerScore = $playerScore;
            }
        }
        return $bestPlayer;
    }

    public function getTeamRankings()
    {
        // Gametypes seem to favor the first team (0/red) in case of ties, but this rarely happens in practice
        if (!$this->isTeamGame()) {
            return array();
        }

        $rankings = array();
        for ($i = 0; $i < $this->info->teamCount; $i++) {
            $rankings[] = [
                'team' => $i,
                'score' => $this->getTeamScore($i),
            ];
        }
        usort($rankings, function ($a, $b) {
            $result = $b['score'] <=> $a['score'];
            if ($result == 0) {
                return $this->getTeamPlayerScoreTotal($b['team']) <=> $this->getTeamPlayerScoreTotal($a['team']);
            }
            return $result;
        });
        return $rankings;
    }

    public function didPlayerWinMatch($playerName, $team)
    {
        foreach ($this->player as $player) {
            if ($player->info->name == $playerName && $player->hasCurrentPlayerId()) {
                if (!$player->isConnected()) {
                    return false;
                }
                if ($this->isTeamGame()) {
                    return $player->team == $team && $player->team == $this->getTeamRankings()[0]['team'];
                } else {
                    return $player == $this->getWinningPlayer();
                }
            }
        }
    }

    public function getKillEvents($reverse = false)
    {
        $index = 0;
        $increment = 1;
        if ($reverse) {
            $index = count($this->killEvent) - 1;
            $increment = -1;
        }
        while (isset($this->killEvent[$index])) {
            yield $this->killEvent[$index];
            $index += $increment;
        }
    }

    public function insertKillEvent(Event\Kill $killEvent)
    {
        $this->killEvent[] = $killEvent;

        $arrayLength = count($this->killEvent);
        if ($arrayLength > 1 && $this->killEvent[$arrayLength - 2]->time > $this->killEvent[$arrayLength - 1]->time) {
            usort($this->killEvent, [$this::class, 'eventCompare']);
        }
    }

    public function getSuicideEvents($reverse = false)
    {
        $index = 0;
        $increment = 1;
        if ($reverse) {
            $index = count($this->suicideEvent) - 1;
            $increment = -1;
        }
        while (isset($this->suicideEvent[$index])) {
            yield $this->suicideEvent[$index];
            $index += $increment;
        }
    }

    public function insertSuicideEvent(Event\Suicide $suicideEvent)
    {
        $this->suicideEvent[] = $suicideEvent;

        $arrayLength = count($this->suicideEvent);
        if ($arrayLength > 1 && $this->suicideEvent[$arrayLength - 2]->time > $this->suicideEvent[$arrayLength - 1]->time) {
            usort($this->suicideEvent, [$this::class, 'eventCompare']);
        }
    }

    public function getEntityEvents($reverse = false)
    {
        $index = 0;
        $increment = 1;
        if ($reverse) {
            $index = count($this->entityEvent) - 1;
            $increment = -1;
        }
        while (isset($this->entityEvent[$index])) {
            yield $this->entityEvent[$index];
            $index += $increment;
        }
    }

    public function insertEntityEvent(Event\Entity $entityEvent)
    {
        $this->entityEvent[] = $entityEvent;

        $arrayLength = count($this->entityEvent);
        if ($arrayLength > 1 && $this->entityEvent[$arrayLength - 2]->time > $this->entityEvent[$arrayLength - 1]->time) {
            usort($this->entityEvent, [$this::class, 'eventCompare']);
        }
    }

    public function getPlayerEvents($reverse = false)
    {
        $index = 0;
        $increment = 1;
        if ($reverse) {
            $index = count($this->playerEvent) - 1;
            $increment = -1;
        }
        while (isset($this->playerEvent[$index])) {
            yield $this->playerEvent[$index];
            $index += $increment;
        }
    }

    public function insertPlayerEvent(Event\Player $playerEvent)
    {
        $this->playerEvent[] = $playerEvent;

        $arrayLength = count($this->playerEvent);
        if ($arrayLength > 1 && $this->playerEvent[$arrayLength - 2]->time > $this->playerEvent[$arrayLength - 1]->time) {
            usort($this->playerEvent, [$this::class, 'eventCompare']);
        }
    }

    public function updatePlayerConnectEvent(Player $player)
    {
        // If the last player event is Connect for the given $player, update it with this $player info (team probably changed)
        $lastEntry = count($this->playerEvent) - 1;

        if ($lastEntry > -1) {
            $lastEvent = $this->playerEvent[$lastEntry];
            if ($lastEvent->type == EventPlayerType::Connect && $player->id == $lastEvent->player && $this->currentTime - $lastEvent->time <= 1) {
                $this->playerEvent[$lastEntry] = Event\Player::connect($player);
                return;
            }
        }
    }

    public function getTeamEvents($reverse = false)
    {
        $index = 0;
        $increment = 1;
        if ($reverse) {
            $index = count($this->teamEvent) - 1;
            $increment = -1;
        }
        while (isset($this->teamEvent[$index])) {
            yield $this->teamEvent[$index];
            $index += $increment;
        }
    }

    public function insertTeamEvent(Event\Team $teamEvent)
    {
        $this->teamEvent[] = $teamEvent;

        $arrayLength = count($this->teamEvent);
        if ($arrayLength > 1 && $this->teamEvent[$arrayLength - 2]->time > $this->teamEvent[$arrayLength - 1]->time) {
            usort($this->teamEvent, [$this::class, 'eventCompare']);
        }
    }

    private function eventCompare($a, $b)
    {
        return $a->time <=> $b->time;
    }

    public function postProcess()
    {
        // Any processing that needs to be done after the entire match is parsed happens here

        // Close down Titan Manager, finishing anything else it needs to do
        unset($this->titanManager);
    }
}
