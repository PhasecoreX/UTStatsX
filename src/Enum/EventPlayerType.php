<?php

namespace App\Enum;

enum EventPlayerType: string
{
    case Connect = 'connect';
    case TeamChange = 'team_change';
    case Disconnect = 'disconnect';
    case FlagCapture = 'flag_capture';
    case FlagPickup = 'flag_pickup';
    case FlagReturn = 'flag_return';
    case FlagTake = 'flag_take';
    case FlagDrop = 'flag_drop';
    case AssaultObjective = 'assault_objective';
    case DomCapture = 'dom_capture';
    case DomScoreUpdate = 'dom_score_update';
    case LmsOut = 'lms_out';
    case JbCaptureTeam = 'jb_capture_team';
    case JbReleaseTeam = 'jb_release_team';
    case JbOnWinningTeam = 'jb_on_winning_team';
    case JbArenaWin = 'jb_arena_win';
    case JbArenaTie = 'jb_arena_tie';
    case JbArenaLose = 'jb_arena_lose';
    case ScoreCorrection = 'score_correction';
}
