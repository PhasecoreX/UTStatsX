<?php

namespace App\Enum;

enum Weapon: string
{
    case DoubleEnforcers = 'Double Enforcers';
    case Enforcer = 'Enforcer';
    case EnhancedShockRifle = 'Enhanced Shock Rifle';
    case FlakCannon = 'Flak Cannon';
    case GESBioRifle = 'GES BioRifle';
    case ImpactHammer = 'Impact Hammer';
    case Minigun = 'Minigun';
    case None = 'None';
    case PulseGun = 'Pulse Gun';
    case Redeemer = 'Redeemer';
    case Ripper = 'Ripper';
    case RocketLauncher = 'Rocket Launcher';
    case ShockRifle = 'Shock Rifle';
    case SniperRifle = 'Sniper Rifle';

    public static function getWeapon(string $weapon)
    {
        switch (strtolower($weapon)) {
            case 'double enforcer':
                return self::DoubleEnforcers;
            case 'ges bio rifle':
                return self::GESBioRifle;
            default:
                break;
        }
        return self::tryFrom(ucwords($weapon));
    }
}
