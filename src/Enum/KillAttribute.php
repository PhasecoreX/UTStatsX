<?php

namespace App\Enum;

enum KillAttribute: string
{
    case FlagCover = 'flag_cover';
    case FlagKill = 'flag_kill';
    case Headshot = 'headshot';
}
