<?php

namespace App\Enum;

enum DamageType: string
{
    case AltJolted = 'AltJolted';
    case Corroded = 'Corroded';
    case Crushed = 'Crushed';
    case Decapitated = 'Decapitated';
    case Eradicated = 'Eradicated';
    case FlakDeath = 'FlakDeath';
    case Gibbed = 'Gibbed';
    case GrenadeDeath = 'GrenadeDeath';
    case Hacked = 'Hacked';
    case Impact = 'Impact';
    case Jolted = 'Jolted';
    case Pulsed = 'Pulsed';
    case Pushed = 'Pushed';
    case RedeemerDeath = 'RedeemerDeath';
    case RedeemerSplashDamage = 'RedeemerSplashDamage';
    case RipperAltDeath = 'RipperAltDeath';
    case RocketDeath = 'RocketDeath';
    case Shot = 'Shot';
    case Shredded = 'Shredded';
    case SpecialDamage = 'SpecialDamage';
    case Stomped = 'Stomped';
    case Zapped = 'Zapped';

    public static function getDamageType(string $damageType)
    {
        return self::tryFrom(ucwords($damageType));
    }
}
