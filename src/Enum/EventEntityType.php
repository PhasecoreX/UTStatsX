<?php

namespace App\Enum;

enum EventEntityType: string
{
    case PlayerKillEntity = 'player_kill_entity';
    case PlayerKillIgnoredEntity = 'player_kill_ignored_entity';
    case EntityKillPlayer = 'entity_kill_player';
}
