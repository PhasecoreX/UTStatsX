<?php

namespace App\Enum;

enum KillType: string
{
    case Kill = 'kill';
    case Death = 'death';
    case Suicide = 'suicide';
    case TeamKill = 'team_kill';
    case TeamDeath = 'team_death';
    case Frag = 'frag';
}
