<?php

namespace App\Enum;

enum EventTeamType: string
{
    case DomScoreUpdate = 'dom_score_update';
    case TeamCapture = 'team_capture';
    case TeamLmsEndTotal = 'team_lms_end_total';
    case ScoreCorrection = 'score_correction';
}
