<?php

namespace App\Repository;

use App\Entity\TeamScore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TeamScore>
 *
 * @method TeamScore|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeamScore|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeamScore[]    findAll()
 * @method TeamScore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamScoreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TeamScore::class);
    }

//    /**
//     * @return TeamScore[] Returns an array of TeamScore objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TeamScore
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
