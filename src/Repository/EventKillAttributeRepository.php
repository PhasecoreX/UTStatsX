<?php

namespace App\Repository;

use App\Entity\EventKillAttribute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EventKillAttribute>
 *
 * @method EventKillAttribute|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventKillAttribute|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventKillAttribute[]    findAll()
 * @method EventKillAttribute[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventKillAttributeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventKillAttribute::class);
    }

//    /**
//     * @return EventKillAttribute[] Returns an array of EventKillAttribute objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EventKillAttribute
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
