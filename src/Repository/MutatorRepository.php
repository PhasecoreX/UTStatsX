<?php

namespace App\Repository;

use App\Entity\Mutator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Mutator>
 *
 * @method Mutator|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mutator|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mutator[]    findAll()
 * @method Mutator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MutatorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mutator::class);
    }

//    /**
//     * @return Mutator[] Returns an array of Mutator objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Mutator
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
