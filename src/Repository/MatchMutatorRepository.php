<?php

namespace App\Repository;

use App\Entity\MatchMutator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MatchMutator>
 *
 * @method MatchMutator|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatchMutator|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatchMutator[]    findAll()
 * @method MatchMutator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchMutatorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MatchMutator::class);
    }

//    /**
//     * @return MatchMutator[] Returns an array of MatchMutator objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?MatchMutator
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
