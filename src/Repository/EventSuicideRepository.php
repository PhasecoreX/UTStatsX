<?php

namespace App\Repository;

use App\Entity\EventSuicide;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EventSuicide>
 *
 * @method EventSuicide|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventSuicide|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventSuicide[]    findAll()
 * @method EventSuicide[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventSuicideRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventSuicide::class);
    }

//    /**
//     * @return EventSuicide[] Returns an array of EventSuicide objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EventSuicide
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
