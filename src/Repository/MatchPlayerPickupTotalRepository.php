<?php

namespace App\Repository;

use App\Entity\MatchPlayerPickupTotal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MatchPlayerPickupTotal>
 *
 * @method MatchPlayerPickupTotal|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatchPlayerPickupTotal|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatchPlayerPickupTotal[]    findAll()
 * @method MatchPlayerPickupTotal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchPlayerPickupTotalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MatchPlayerPickupTotal::class);
    }

//    /**
//     * @return MatchPlayerPickupTotal[] Returns an array of MatchPlayerPickupTotal objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?MatchPlayerPickupTotal
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
