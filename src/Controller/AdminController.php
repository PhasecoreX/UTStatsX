<?php

namespace App\Controller;

use App\Import\UTLogImporter;
use App\Parse\UTLogParser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    #[Route('/admin/import', name: 'admin_import')]
    public function import(EntityManagerInterface $em): Response
    {
        $parser = new UTLogParser();
        $importer = new UTLogImporter($em);
        $folder = '../var/ut_logs/';

        $resultText = "Searching folder <code>{$folder}</code><br>";
        if (is_dir($folder)) {
            $logfiles = array();
            foreach (new \DirectoryIterator($folder) as $fileInfo) {
                if ($fileInfo->isDot()) {
                    continue;
                }
                $pathName = $fileInfo->getPathname();
                if (str_ends_with(strtolower($pathName), '.log')) {
                    $logfiles[] = $pathName;
                }
            }

            sort($logfiles);
            $totalTime = 0.0;
            $count = 0;
            foreach ($logfiles as $logfile) {
                $match = null;
                try {
                    $logFileHash = $parser->hashLog($logfile);

                    if ($importer->logHashExists($logFileHash)) {
                        continue;
                    }

                    $fileName = basename($logfile);
                    $resultText .= "Parsing {$fileName} - ";
                    $count++;
                    $startTime = microtime(true);
                    $match = $parser->parseLog($logfile);
                    $resultText .= $importer->importMatch($match);
                    $totalTime += microtime(true) - $startTime;
                    if (!empty($match->parseWarnings)) {
                        $resultText .= '<br>Warnings:<br>';
                        foreach ($match->parseWarnings as $warning) {
                            $resultText .= "<br>{$warning}";
                        }
                        $resultText .= '<br><br>';
                    }
                } catch (\Exception $e) {
                    $resultText .= "Failed ({$e->getMessage()})<br>";
                }
            }

            if ($count > 0) {
                $time = round($totalTime, 2);
                $resultText .= "<br>Processed {$count} log files in {$time} seconds";
                if ($time > 0) {
                    $perSecond = round($count / $time);
                    $perLog = round($time / $count, 3);
                    $resultText .= " (~{$perLog} seconds per log file / ~{$perSecond} log files per second)";
                }
            }
            $skipped = count($logfiles) - $count;
            if ($skipped > 0) {
                $resultText .= "<br>Skipped {$skipped} log files that are already imported";
            }
        } else {
            $resultText .= "Failed to open folder <code>{$folder}</code>";
        }

        return $this->render('admin/import.html.twig', [
            'import_output' => $resultText,
        ]);
    }
}
