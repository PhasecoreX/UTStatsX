<?php

namespace App\Controller;

use App\Entity\GameType;
use App\Entity\Map;
use App\Entity\MatchInfo;
use App\Entity\Player;
use App\Enum\EventPlayerType;
use App\Enum\KillAttribute;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class PublicController extends AbstractController
{
    private string $MATCHTIME = 'ceil(0.01 * (mi.endTime - mi.startTime) * mi.timeFactor)';
    private string $MATCHPLAYERTIME = 'ceil(0.01 * mp.time * mi.timeFactor)';

    #[Route('/', name: 'index')]
    public function index(EntityManagerInterface $em): Response
    {
        $totals = $em
            ->createQueryBuilder()
            ->select(
                'coalesce(sum(mp.score), 0) as score',
                'coalesce(sum(mp.kills), 0) as kills',
                'coalesce(sum(mp.suicides), 0) as suicides',
                'coalesce(sum(mp.teamKills), 0) as teamKills',
                "coalesce(sum({$this->MATCHPLAYERTIME}), 0) as matchplayertime",
            )
            ->from('App\Entity\MatchPlayer', 'mp')
            ->innerJoin('mp.matchInfo', 'mi')
            ->getQuery()
            ->getResult()[0];

        $totals['frags'] = $totals['kills'] - $totals['suicides'] - $totals['teamKills'];
        $totals['matches'] = $em->getRepository(MatchInfo::class)->count([]);
        $totals['players'] = $em->getRepository(Player::class)->count([]);

        $matchInfos = $this->getMatchListQueryBuilder($em)->setMaxResults(10)->getQuery()->getResult();

        return $this->render('public/index.html.twig', [
            'total' => $totals,
            'matchInfos' => $matchInfos,
        ]);
    }

    #[Route('/match', name: 'matchinfo_list')]
    public function matchInfoList(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request): Response
    {
        $player = null;
        $playerId = $request->query->getInt('player');
        if ($playerId > 0) {
            $player = $em->getRepository(Player::class)->find($playerId);
        }
        $query = $this->getMatchListQueryBuilder($em, $player)->getQuery();

        $pagination = $paginator->paginate(
            $query,  /* query NOT result */
            $request->query->getInt('page', 1),  /* page number */
            50  /* limit per page */
        );

        return $this->render('public/matchinfo_list.html.twig', [
            'pagination' => $pagination,
            'player' => $player,
        ]);
    }

    #[Route('/match/{hash}', name: 'matchinfo')]
    public function matchInfo(EntityManagerInterface $em, #[MapEntity(mapping: ['hash' => 'hash'])] MatchInfo $matchInfo): Response
    {
        $matchPlayersUnsortedQB = $this
            ->getMatchPlayerStatsQueryBuilder($em)
            ->addSelect(
                'p.name as name',
                'p.id as playerid',
                'mp.id as id',
                'mp.team as team',
            )
            ->where('mi = :match_info_id')
            ->setParameter('match_info_id', $matchInfo->getId());

        if ($matchInfo->getGameType()->getScoreboard() == 'lms') {
            $matchPlayersUnsortedQB
                ->addOrderBy('lives', 'desc')
                ->addOrderBy('matchplayertime', 'desc');
        }

        $matchPlayersUnsorted = $matchPlayersUnsortedQB
            ->addOrderBy('score', 'desc')
            ->addOrderBy('mp.wonMatch', 'desc')
            ->getQuery()
            ->getResult();

        // Get teams in score order
        $matchPlayersByTeam = array();
        $teamScore = array();
        foreach ($matchInfo->getTeamScores() as $team) {
            $matchPlayersByTeam[$team->getTeam()] = array();
            $teamScore[$team->getTeam()] = $team->getScore();
        }
        foreach ($matchPlayersUnsorted as $matchPlayer) {
            $matchPlayersByTeam[$matchPlayer['team']][$matchPlayer['id']] = $matchPlayer;
        }

        // Get list of match players ordered by team and score
        $matchPlayers = array();
        foreach ($matchPlayersByTeam as $teamMatchPlayers) {
            foreach ($teamMatchPlayers as $teamMatchPlayer) {
                $matchPlayers[] = $teamMatchPlayer;
            }
        }

        // Get kill matchup 2D array based ordered by team and score
        $killMatchup = array(array());
        foreach ($matchPlayers as $killer) {
            foreach ($matchPlayers as $victim) {
                $killMatchup[$killer['id']][$victim['id']] = 0;
            }
        }

        foreach ($matchInfo->getEventKills() as $kill) {
            $killMatchup[$kill->getMatchPlayer()->getId()][$kill->getVictim()->getId()]++;
        }
        foreach ($matchInfo->getEventSuicides() as $suicide) {
            $killMatchup[$suicide->getMatchPlayer()->getId()][$suicide->getMatchPlayer()->getId()]++;
        }

        $teamTotal = $this
            ->getMatchPlayerStatsTotalQueryBuilder($em)
            ->addSelect(
                'mp.team as team',
            )
            ->indexBy('mp', 'mp.team')
            ->where('mi = :match_info_id')
            ->setParameter('match_info_id', $matchInfo->getId())
            ->groupBy('mp.team')
            ->getQuery()
            ->getResult();

        return $this->render('public/matchinfo.html.twig', [
            'matchInfo' => $matchInfo,
            'matchPlayers' => $matchPlayers,
            'matchPlayersByTeam' => $matchPlayersByTeam,
            'teamScore' => $teamScore,
            'teamTotal' => $teamTotal,
            'killMatchup' => $killMatchup,
        ]);
    }

    #[Route('/player', name: 'player_list')]
    public function playerList(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request): Response
    {
        $query = $this
            ->getPlayerStatsTotalQueryBuilder($em)
            ->addSelect(
                'p.id as id',
                'p.name as name',
                'count(DISTINCT mp.matchInfo) as matches',
            )
            ->groupBy('p.id')
            ->orderBy('p.name', 'asc')
            ->getQuery();

        $pagination = $paginator->paginate(
            $query,  /* query NOT result */
            $request->query->getInt('page', 1),  /* page number */
            50,  /* limit per page */
            ['defaultSortDirection' => 'desc']
        );

        return $this->render('public/player_list.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route('/player/{id}', name: 'player')]
    public function player(EntityManagerInterface $em, Player $player): Response
    {
        $gametypeQB = $this
            ->getPlayerStatsTotalQueryBuilder($em)
            ->addSelect(
                'gt.name as gametype',
                'gt.scoreboard as scoreboard',
                'count(DISTINCT mp.matchInfo) as matches',
                'sum(mp.wonMatch) as wins',
                '100.0 * sum(mp.wonMatch) / count(DISTINCT mp.matchInfo) as winpercent',
            )
            ->innerJoin('mi.gameType', 'gt')
            ->where('p = :user_id')
            ->setParameter('user_id', $player->getId());

        $gametypeTotal = $gametypeQB->getQuery()->getResult()[0];

        $gametypes = $gametypeQB
            ->groupBy('gt.name')
            ->orderBy('gt.name', 'asc')
            ->getQuery()
            ->getResult();

        $scoreboard = array();
        foreach ($gametypes as $gametype) {
            $scoreboard[$gametype['scoreboard']] = true;
        }

        $queryResult = $em
            ->createQueryBuilder()
            ->select('ep.type, count(ep.type) as total')
            ->from('App\Entity\EventPlayer', 'ep', 'ep.type')
            ->innerJoin('ep.matchPlayer', 'mp')
            ->innerJoin('mp.player', 'p')
            ->where('p = :user_id')
            ->groupBy('ep.type')
            ->setParameter('user_id', $player->getId())
            ->getQuery()
            ->getResult();
        $playerEventTotal = array();
        foreach (EventPlayerType::cases() as $eventType) {
            $playerEventTotal[$eventType->value] = 0;
            if (isset($queryResult[$eventType->value])) {
                $playerEventTotal[$eventType->value] = $queryResult[$eventType->value]['total'];
            }
        }

        $queryResult = $em
            ->createQueryBuilder()
            ->select('eka.type, count(eka.type) as total')
            ->from('App\Entity\EventKillAttribute', 'eka', 'eka.type')
            ->innerJoin('eka.eventKill', 'ek')
            ->innerJoin('ek.matchPlayer', 'mp')
            ->innerJoin('mp.player', 'p')
            ->where('p = :user_id')
            ->groupBy('eka.type')
            ->setParameter('user_id', $player->getId())
            ->getQuery()
            ->getResult();
        $killAttributeTotal = array();
        foreach (KillAttribute::cases() as $attributeType) {
            $killAttributeTotal[$attributeType->value] = 0;
            if (isset($queryResult[$attributeType->value])) {
                $killAttributeTotal[$attributeType->value] = $queryResult[$attributeType->value]['total'];
            }
        }

        $matchInfos = $this
            ->getMatchListQueryBuilder($em, $player)
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();

        $weaponTotals = $em
            ->createQueryBuilder()
            ->select(
                'ek.matchPlayerWeapon as name',
                'count(ek) as kills',
            )
            ->from('App\Entity\EventKill', 'ek')
            ->innerJoin('ek.matchPlayer', 'killer')
            ->innerJoin('ek.victim', 'victim')
            ->innerJoin('killer.player', 'p')
            ->where('p = :user_id')
            ->andWhere('killer.team < 0 OR killer.team != victim.team')
            ->groupBy('ek.matchPlayerWeapon')
            ->orderBy('count(ek)', 'desc')
            ->setParameter('user_id', $player->getId())
            ->getQuery()
            ->getResult();

        $pickupTotals = $em
            ->createQueryBuilder()
            ->select(
                'pk.name as name',
                'sum(mppt.total) as total',
            )
            ->from('App\Entity\MatchPlayerPickupTotal', 'mppt')
            ->innerJoin('mppt.pickup', 'pk')
            ->innerJoin('mppt.matchPlayer', 'mp')
            ->innerJoin('mp.player', 'p')
            ->where('p = :user_id')
            ->groupBy('pk.name')
            ->orderBy('sum(mppt.total)', 'desc')
            ->setParameter('user_id', $player->getId())
            ->getQuery()
            ->getResult();

        return $this->render('public/player.html.twig', [
            'player' => $player,
            'gametypes' => $gametypes,
            'gametypeTotal' => $gametypeTotal,
            'playerEventTotal' => $playerEventTotal,
            'killAttributeTotal' => $killAttributeTotal,
            'scoreboard' => $scoreboard,
            'matchInfos' => $matchInfos,
            'weaponTotals' => $weaponTotals,
            'pickupTotals' => $pickupTotals,
        ]);
    }

    #[Route('/map', name: 'map_list')]
    public function mapList(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request): Response
    {
        $totalScoreSubQueryDQL = $em
            ->createQueryBuilder()
            ->select('sum(mp.score)')
            ->from('App\Entity\MatchPlayer', 'mp')
            ->innerJoin('mp.matchInfo', 'mi2')
            ->where('mi2.map = m.id')
            ->getDQL();

        $query = $em
            ->createQueryBuilder()
            ->select(
                'm.hash as hash',
                'm.name as name',
                'count(mi) as matches',
                "sum({$this->MATCHTIME}) as matchTime",
                'max(mi.date) as lastMatch',
                "({$totalScoreSubQueryDQL}) as totalScore",
            )
            ->from('App\Entity\Map', 'm')
            ->innerJoin('m.matchInfos', 'mi')
            ->groupBy('m.id')
            ->orderBy('m.name', 'asc')
            ->getQuery();

        $pagination = $paginator->paginate(
            $query,  /* query NOT result */
            $request->query->getInt('page', 1),  /* page number */
            50,  /* limit per page */
            ['defaultSortDirection' => 'desc']
        );

        return $this->render('public/map_list.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route('/map/{hash}', name: 'map')]
    public function map(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request, #[MapEntity(mapping: ['hash' => 'hash'])] Map $map): Response
    {
        $query = $this
            ->getMatchListQueryBuilder($em)
            ->where('m = :map_id')
            ->setParameter('map_id', $map)
            ->getQuery();

        $pagination = $paginator->paginate(
            $query,  /* query NOT result */
            $request->query->getInt('page', 1),  /* page number */
            50,  /* limit per page */
            ['defaultSortDirection' => 'desc']
        );

        $total = $this
            ->getMatchPlayerStatsTotalQueryBuilder($em)
            ->innerJoin('mi.map', 'm')
            ->where('m = :map_id')
            ->setParameter('map_id', $map)
            ->getQuery()
            ->getResult()[0];

        $total['matchTime'] = $em
            ->createQueryBuilder()
            ->select(
                "sum({$this->MATCHTIME}) as matchTime",
            )
            ->from('App\Entity\MatchInfo', 'mi')
            ->innerJoin('mi.map', 'm')
            ->where('m = :map_id')
            ->setParameter('map_id', $map)
            ->getQuery()
            ->getResult()[0]['matchTime'];

        return $this->render('public/map.html.twig', [
            'map' => $map,
            'total' => $total,
            'pagination' => $pagination,
        ]);
    }

    #[Route('/gametype', name: 'gametype_list')]
    public function gameTypeList(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request): Response
    {
        $totalScoreSubQueryDQL = $em
            ->createQueryBuilder()
            ->select('sum(mp.score)')
            ->from('App\Entity\MatchPlayer', 'mp')
            ->innerJoin('mp.matchInfo', 'mi2')
            ->where('mi2.gameType = gt.id')
            ->getDQL();

        $query = $em
            ->createQueryBuilder()
            ->select(
                'gt.slug as slug',
                'gt.name as name',
                'count(mi) as matches',
                "sum({$this->MATCHTIME}) as matchTime",
                'max(mi.date) as lastMatch',
                "({$totalScoreSubQueryDQL}) as totalScore",
            )
            ->from('App\Entity\GameType', 'gt')
            ->innerJoin('gt.matchInfos', 'mi')
            ->groupBy('gt.id')
            ->orderBy('gt.name', 'asc')
            ->getQuery();

        $pagination = $paginator->paginate(
            $query,  /* query NOT result */
            $request->query->getInt('page', 1),  /* page number */
            50,  /* limit per page */
            ['defaultSortDirection' => 'desc']
        );

        return $this->render('public/gametype_list.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route('/gametype/{slug}', name: 'gametype')]
    public function gameType(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request, #[MapEntity(mapping: ['slug' => 'slug'])] GameType $gameType): Response
    {
        $query = $this
            ->getMatchListQueryBuilder($em)
            ->where('gt = :gametype_id')
            ->setParameter('gametype_id', $gameType)
            ->getQuery();

        $pagination = $paginator->paginate(
            $query,  /* query NOT result */
            $request->query->getInt('page', 1),  /* page number */
            50,  /* limit per page */
            ['defaultSortDirection' => 'desc']
        );

        $total = $this
            ->getMatchPlayerStatsTotalQueryBuilder($em)
            ->innerJoin('mi.gameType', 'gt')
            ->where('gt = :gametype_id')
            ->setParameter('gametype_id', $gameType)
            ->getQuery()
            ->getResult()[0];

        $total['matchTime'] = $em
            ->createQueryBuilder()
            ->select(
                "sum({$this->MATCHTIME}) as matchTime",
            )
            ->from('App\Entity\MatchInfo', 'mi')
            ->innerJoin('mi.gameType', 'gt')
            ->where('gt = :gametype_id')
            ->setParameter('gametype_id', $gameType)
            ->getQuery()
            ->getResult()[0]['matchTime'];

        return $this->render('public/gametype.html.twig', [
            'gameType' => $gameType,
            'total' => $total,
            'pagination' => $pagination,
        ]);
    }

    #[Route('/totals', name: 'totals')]
    public function totals(EntityManagerInterface $em): Response
    {
        $mi2 = str_replace('mi.', 'mi2.', $this->MATCHTIME);

        $totalMatchTimeSubQueryDQL = $em
            ->createQueryBuilder()
            ->select("sum({$mi2})")
            ->from('App\Entity\MatchInfo', 'mi2')
            ->innerJoin('mi2.gameType', 'gt2')
            ->where('gt2 = gt')
            ->getDQL();

        $totalByGameType = $this
            ->getMatchPlayerStatsTotalQueryBuilder($em)
            ->addSelect(
                'gt.name as name',
                'gt.slug as slug',
                "({$totalMatchTimeSubQueryDQL}) as matchTime",
            )
            ->innerJoin('mi.gameType', 'gt')
            ->groupBy('gt.name')
            ->orderBy('gt.name', 'asc')
            ->getQuery()
            ->getResult();

        $total = $this
            ->getMatchPlayerStatsTotalQueryBuilder($em)
            ->getQuery()
            ->getResult()[0];

        $total['matchTime'] = $em
            ->createQueryBuilder()
            ->select(
                "sum({$this->MATCHTIME}) as matchTime",
            )
            ->from('App\Entity\MatchInfo', 'mi')
            ->getQuery()
            ->getResult()[0]['matchTime'];

        return $this->render('public/totals.html.twig', [
            'totalByGameType' => $totalByGameType,
            'total' => $total,
        ]);
    }

    private function getPlayerStatsTotalQueryBuilder(EntityManagerInterface $em)
    {
        return $em
            ->createQueryBuilder()
            ->select(
                'sum(mp.score) as score',
                'sum(mp.kills) - sum(mp.suicides) - sum(mp.teamKills) as frags',
                'sum(mp.kills) as kills',
                'sum(mp.deaths) as deaths',
                'sum(mp.suicides) as suicides',
                'sum(mp.teamKills) as teamKills',
                'sum(mp.teamDeaths) as teamDeaths',
                '100.0 * sum(mp.kills) / (CASE WHEN sum(mp.kills) != 0 THEN sum(mp.kills) ELSE 1 END + sum(mp.deaths) + sum(mp.suicides) + sum(mp.teamKills)) as efficiency',
                "1.0 * sum({$this->MATCHPLAYERTIME}) / (sum(mp.deaths) + sum(mp.suicides) + 1) as ttl",
                "sum({$this->MATCHPLAYERTIME}) as matchplayertime",
            )
            ->from('App\Entity\Player', 'p')
            ->innerJoin('p.matchPlayers', 'mp')
            ->innerJoin('mp.matchInfo', 'mi');
    }

    private function getMatchPlayerStatsQueryBuilder(EntityManagerInterface $em)
    {
        return $em
            ->createQueryBuilder()
            ->select(
                'mp.score as score',
                'mp.kills - mp.suicides - mp.teamKills as frags',
                'mp.kills as kills',
                'mp.deaths as deaths',
                'mp.suicides as suicides',
                'mp.teamKills as teamKills',
                'mp.teamDeaths as teamDeaths',
                'mi.fragLimit - mp.deaths - mp.teamDeaths - mp.suicides as lives',
                '100.0 * mp.kills / (CASE WHEN mp.kills != 0 THEN mp.kills ELSE 1 END + mp.deaths + mp.suicides + mp.teamKills) as efficiency',
                "1.0 * {$this->MATCHPLAYERTIME} / (mp.deaths + mp.suicides + 1) as ttl",
                "{$this->MATCHPLAYERTIME} as matchplayertime",
            )
            ->from('App\Entity\MatchPlayer', 'mp')
            ->innerJoin('mp.player', 'p')
            ->innerJoin('mp.matchInfo', 'mi');
    }

    private function getMatchPlayerStatsTotalQueryBuilder(EntityManagerInterface $em)
    {
        return $em
            ->createQueryBuilder()
            ->select(
                'coalesce(sum(mp.score), 0) as score',
                'coalesce(sum(mp.kills) - sum(mp.suicides) - sum(mp.teamKills), 0) as frags',
                'coalesce(sum(mp.kills), 0) as kills',
                'coalesce(sum(mp.deaths), 0) as deaths',
                'coalesce(sum(mp.suicides), 0) as suicides',
                'coalesce(sum(mp.teamKills), 0) as teamKills',
                'coalesce(sum(mp.teamDeaths), 0) as teamDeaths',
                'coalesce(sum(mi.fragLimit) - sum(mp.deaths) - sum(mp.teamDeaths) - sum(mp.suicides), 0) as lives',
                '100.0 * sum(mp.kills) / (CASE WHEN sum(mp.kills) != 0 THEN sum(mp.kills) ELSE 1 END + sum(mp.deaths) + sum(mp.suicides) + sum(mp.teamKills)) as efficiency',
                'count(DISTINCT mp.matchInfo) as matches',
            )
            ->from('App\Entity\MatchPlayer', 'mp')
            ->innerJoin('mp.player', 'p')
            ->innerJoin('mp.matchInfo', 'mi');
    }

    private function getMatchListQueryBuilder(EntityManagerInterface $em, Player $player = null)
    {
        $totalPlayersSubQueryDQL = $em
            ->createQueryBuilder()
            ->select('count(DISTINCT p2)')
            ->from('App\Entity\MatchPlayer', 'mp2')
            ->innerJoin('mp2.player', 'p2')
            ->where('mp2.matchInfo = mi')
            ->getDQL();

        $qb = $em
            ->createQueryBuilder()
            ->select(
                'mi.hash as hash',
                'mi.date as date',
                'gt.name as gametype',
                'm.name as map',
                "({$totalPlayersSubQueryDQL}) as players",
                "{$this->MATCHTIME} as matchTime",
            )
            ->from('App\Entity\MatchInfo', 'mi')
            ->innerJoin('mi.map', 'm')
            ->innerJoin('mi.gameType', 'gt')
            ->groupBy('mi.id')
            ->orderBy('mi.date', 'desc');

        if (isset($player)) {
            $qb
                ->addSelect('sum(mp.wonMatch) as wonMatch')
                ->innerJoin('mi.matchPlayers', 'mp')
                ->innerJoin('mp.player', 'p')
                ->where('p = :user_id')
                ->setParameter('user_id', $player->getId());
        }

        return $qb;
    }
}
