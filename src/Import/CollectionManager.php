<?php

namespace App\Import;

use Doctrine\ORM\EntityManagerInterface;

class CollectionManager
{
    private EntityManagerInterface $em;
    private $existingCollection = array();
    private $collectionClass;
    private $id = 0;

    public function __construct(EntityManagerInterface $em, $existingCollection, $collectionClass)
    {
        $this->em = $em;
        if (isset($existingCollection)) {
            $this->existingCollection = $existingCollection;
        }
        $this->collectionClass = $collectionClass;
    }

    public function __destruct()
    {
        // Delete all elements from the existingCollection that were not accessed
        for ($i = $this->id; $i < count($this->existingCollection); $i++) {
            $this->em->remove($this->existingCollection[$i]);
        }
    }

    public function getNext()
    {
        if (isset($this->existingCollection[$this->id])) {
            $element = $this->existingCollection[$this->id];
            $this->id++;
            return $element;
        } else {
            $newElement = new $this->collectionClass();
            $this->em->persist($newElement);
            return $newElement;
        }
    }
}
