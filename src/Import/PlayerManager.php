<?php

namespace App\Import;

use App\Entity\MatchInfo;
use App\Entity\MatchPlayer;
use App\Entity\Player;
use App\Parse\MatchData;
use Doctrine\ORM\EntityManagerInterface;

class PlayerManager
{
    private EntityManagerInterface $em;
    private MatchData $match;
    private MatchInfo $matchInfo;
    private $playerNameCache = array();  // name -> Player
    private $previousMatchPlayers = array(array());  // name / team -> MatchPlayer
    private $matchPlayerCache = array(array());  // match player ID / team -> MatchPlayer

    public function __construct(EntityManagerInterface $em, MatchData $match, MatchInfo $matchInfo)
    {
        $this->em = $em;
        $this->match = $match;
        $this->matchInfo = $matchInfo;

        $id = $this->matchInfo->getId();
        if (isset($id)) {
            // matchInfo was already in the database, so populate the caches
            $matchPlayers = $this
                ->em
                ->createQueryBuilder()
                ->select('mp, p')
                ->from('App\Entity\MatchPlayer', 'mp')
                ->innerJoin('mp.player', 'p')
                ->where('mp.matchInfo = :match_info')
                ->setParameter('match_info', $this->matchInfo)
                ->getQuery()
                ->getResult();
            foreach ($matchPlayers as $matchPlayer) {
                $playerName = $matchPlayer->getPlayer()->getName();
                $this->playerNameCache[$playerName] = $matchPlayer->getPlayer();
                $matchPlayer->setScore(0);
                $matchPlayer->setKills(0);
                $matchPlayer->setDeaths(0);
                $matchPlayer->setSuicides(0);
                $matchPlayer->setTeamkills(0);
                $matchPlayer->setTeamdeaths(0);
                $this->previousMatchPlayers[$playerName][$matchPlayer->getTeam()] = $matchPlayer;
            }
        }
    }

    public function __destruct()
    {
        // Delete all MatchPlayers that are no longer used
        foreach ($this->previousMatchPlayers as $matchPlayerTeam) {
            foreach ($matchPlayerTeam as $matchPlayer) {
                $this->match->logWarning("Removing matchPlayer {$matchPlayer->getId()} from database, please re-import log");
                $this->em->remove($matchPlayer);
            }
        }
    }

    public function getMatchPlayer(int $matchPlayerId, int $team)
    {
        if (isset($this->matchPlayerCache[$matchPlayerId][$team])) {
            return $this->matchPlayerCache[$matchPlayerId][$team];
        }

        if (!isset($this->match->player[$matchPlayerId])) {
            throw new Exception\LogImportError("Unknown matchPlayer {$matchPlayerId}. Maybe this map has a Titan or some other special pawn that UTStatsX doesn't know about?");
        }
        $playerInfo = $this->match->player[$matchPlayerId]->info;

        if (isset($this->previousMatchPlayers[$playerInfo->name][$team])) {
            $matchPlayer = $this->previousMatchPlayers[$playerInfo->name][$team];
            $this->matchPlayerCache[$matchPlayerId][$team] = $matchPlayer;
            unset($this->previousMatchPlayers[$playerInfo->name][$team]);
            return $matchPlayer;
        }

        $player = null;
        if (isset($this->playerNameCache[$playerInfo->name])) {
            $player = $this->playerNameCache[$playerInfo->name];
        } else {
            $player = $this->em->getRepository(Player::class)->findOneBy(['name' => $playerInfo->name]);
            if (!$player) {
                $player = new Player();
                $this->em->persist($player);
                $player->setName($playerInfo->name);
                $player->setLastMatchDate($this->match->startDate);
            }
            if ($this->match->startDate > $player->getLastMatchDate()) {
                $player->setLastMatchDate($this->match->startDate);
                $player->setFace($playerInfo->face);
                $player->setVoice($playerInfo->voice);
            }
            $this->playerNameCache[$playerInfo->name] = $player;
        }

        // If the player reconnects as a different ID, this will catch it
        foreach ($this->matchPlayerCache as $matchPlayerTeam) {
            if (isset($matchPlayerTeam[$team]) && $matchPlayerTeam[$team]->getPlayer() == $player) {
                $this->matchPlayerCache[$matchPlayerId][$team] = $matchPlayerTeam[$team];
                return $matchPlayerTeam[$team];
            }
        }

        $matchPlayer = new MatchPlayer();
        $this->em->persist($matchPlayer);
        $matchPlayer->setMatchInfo($this->matchInfo);
        $matchPlayer->setPlayer($player);
        $matchPlayer->setTeam($team);

        $this->matchPlayerCache[$matchPlayerId][$team] = $matchPlayer;
        return $matchPlayer;
    }

    public function allMatchPlayers()
    {
        foreach ($this->matchPlayerCache as $matchPlayerTeams) {
            foreach ($matchPlayerTeams as $matchPlayer) {
                yield $matchPlayer;
            }
        }
    }
}
