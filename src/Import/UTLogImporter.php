<?php

namespace App\Import;

use App\Entity\EventEntity;
use App\Entity\EventKill;
use App\Entity\EventKillAttribute;
use App\Entity\EventPlayer;
use App\Entity\EventSuicide;
use App\Entity\EventTeam;
use App\Entity\GameType;
use App\Entity\Map;
use App\Entity\MatchInfo;
use App\Entity\MatchMutator;
use App\Entity\MatchPlayerPickupTotal;
use App\Entity\Mutator;
use App\Entity\Pickup;
use App\Entity\Server;
use App\Entity\TeamScore;
use App\Enum\EventEntityType;
use App\Parse\MatchData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

class UTLogImporter
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function logHashExists(string $logFileHash)
    {
        $count = $this->em->getRepository(MatchInfo::class)->count(['logFileHash' => $logFileHash, 'dirty' => false]);
        return $count > 0;
    }

    public function importMatch(MatchData $match)
    {
        try {
            $this->em->getConnection()->beginTransaction();
            $resultText = $this->doImportMatch($match);
            $this->em->getConnection()->commit();
            return $resultText;
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->em->clear();
            throw $e;
        }
    }

    private function doImportMatch(MatchData $match)
    {
        $resultText = '';

        if (!isset($match->startDate)) {
            $resultText .= 'Ignored (match never started)<br>';
            return $resultText;
        }
        if (!isset($match->endTime)) {
            $resultText .= 'Ignored (match never ended)<br>';
            return $resultText;
        }

        // MatchInfo
        $matchHash = $match->info->getMatchHash();
        $matchInfo = $this->em->getRepository(MatchInfo::class)->findOneBy(['hash' => $matchHash]);
        if (!isset($matchInfo)) {
            $resultText .= "Inserting match {$matchHash}<br>";
            $matchInfo = new MatchInfo();
            $this->em->persist($matchInfo);
            $matchInfo->setHash($matchHash);
        } else {
            $resultText .= "Updating existing match {$matchHash}<br>";
        }
        $matchInfo->setLogFileHash($match->logFileHash);
        $matchInfo->setLogFileName($match->logFileName);
        $matchInfo->setDate($match->startDate);
        $matchInfo->setTimezone($match->info->timezone);
        $matchInfo->setTimeFactor($match->info->timeFactor);
        $matchInfo->setStartTime($match->startTime);
        $matchInfo->setEndTime($match->endTime);
        $matchInfo->setServerVersion($match->info->serverVersion);
        $matchInfo->setFragLimit($match->info->fragLimit);
        $matchInfo->setTimeLimit($match->info->timeLimit);
        $matchInfo->setTeamCount($match->info->teamCount);
        $matchInfo->setGoalTeamScore($match->info->goalTeamScore);
        $matchInfo->setAssaultGameCode($match->info->assaultGameCode);
        $matchInfo->setAssaultAttacker($match->info->assaultAttacker);
        $matchInfo->setAssaultDefender($match->info->assaultDefender);
        $matchInfo->setMatchNumber($match->info->matchNumber);

        // Team Scores
        $teamPlayerScores = array();
        $teamPlayerScores[-1] = 0;
        $cm = new CollectionManager($this->em, $matchInfo->getTeamScores(), TeamScore::class);
        foreach ($match->getTeamRankings() as $rank => $team) {
            $teamScore = $cm->getNext();
            $teamScore->setMatchInfo($matchInfo);
            $teamScore->setTeam($team['team']);
            $teamScore->setScore($team['score']);
            $teamScore->setRank($rank);

            $teamPlayerScores[$team['team']] = 0;
        }
        unset($cm);

        // Mutators
        $cm = new CollectionManager($this->em, $matchInfo->getMatchMutators(), MatchMutator::class);
        foreach ($match->info->mutators as $id => $matchMutatorName) {
            $matchMutator = $cm->getNext();
            $matchMutator->setMatchInfo($matchInfo);
            $matchMutator->setParseOrder($id);

            $mutator = $matchMutator->getMutator();
            if (!isset($mutator) || $matchMutatorName != $mutator->getName()) {
                $mutator = $this->em->getRepository(Mutator::class)->findOneBy(['name' => $matchMutatorName]);
                if (!isset($mutator)) {
                    $mutator = new Mutator();
                    $this->em->persist($mutator);
                    $mutator->setName($matchMutatorName);
                    $mutator->setDisplayName($matchMutatorName);
                    $mutator->setHidden(false);
                }
                $matchMutator->setMutator($mutator);
            }
        }
        unset($cm);

        // Server
        $server = $matchInfo->getServer();
        $serverName = $match->info->serverName;
        if (!isset($server) || $serverName != $server->getName()) {
            $server = $this->em->getRepository(Server::class)->findOneBy(['name' => $serverName]);
            if (!isset($server)) {
                $server = new Server();
                $this->em->persist($server);
                $server->setName($serverName);
                $server->setLastUpdated($match->startDate);
            }
            $matchInfo->setServer($server);
        }
        if ($match->startDate > $server->getLastUpdated()) {
            $server->setLastUpdated($match->startDate);
            $server->setShortName($match->info->shortName);
            $server->setAdminName($match->info->adminName);
            $server->setAdminEmail($match->info->adminEmail);
            $server->setVersion($match->info->serverVersion);
        }

        // Gametype
        $gameType = $matchInfo->getGameType();
        $gameName = $match->info->gameName;
        $slugger = new AsciiSlugger();
        $gameTypeSlug = $slugger->slug(strtolower($gameName));
        if (!isset($gameType) || $gameTypeSlug != $gameType->getSlug()) {
            $gameType = $this->em->getRepository(GameType::class)->findOneBy(['slug' => $gameTypeSlug]);
            if (!isset($gameType)) {
                $gameType = new GameType();
                $this->em->persist($gameType);
                $gameType->setSlug($gameTypeSlug);
                $gameType->setName($gameName);
            }
            $matchInfo->setGameType($gameType);
        }
        $scoreboardType = strtolower($match->info->gameClass);
        $scoreboardType = preg_replace('/^excessive\w*-(\w+)\.excessive/', '${1}.', $scoreboardType);
        $scoreboardType = preg_replace('/^excessive\w*\.excessive/', 'botpack.', $scoreboardType);

        switch ($scoreboardType) {
            case 'botpack.assault':
                {
                    $gameType->setScoreboard('as');
                }
                break;
            case 'botpack.ctfgame':
            case 'ctf4.ctf4game':
                {
                    $gameType->setScoreboard('ctf');
                }
                break;
            case 'botpack.domination':
            case 'doubledomination.doubledomination':
                {
                    $gameType->setScoreboard('dom');
                }
                break;
            case 'jailbreak.jailbreak':
                {
                    $gameType->setScoreboard('jb');
                }
                break;
            case 'botpack.lastmanstanding':
            case 'tlastmanstanding.tlastmanstanding':
            case 'teamlmslite.teamlmslite':
                {
                    $gameType->setScoreboard('lms');
                }
                break;
            case 'monsterhunt.monsterhunt':
            case 'monsterhunt.monsterhuntarena':
            case 'monsterhunt.monsterhuntdefence':
                {
                    $gameType->setScoreboard('mh');
                }
                break;
            default:
                break;
        }

        // Map
        $map = $matchInfo->getMap();
        $mapHash = $match->info->getMapHash();
        if (!isset($map) || $mapHash != $map->getHash()) {
            $map = $this->em->getRepository(Map::class)->findOneBy(['hash' => $mapHash]);
            if (!isset($map)) {
                $map = new Map();
                $this->em->persist($map);
                $map->setHash($mapHash);
                $map->setName($match->info->mapName);
                $map->setTitle($match->info->mapTitle);
                $map->setAuthor($match->info->mapAuthor);
            }
            $matchInfo->setMap($map);
        }

        $pm = new PlayerManager($this->em, $match, $matchInfo);

        // EventPlayer
        $cm = new CollectionManager($this->em, $matchInfo->getEventPlayers(), EventPlayer::class);
        foreach ($match->getPlayerEvents() as $event) {
            $eventPlayer = $cm->getNext();
            $eventPlayer->setMatchInfo($matchInfo);
            $player = $pm->getMatchPlayer($event->player, $event->playerTeam);
            $eventPlayer->setTime($event->time);
            $eventPlayer->setType($event->type);
            $eventPlayer->setMatchPlayer($player);
            $eventPlayer->setTargetId($event->targetId);
            $eventPlayer->setTargetStr($event->targetStr);
            $eventPlayer->setPlayerScore($event->playerScore);
            $eventPlayer->setPlayerTeamScore($event->playerTeamScore);

            // Update MatchPlayer totals
            $player->incrementScore($event->playerScore);
        }
        unset($cm);

        // EventEntity
        $cm = new CollectionManager($this->em, $matchInfo->getEventEntities(), EventEntity::class);
        foreach ($match->getEntityEvents() as $event) {
            $eventEntity = $cm->getNext();
            $eventEntity->setMatchInfo($matchInfo);
            $player = $pm->getMatchPlayer($event->player, $event->playerTeam);
            $eventEntity->setTime($event->time);
            $eventEntity->setType($event->type);
            $eventEntity->setMatchPlayer($player);
            $eventEntity->setEntityName($event->entityName);

            // Update MatchPlayer totals
            $player->incrementScore($event->playerScore);
            if ($event->type == EventEntityType::PlayerKillEntity) {
                $player->incrementKills(1);
            } elseif ($event->type == EventEntityType::EntityKillPlayer) {
                $player->incrementDeaths(1);
            }
        }
        unset($cm);

        // EventTeam
        $cm = new CollectionManager($this->em, $matchInfo->getEventTeams(), EventTeam::class);
        foreach ($match->getTeamEvents() as $event) {
            $eventTeam = $cm->getNext();
            $eventTeam->setMatchInfo($matchInfo);
            $eventTeam->setTime($event->time);
            $eventTeam->setType($event->type);
            $eventTeam->setTeam($event->team);
            $eventTeam->setScore($event->score);
        }
        unset($cm);

        // EventKill
        $cm = new CollectionManager($this->em, $matchInfo->getEventKills(), EventKill::class);
        foreach ($match->getKillEvents() as $event) {
            $eventKill = $cm->getNext();
            $eventKill->setMatchInfo($matchInfo);
            $player = $pm->getMatchPlayer($event->player, $event->playerTeam);
            $victim = $pm->getMatchPlayer($event->victim, $event->victimTeam);
            $eventKill->setTime($event->time);
            $eventKill->setMatchPlayer($player);
            $eventKill->setMatchPlayerWeapon($event->playerWeapon);
            $eventKill->setVictim($victim);
            $eventKill->setVictimWeapon($event->victimWeapon);
            $eventKill->setDamageType($event->damageType);
            $eventKill->setPlayerScore($event->playerScore);
            $eventKill->setPlayerTeamScore($event->playerTeamScore);

            // EventKill attributes
            $cma = new CollectionManager($this->em, $eventKill->getEventKillAttributes(), EventKillAttribute::class);
            foreach ($event->attributes as $attribute) {
                $eventKillAttribute = $cma->getNext();
                $eventKillAttribute->setEventKill($eventKill);
                $eventKillAttribute->setType($attribute);
            }
            unset($cma);

            // Update MatchPlayer totals
            $player->incrementScore($event->playerScore);
            if ($player == $victim) {
                throw new Exception\LogImportError("EventKill table doesn't allow the killer and victim to be the same MatchPlayer.");
            } elseif ($player->getTeam() > -1 && $player->getTeam() == $victim->getTeam()) {
                $player->incrementTeamkills(1);
                $victim->incrementTeamdeaths(1);
            } else {
                $player->incrementKills(1);
                $victim->incrementDeaths(1);
            }
        }
        unset($cm);

        // EventSuicide
        $cm = new CollectionManager($this->em, $matchInfo->getEventSuicides(), EventSuicide::class);
        foreach ($match->getSuicideEvents() as $event) {
            $eventSuicide = $cm->getNext();
            $eventSuicide->setMatchInfo($matchInfo);
            $player = $pm->getMatchPlayer($event->player, $event->playerTeam);
            $eventSuicide->setTime($event->time);
            $eventSuicide->setMatchPlayer($player);
            $eventSuicide->setMatchPlayerWeapon($event->playerWeapon);
            $eventSuicide->setDamageType($event->damageType);
            $eventSuicide->setPlayerScore($event->playerScore);
            $eventSuicide->setPlayerTeamScore($event->playerTeamScore);

            // Update MatchPlayer totals
            $player->incrementScore($event->playerScore);
            $player->incrementSuicides(1);
        }
        unset($cm);

        $checkedPlayers = array();
        $winnerSet = false;
        $insertedPickups = array();
        foreach ($pm->allMatchPlayers() as $matchPlayer) {
            $playerName = $matchPlayer->getPlayer()->getName();
            $playerTeam = $matchPlayer->getTeam();
            $parsePlayer = $match->getPlayerByName($playerName);

            // Set win/lose status
            $wonMatch = $match->didPlayerWinMatch($playerName, $playerTeam);
            $winnerSet = $winnerSet || $wonMatch;
            $matchPlayer->setWonMatch($wonMatch);

            // Set time
            $matchPlayer->setTime($parsePlayer->getCentisecondsOnServer($playerTeam));

            // Count pickups
            $cm = new CollectionManager($this->em, $matchPlayer->getMatchPlayerPickupTotals(), MatchPlayerPickupTotal::class);
            foreach ($parsePlayer->getPickupTotals($playerTeam) as $pickupName => $total) {
                $matchPlayerPickupTotal = $cm->getNext();
                $matchPlayerPickupTotal->setMatchPlayer($matchPlayer);
                $matchPlayerPickupTotal->setTotal($total);

                $pickup = $matchPlayerPickupTotal->getPickup();
                if (!isset($pickup) || $pickupName != $pickup->getName()) {
                    $pickup = $this->em->getRepository(Pickup::class)->findOneBy(['name' => $pickupName]);
                    if (!isset($pickup)) {
                        if (isset($insertedPickups[$pickupName])) {
                            $pickup = $insertedPickups[$pickupName];
                        } else {
                            $pickup = new Pickup();
                            $this->em->persist($pickup);
                            $pickup->setName($pickupName);
                            $pickup->setDisplayName($pickupName);
                            $pickup->setHidden(false);
                            $insertedPickups[$pickupName] = $pickup;
                        }
                    }
                    $matchPlayerPickupTotal->setPickup($pickup);
                }
            }
            unset($cm);

            // Check our work
            $checkedPlayerKey = "{$playerName}|{$playerTeam}";
            if (isset($checkedPlayers[$checkedPlayerKey])) {
                continue;
            }
            $checkedPlayers[$checkedPlayerKey] = true;

            // Score
            $calculated = $matchPlayer->getScore();
            $teamPlayerScores[$playerTeam] += $calculated;
            $expected = $parsePlayer->getTotalScore($playerTeam);
            if ($calculated != $expected) {
                $match->logWarning("{$playerName} on team {$playerTeam} imported {$calculated} score, but parser expected {$expected}");
            }

            // Frags
            $calculated = $matchPlayer->getKills() - $matchPlayer->getSuicides();
            $expected = $parsePlayer->getTotalFrags($playerTeam);
            if ($calculated != $expected) {
                $match->logWarning("{$playerName} on team {$playerTeam} imported {$calculated} frags, but parser expected {$expected}");
            }

            // Kills
            $calculated = $matchPlayer->getKills();
            $expected = $parsePlayer->getTotalKills($playerTeam);
            if ($calculated != $expected) {
                $match->logWarning("{$playerName} on team {$playerTeam} imported {$calculated} kills, but parser expected {$expected}");
            }

            // Deaths
            $calculated = $matchPlayer->getDeaths();
            $expected = $parsePlayer->getTotalDeaths($playerTeam);
            if ($calculated != $expected) {
                $match->logWarning("{$playerName} on team {$playerTeam} imported {$calculated} deaths, but parser expected {$expected}");
            }

            // Suicides
            $calculated = $matchPlayer->getSuicides();
            $expected = $parsePlayer->getTotalSuicides($playerTeam);
            if ($calculated != $expected) {
                $match->logWarning("{$playerName} on team {$playerTeam} imported {$calculated} suicides, but parser expected {$expected}");
            }

            // TeamKills
            $calculated = $matchPlayer->getTeamKills();
            $expected = $parsePlayer->getTotalTeamKills($playerTeam);
            if ($calculated != $expected) {
                $match->logWarning("{$playerName} on team {$playerTeam} imported {$calculated} team kills, but parser expected {$expected}");
            }

            // TeamDeaths
            $calculated = $matchPlayer->getTeamDeaths();
            $expected = $parsePlayer->getTotalTeamDeaths($playerTeam);
            if ($calculated != $expected) {
                $match->logWarning("{$playerName} on team {$playerTeam} imported {$calculated} team deaths, but parser expected {$expected}");
            }
        }

        if (!$winnerSet) {
            $match->logWarning('Could not determine winner');
        }

        // Check team player score totals
        foreach ($teamPlayerScores as $teamId => $calculated) {
            $expected = $match->getTeamPlayerScoreTotal($teamId);
            if ($calculated != $expected) {
                $match->logWarning("Team {$teamId} imported {$calculated} player team score, but parser expected {$expected}");
            }
        }

        // Cleanup player manager
        unset($pm);

        // Set dirty flag
        $matchInfo->setDirty(count($match->parseWarnings) > 0);

        $this->em->flush();
        $this->em->clear();
        return $resultText;
    }
}
