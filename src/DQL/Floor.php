<?php

namespace App\DQL;

use Doctrine\DBAL\Platforms\SqlitePlatform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\TokenType;

class Floor extends FunctionNode
{
    private $arithmeticExpression;

    public function parse(\Doctrine\ORM\Query\Parser $parser): void
    {
        $parser->match(TokenType::T_IDENTIFIER);
        $parser->match(TokenType::T_OPEN_PARENTHESIS);

        $this->arithmeticExpression = $parser->ArithmeticExpression();

        $parser->match(TokenType::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker): string
    {
        $sqlString = $sqlWalker->walkArithmeticExpression($this->arithmeticExpression);
        if ($sqlWalker->getConnection()->getDatabasePlatform() instanceof SqlitePlatform) {
            return "(cast({$sqlString} as int) - ({$sqlString} < cast({$sqlString} as int)))";
        }
        return "FLOOR({$sqlString})";
    }
}
