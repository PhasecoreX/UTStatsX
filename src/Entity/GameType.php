<?php

namespace App\Entity;

use App\Repository\GameTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameTypeRepository::class)]
#[ORM\Index(name: 'gametype_slug_idx', columns: ['slug'])]
class GameType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $scoreboard = null;

    #[ORM\OneToMany(mappedBy: 'gameType', targetEntity: MatchInfo::class)]
    private Collection $matchInfos;

    public function __construct()
    {
        $this->matchInfos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getScoreboard(): ?string
    {
        return $this->scoreboard;
    }

    public function setScoreboard(?string $scoreboard): static
    {
        $this->scoreboard = $scoreboard;

        return $this;
    }

    /**
     * @return Collection<int, MatchInfo>
     */
    public function getMatchInfos(): Collection
    {
        return $this->matchInfos;
    }

    public function addMatchInfo(MatchInfo $matchInfo): static
    {
        if (!$this->matchInfos->contains($matchInfo)) {
            $this->matchInfos->add($matchInfo);
            $matchInfo->setGameType($this);
        }

        return $this;
    }

    public function removeMatchInfo(MatchInfo $matchInfo): static
    {
        if ($this->matchInfos->removeElement($matchInfo)) {
            // set the owning side to null (unless already changed)
            if ($matchInfo->getGameType() === $this) {
                $matchInfo->setGameType(null);
            }
        }

        return $this;
    }
}
