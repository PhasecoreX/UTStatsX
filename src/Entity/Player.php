<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PlayerRepository::class)]
#[ORM\Index(name: 'player_name_idx', columns: ['name'])]
class Player
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'datetime_immutable_micro')]
    private $lastMatchDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $face = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $voice = null;

    #[ORM\OneToMany(mappedBy: 'player', targetEntity: MatchPlayer::class)]
    private Collection $matchPlayers;

    public function __construct()
    {
        $this->matchPlayers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getLastMatchDate()
    {
        return $this->lastMatchDate;
    }

    public function setLastMatchDate($lastMatchDate): static
    {
        $this->lastMatchDate = $lastMatchDate;

        return $this;
    }

    public function getFace(): ?string
    {
        return $this->face;
    }

    public function setFace(?string $face): static
    {
        $this->face = $face;

        return $this;
    }

    public function getVoice(): ?string
    {
        return $this->voice;
    }

    public function setVoice(?string $voice): static
    {
        $this->voice = $voice;

        return $this;
    }

    /**
     * @return Collection<int, MatchPlayer>
     */
    public function getMatchPlayers(): Collection
    {
        return $this->matchPlayers;
    }

    public function addMatchPlayer(MatchPlayer $matchPlayer): static
    {
        if (!$this->matchPlayers->contains($matchPlayer)) {
            $this->matchPlayers->add($matchPlayer);
            $matchPlayer->setPlayer($this);
        }

        return $this;
    }

    public function removeMatchPlayer(MatchPlayer $matchPlayer): static
    {
        if ($this->matchPlayers->removeElement($matchPlayer)) {
            // set the owning side to null (unless already changed)
            if ($matchPlayer->getPlayer() === $this) {
                $matchPlayer->setPlayer(null);
            }
        }

        return $this;
    }
}
