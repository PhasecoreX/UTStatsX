<?php

namespace App\Entity;

use App\Repository\MatchPlayerPickupTotalRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MatchPlayerPickupTotalRepository::class)]
class MatchPlayerPickupTotal
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'matchPlayerPickupTotals')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchPlayer $matchPlayer = null;

    #[ORM\ManyToOne(inversedBy: 'matchPlayerPickupTotals')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Pickup $pickup = null;

    #[ORM\Column]
    private ?int $total = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchPlayer(): ?MatchPlayer
    {
        return $this->matchPlayer;
    }

    public function setMatchPlayer(?MatchPlayer $matchPlayer): static
    {
        $this->matchPlayer = $matchPlayer;

        return $this;
    }

    public function getPickup(): ?Pickup
    {
        return $this->pickup;
    }

    public function setPickup(?Pickup $pickup): static
    {
        $this->pickup = $pickup;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): static
    {
        $this->total = $total;

        return $this;
    }
}
