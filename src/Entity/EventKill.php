<?php

namespace App\Entity;

use App\Repository\EventKillRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventKillRepository::class)]
class EventKill
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'eventKills')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchInfo $matchInfo = null;

    #[ORM\Column]
    private ?int $time = null;

    #[ORM\ManyToOne(inversedBy: 'eventKills')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchPlayer $matchPlayer = null;

    #[ORM\Column(length: 255)]
    private ?string $matchPlayerWeapon = null;

    #[ORM\ManyToOne(inversedBy: 'eventDeaths')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchPlayer $victim = null;

    #[ORM\Column(length: 255)]
    private ?string $victimWeapon = null;

    #[ORM\Column(length: 255)]
    private ?string $damageType = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $playerScore = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $playerTeamScore = null;

    #[ORM\OneToMany(mappedBy: 'eventKill', targetEntity: EventKillAttribute::class, fetch: 'EAGER')]
    private Collection $eventKillAttributes;

    public function __construct()
    {
        $this->eventKillAttributes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchInfo(): ?MatchInfo
    {
        return $this->matchInfo;
    }

    public function setMatchInfo(?MatchInfo $matchInfo): static
    {
        $this->matchInfo = $matchInfo;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): static
    {
        $this->time = $time;

        return $this;
    }

    public function getMatchPlayer(): ?MatchPlayer
    {
        return $this->matchPlayer;
    }

    public function setMatchPlayer(?MatchPlayer $matchPlayer): static
    {
        $this->matchPlayer = $matchPlayer;

        return $this;
    }

    public function getMatchPlayerWeapon(): ?string
    {
        return $this->matchPlayerWeapon;
    }

    public function setMatchPlayerWeapon(string $matchPlayerWeapon): static
    {
        $this->matchPlayerWeapon = $matchPlayerWeapon;

        return $this;
    }

    public function getVictim(): ?MatchPlayer
    {
        return $this->victim;
    }

    public function setVictim(?MatchPlayer $victim): static
    {
        $this->victim = $victim;

        return $this;
    }

    public function getVictimWeapon(): ?string
    {
        return $this->victimWeapon;
    }

    public function setVictimWeapon(string $victimWeapon): static
    {
        $this->victimWeapon = $victimWeapon;

        return $this;
    }

    public function getDamageType(): ?string
    {
        return $this->damageType;
    }

    public function setDamageType(string $damageType): static
    {
        $this->damageType = $damageType;

        return $this;
    }

    public function getPlayerScore(): ?int
    {
        return $this->playerScore;
    }

    public function setPlayerScore(int $playerScore): static
    {
        $this->playerScore = $playerScore;

        return $this;
    }

    public function getPlayerTeamScore(): ?int
    {
        return $this->playerTeamScore;
    }

    public function setPlayerTeamScore(int $playerTeamScore): static
    {
        $this->playerTeamScore = $playerTeamScore;

        return $this;
    }

    /**
     * @return Collection<int, EventKillAttribute>
     */
    public function getEventKillAttributes(): Collection
    {
        return $this->eventKillAttributes;
    }

    public function addEventKillAttribute(EventKillAttribute $eventKillAttribute): static
    {
        if (!$this->eventKillAttributes->contains($eventKillAttribute)) {
            $this->eventKillAttributes->add($eventKillAttribute);
            $eventKillAttribute->setEventKill($this);
        }

        return $this;
    }

    public function removeEventKillAttribute(EventKillAttribute $eventKillAttribute): static
    {
        if ($this->eventKillAttributes->removeElement($eventKillAttribute)) {
            // set the owning side to null (unless already changed)
            if ($eventKillAttribute->getEventKill() === $this) {
                $eventKillAttribute->setEventKill(null);
            }
        }

        return $this;
    }
}
