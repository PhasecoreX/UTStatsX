<?php

namespace App\Entity;

use App\Enum\EventEntityType;
use App\Repository\EventEntityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventEntityRepository::class)]
class EventEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'eventEntities')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchInfo $matchInfo = null;

    #[ORM\Column]
    private ?int $time = null;

    #[ORM\Column(length: 255, type: 'string', enumType: EventEntityType::class)]
    private ?EventEntityType $type = null;

    #[ORM\ManyToOne(inversedBy: 'eventEntities')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchPlayer $matchPlayer = null;

    #[ORM\Column(length: 255)]
    private ?string $entityName = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchInfo(): ?MatchInfo
    {
        return $this->matchInfo;
    }

    public function setMatchInfo(?MatchInfo $matchInfo): static
    {
        $this->matchInfo = $matchInfo;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): static
    {
        $this->time = $time;

        return $this;
    }

    public function getType(): ?EventEntityType
    {
        return $this->type;
    }

    public function setType(EventEntityType $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getMatchPlayer(): ?MatchPlayer
    {
        return $this->matchPlayer;
    }

    public function setMatchPlayer(?MatchPlayer $matchPlayer): static
    {
        $this->matchPlayer = $matchPlayer;

        return $this;
    }

    public function getEntityName(): ?string
    {
        return $this->entityName;
    }

    public function setEntityName(string $entityName): static
    {
        $this->entityName = $entityName;

        return $this;
    }
}
