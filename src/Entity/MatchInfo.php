<?php

namespace App\Entity;

use App\Repository\MatchInfoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MatchInfoRepository::class)]
#[ORM\Index(name: 'match_info_hash_idx', columns: ['hash'])]
#[ORM\Index(name: 'match_info_log_file_hash_idx', columns: ['log_file_hash'])]
class MatchInfo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 32)]
    private ?string $hash = null;

    #[ORM\Column(length: 32)]
    private ?string $logFileHash = null;

    #[ORM\Column(length: 255)]
    private ?string $logFileName = null;

    #[ORM\Column]
    private ?bool $dirty = null;

    #[ORM\ManyToOne(inversedBy: 'matchInfos', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Server $server = null;

    #[ORM\ManyToOne(inversedBy: 'matchInfos', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private ?GameType $gameType = null;

    #[ORM\ManyToOne(inversedBy: 'matchInfos', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Map $map = null;

    #[ORM\Column(type: 'datetime_immutable_micro')]
    private $date = null;

    #[ORM\Column(length: 255)]
    private ?string $timezone = null;

    #[ORM\Column]
    private ?float $timeFactor = null;

    #[ORM\Column]
    private ?int $startTime = null;

    #[ORM\Column]
    private ?int $endTime = null;

    #[ORM\Column(length: 8, nullable: true)]
    private ?string $serverVersion = null;

    #[ORM\Column]
    private ?int $fragLimit = null;

    #[ORM\Column]
    private ?int $timeLimit = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $teamCount = null;

    #[ORM\Column]
    private ?int $goalTeamScore = null;

    #[ORM\Column(length: 8, nullable: true)]
    private ?string $assaultGameCode = null;

    #[ORM\Column(nullable: true, type: Types::SMALLINT)]
    private ?int $assaultAttacker = null;

    #[ORM\Column(nullable: true, type: Types::SMALLINT)]
    private ?int $assaultDefender = null;

    #[ORM\Column]
    private ?int $matchNumber = null;

    #[ORM\OneToMany(mappedBy: 'matchInfo', targetEntity: MatchPlayer::class)]
    private Collection $matchPlayers;

    #[ORM\OneToMany(mappedBy: 'matchInfo', targetEntity: EventEntity::class)]
    private Collection $eventEntities;

    #[ORM\OneToMany(mappedBy: 'matchInfo', targetEntity: EventKill::class)]
    private Collection $eventKills;

    #[ORM\OneToMany(mappedBy: 'matchInfo', targetEntity: EventPlayer::class)]
    private Collection $eventPlayers;

    #[ORM\OneToMany(mappedBy: 'matchInfo', targetEntity: EventSuicide::class)]
    private Collection $eventSuicides;

    #[ORM\OneToMany(mappedBy: 'matchInfo', targetEntity: EventTeam::class)]
    private Collection $eventTeams;

    #[ORM\OneToMany(mappedBy: 'matchInfo', targetEntity: MatchMutator::class)]
    #[ORM\OrderBy(['parseOrder' => 'ASC'])]
    private Collection $matchMutators;

    #[ORM\OneToMany(mappedBy: 'matchInfo', targetEntity: TeamScore::class)]
    #[ORM\OrderBy(['rank' => 'ASC'])]
    private Collection $teamScores;

    public function __construct()
    {
        $this->matchPlayers = new ArrayCollection();
        $this->eventEntities = new ArrayCollection();
        $this->eventKills = new ArrayCollection();
        $this->eventPlayers = new ArrayCollection();
        $this->eventSuicides = new ArrayCollection();
        $this->eventTeams = new ArrayCollection();
        $this->matchMutators = new ArrayCollection();
        $this->teamScores = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): static
    {
        $this->hash = $hash;

        return $this;
    }

    public function getLogFileHash(): ?string
    {
        return $this->logFileHash;
    }

    public function setLogFileHash(string $logFileHash): static
    {
        $this->logFileHash = $logFileHash;

        return $this;
    }

    public function getLogFileName(): ?string
    {
        return $this->logFileName;
    }

    public function setLogFileName(string $logFileName): static
    {
        $this->logFileName = $logFileName;

        return $this;
    }

    public function isDirty(): ?bool
    {
        return $this->dirty;
    }

    public function setDirty(bool $dirty): static
    {
        $this->dirty = $dirty;

        return $this;
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }

    public function setServer(?Server $server): static
    {
        $this->server = $server;

        return $this;
    }

    public function getGameType(): ?GameType
    {
        return $this->gameType;
    }

    public function setGameType(?GameType $gameType): static
    {
        $this->gameType = $gameType;

        return $this;
    }

    public function getMap(): ?Map
    {
        return $this->map;
    }

    public function setMap(?Map $map): static
    {
        $this->map = $map;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): static
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getTimeFactor(): ?float
    {
        return $this->timeFactor;
    }

    public function setTimeFactor(float $timeFactor): static
    {
        $this->timeFactor = $timeFactor;

        return $this;
    }

    public function getStartTime(): ?int
    {
        return $this->startTime;
    }

    public function setStartTime(int $startTime): static
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?int
    {
        return $this->endTime;
    }

    public function setEndTime(int $endTime): static
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getServerVersion(): ?string
    {
        return $this->serverVersion;
    }

    public function setServerVersion(?string $serverVersion): static
    {
        $this->serverVersion = $serverVersion;

        return $this;
    }

    public function getFragLimit(): ?int
    {
        return $this->fragLimit;
    }

    public function setFragLimit(int $fragLimit): static
    {
        $this->fragLimit = $fragLimit;

        return $this;
    }

    public function getTimeLimit(): ?int
    {
        return $this->timeLimit;
    }

    public function setTimeLimit(int $timeLimit): static
    {
        $this->timeLimit = $timeLimit;

        return $this;
    }

    public function getTeamCount(): ?int
    {
        return $this->teamCount;
    }

    public function setTeamCount(int $teamCount): static
    {
        $this->teamCount = $teamCount;

        return $this;
    }

    public function getGoalTeamScore(): ?int
    {
        return $this->goalTeamScore;
    }

    public function setGoalTeamScore(int $goalTeamScore): static
    {
        $this->goalTeamScore = $goalTeamScore;

        return $this;
    }

    public function getAssaultGameCode(): ?string
    {
        return $this->assaultGameCode;
    }

    public function setAssaultGameCode(?string $assaultGameCode): static
    {
        $this->assaultGameCode = $assaultGameCode;

        return $this;
    }

    public function getAssaultAttacker(): ?int
    {
        return $this->assaultAttacker;
    }

    public function setAssaultAttacker(?int $assaultAttacker): static
    {
        $this->assaultAttacker = $assaultAttacker;

        return $this;
    }

    public function getAssaultDefender(): ?int
    {
        return $this->assaultDefender;
    }

    public function setAssaultDefender(?int $assaultDefender): static
    {
        $this->assaultDefender = $assaultDefender;

        return $this;
    }

    public function getMatchNumber(): ?int
    {
        return $this->matchNumber;
    }

    public function setMatchNumber(int $matchNumber): static
    {
        $this->matchNumber = $matchNumber;

        return $this;
    }

    /**
     * @return Collection<int, MatchPlayer>
     */
    public function getMatchPlayers(): Collection
    {
        return $this->matchPlayers;
    }

    public function addMatchPlayer(MatchPlayer $matchPlayer): static
    {
        if (!$this->matchPlayers->contains($matchPlayer)) {
            $this->matchPlayers->add($matchPlayer);
            $matchPlayer->setMatchInfo($this);
        }

        return $this;
    }

    public function removeMatchPlayer(MatchPlayer $matchPlayer): static
    {
        if ($this->matchPlayers->removeElement($matchPlayer)) {
            // set the owning side to null (unless already changed)
            if ($matchPlayer->getMatchInfo() === $this) {
                $matchPlayer->setMatchInfo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventEntity>
     */
    public function getEventEntities(): Collection
    {
        return $this->eventEntities;
    }

    public function addEventEntity(EventEntity $eventEntity): static
    {
        if (!$this->eventEntities->contains($eventEntity)) {
            $this->eventEntities->add($eventEntity);
            $eventEntity->setMatchInfo($this);
        }

        return $this;
    }

    public function removeEventEntity(EventEntity $eventEntity): static
    {
        if ($this->eventEntities->removeElement($eventEntity)) {
            // set the owning side to null (unless already changed)
            if ($eventEntity->getMatchInfo() === $this) {
                $eventEntity->setMatchInfo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventKill>
     */
    public function getEventKills(): Collection
    {
        return $this->eventKills;
    }

    public function addEventKill(EventKill $eventKill): static
    {
        if (!$this->eventKills->contains($eventKill)) {
            $this->eventKills->add($eventKill);
            $eventKill->setMatchInfo($this);
        }

        return $this;
    }

    public function removeEventKill(EventKill $eventKill): static
    {
        if ($this->eventKills->removeElement($eventKill)) {
            // set the owning side to null (unless already changed)
            if ($eventKill->getMatchInfo() === $this) {
                $eventKill->setMatchInfo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventPlayer>
     */
    public function getEventPlayers(): Collection
    {
        return $this->eventPlayers;
    }

    public function addEventPlayer(EventPlayer $eventPlayer): static
    {
        if (!$this->eventPlayers->contains($eventPlayer)) {
            $this->eventPlayers->add($eventPlayer);
            $eventPlayer->setMatchInfo($this);
        }

        return $this;
    }

    public function removeEventPlayer(EventPlayer $eventPlayer): static
    {
        if ($this->eventPlayers->removeElement($eventPlayer)) {
            // set the owning side to null (unless already changed)
            if ($eventPlayer->getMatchInfo() === $this) {
                $eventPlayer->setMatchInfo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventSuicide>
     */
    public function getEventSuicides(): Collection
    {
        return $this->eventSuicides;
    }

    public function addEventSuicide(EventSuicide $eventSuicide): static
    {
        if (!$this->eventSuicides->contains($eventSuicide)) {
            $this->eventSuicides->add($eventSuicide);
            $eventSuicide->setMatchInfo($this);
        }

        return $this;
    }

    public function removeEventSuicide(EventSuicide $eventSuicide): static
    {
        if ($this->eventSuicides->removeElement($eventSuicide)) {
            // set the owning side to null (unless already changed)
            if ($eventSuicide->getMatchInfo() === $this) {
                $eventSuicide->setMatchInfo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventTeam>
     */
    public function getEventTeams(): Collection
    {
        return $this->eventTeams;
    }

    public function addEventTeam(EventTeam $eventTeam): static
    {
        if (!$this->eventTeams->contains($eventTeam)) {
            $this->eventTeams->add($eventTeam);
            $eventTeam->setMatchInfo($this);
        }

        return $this;
    }

    public function removeEventTeam(EventTeam $eventTeam): static
    {
        if ($this->eventTeams->removeElement($eventTeam)) {
            // set the owning side to null (unless already changed)
            if ($eventTeam->getMatchInfo() === $this) {
                $eventTeam->setMatchInfo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MatchMutator>
     */
    public function getMatchMutators(): Collection
    {
        return $this->matchMutators;
    }

    public function addMatchMutator(MatchMutator $matchMutator): static
    {
        if (!$this->matchMutators->contains($matchMutator)) {
            $this->matchMutators->add($matchMutator);
            $matchMutator->setMatchInfo($this);
        }

        return $this;
    }

    public function removeMatchMutator(MatchMutator $matchMutator): static
    {
        if ($this->matchMutators->removeElement($matchMutator)) {
            // set the owning side to null (unless already changed)
            if ($matchMutator->getMatchInfo() === $this) {
                $matchMutator->setMatchInfo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TeamScore>
     */
    public function getTeamScores(): Collection
    {
        return $this->teamScores;
    }

    public function addTeamScore(TeamScore $teamScore): static
    {
        if (!$this->teamScores->contains($teamScore)) {
            $this->teamScores->add($teamScore);
            $teamScore->setMatchInfo($this);
        }

        return $this;
    }

    public function removeTeamScore(TeamScore $teamScore): static
    {
        if ($this->teamScores->removeElement($teamScore)) {
            // set the owning side to null (unless already changed)
            if ($teamScore->getMatchInfo() === $this) {
                $teamScore->setMatchInfo(null);
            }
        }

        return $this;
    }
}
