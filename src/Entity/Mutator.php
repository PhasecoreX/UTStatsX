<?php

namespace App\Entity;

use App\Repository\MutatorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MutatorRepository::class)]
#[ORM\Index(name: 'mutator_name_idx', columns: ['name'])]
class Mutator
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $displayName = null;

    #[ORM\Column]
    private ?bool $hidden = null;

    #[ORM\OneToMany(mappedBy: 'mutator', targetEntity: MatchMutator::class)]
    private Collection $matchMutators;

    public function __construct()
    {
        $this->matchMutators = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): static
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    public function setHidden(bool $hidden): static
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * @return Collection<int, MatchMutator>
     */
    public function getMatchMutators(): Collection
    {
        return $this->matchMutators;
    }

    public function addMatchMutator(MatchMutator $matchMutator): static
    {
        if (!$this->matchMutators->contains($matchMutator)) {
            $this->matchMutators->add($matchMutator);
            $matchMutator->setMutator($this);
        }

        return $this;
    }

    public function removeMatchMutator(MatchMutator $matchMutator): static
    {
        if ($this->matchMutators->removeElement($matchMutator)) {
            // set the owning side to null (unless already changed)
            if ($matchMutator->getMutator() === $this) {
                $matchMutator->setMutator(null);
            }
        }

        return $this;
    }
}
