<?php

namespace App\Entity;

use App\Repository\TeamScoreRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TeamScoreRepository::class)]
class TeamScore
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $team = null;

    #[ORM\Column]
    private ?int $score = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $rank = null;

    #[ORM\ManyToOne(inversedBy: 'teamScores')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchInfo $matchInfo = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeam(): ?int
    {
        return $this->team;
    }

    public function setTeam(int $team): static
    {
        $this->team = $team;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): static
    {
        $this->score = $score;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(int $rank): static
    {
        $this->rank = $rank;

        return $this;
    }

    public function getMatchInfo(): ?MatchInfo
    {
        return $this->matchInfo;
    }

    public function setMatchInfo(?MatchInfo $matchInfo): static
    {
        $this->matchInfo = $matchInfo;

        return $this;
    }
}
