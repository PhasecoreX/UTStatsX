<?php

namespace App\Entity;

use App\Enum\EventPlayerType;
use App\Repository\EventPlayerRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventPlayerRepository::class)]
class EventPlayer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'eventPlayers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchInfo $matchInfo = null;

    #[ORM\Column]
    private ?int $time = null;

    #[ORM\Column(length: 255, type: 'string', enumType: EventPlayerType::class)]
    private ?EventPlayerType $type = null;

    #[ORM\ManyToOne(inversedBy: 'eventPlayers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchPlayer $matchPlayer = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $targetId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $targetStr = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $playerScore = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $playerTeamScore = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchInfo(): ?MatchInfo
    {
        return $this->matchInfo;
    }

    public function setMatchInfo(?MatchInfo $matchInfo): static
    {
        $this->matchInfo = $matchInfo;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): static
    {
        $this->time = $time;

        return $this;
    }

    public function getType(): ?EventPlayerType
    {
        return $this->type;
    }

    public function setType(EventPlayerType $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getMatchPlayer(): ?MatchPlayer
    {
        return $this->matchPlayer;
    }

    public function setMatchPlayer(?MatchPlayer $matchPlayer): static
    {
        $this->matchPlayer = $matchPlayer;

        return $this;
    }

    public function getTargetId(): ?int
    {
        return $this->targetId;
    }

    public function setTargetId(?int $targetId): static
    {
        $this->targetId = $targetId;

        return $this;
    }

    public function getTargetStr(): ?string
    {
        return $this->targetStr;
    }

    public function setTargetStr(?string $targetStr): static
    {
        $this->targetStr = $targetStr;

        return $this;
    }

    public function getPlayerScore(): ?int
    {
        return $this->playerScore;
    }

    public function setPlayerScore(int $playerScore): static
    {
        $this->playerScore = $playerScore;

        return $this;
    }

    public function getPlayerTeamScore(): ?int
    {
        return $this->playerTeamScore;
    }

    public function setPlayerTeamScore(int $playerTeamScore): static
    {
        $this->playerTeamScore = $playerTeamScore;

        return $this;
    }
}
