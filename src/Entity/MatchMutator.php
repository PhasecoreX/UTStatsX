<?php

namespace App\Entity;

use App\Repository\MatchMutatorRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MatchMutatorRepository::class)]
class MatchMutator
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'matchMutators')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchInfo $matchInfo = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $parseOrder = null;

    #[ORM\ManyToOne(inversedBy: 'matchMutators', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Mutator $mutator = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchInfo(): ?MatchInfo
    {
        return $this->matchInfo;
    }

    public function setMatchInfo(?MatchInfo $matchInfo): static
    {
        $this->matchInfo = $matchInfo;

        return $this;
    }

    public function getParseOrder(): ?int
    {
        return $this->parseOrder;
    }

    public function setParseOrder(int $parseOrder): static
    {
        $this->parseOrder = $parseOrder;

        return $this;
    }

    public function getMutator(): ?Mutator
    {
        return $this->mutator;
    }

    public function setMutator(?Mutator $mutator): static
    {
        $this->mutator = $mutator;

        return $this;
    }
}
