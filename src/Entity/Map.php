<?php

namespace App\Entity;

use App\Repository\MapRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MapRepository::class)]
#[ORM\Index(name: 'map_hash_idx', columns: ['hash'])]
class Map
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 32)]
    private ?string $hash = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    private ?string $author = null;

    #[ORM\OneToMany(mappedBy: 'map', targetEntity: MatchInfo::class)]
    private Collection $matchInfos;

    public function __construct()
    {
        $this->matchInfos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): static
    {
        $this->hash = $hash;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): static
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection<int, MatchInfo>
     */
    public function getMatchInfos(): Collection
    {
        return $this->matchInfos;
    }

    public function addMatchInfo(MatchInfo $matchInfo): static
    {
        if (!$this->matchInfos->contains($matchInfo)) {
            $this->matchInfos->add($matchInfo);
            $matchInfo->setMap($this);
        }

        return $this;
    }

    public function removeMatchInfo(MatchInfo $matchInfo): static
    {
        if ($this->matchInfos->removeElement($matchInfo)) {
            // set the owning side to null (unless already changed)
            if ($matchInfo->getMap() === $this) {
                $matchInfo->setMap(null);
            }
        }

        return $this;
    }
}
