<?php

namespace App\Entity;

use App\Repository\MatchPlayerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MatchPlayerRepository::class)]
class MatchPlayer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'matchPlayers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchInfo $matchInfo = null;

    #[ORM\ManyToOne(inversedBy: 'matchPlayers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Player $player = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $team = null;

    #[ORM\OneToMany(mappedBy: 'matchPlayer', targetEntity: EventEntity::class)]
    private Collection $eventEntities;

    #[ORM\OneToMany(mappedBy: 'matchPlayer', targetEntity: EventKill::class)]
    private Collection $eventKills;

    #[ORM\OneToMany(mappedBy: 'victim', targetEntity: EventKill::class)]
    private Collection $eventDeaths;

    #[ORM\OneToMany(mappedBy: 'matchPlayer', targetEntity: EventPlayer::class)]
    private Collection $eventPlayers;

    #[ORM\OneToMany(mappedBy: 'matchPlayer', targetEntity: EventSuicide::class)]
    private Collection $eventSuicides;

    #[ORM\Column]
    private ?int $score = 0;

    #[ORM\Column]
    private ?int $kills = 0;

    #[ORM\Column]
    private ?int $deaths = 0;

    #[ORM\Column]
    private ?int $suicides = 0;

    #[ORM\Column]
    private ?int $teamKills = 0;

    #[ORM\Column]
    private ?int $teamDeaths = 0;

    #[ORM\Column]
    private ?bool $wonMatch = null;

    #[ORM\Column]
    private ?int $time = null;

    #[ORM\OneToMany(mappedBy: 'matchPlayer', targetEntity: MatchPlayerPickupTotal::class)]
    private Collection $matchPlayerPickupTotals;

    public function __construct()
    {
        $this->eventEntities = new ArrayCollection();
        $this->eventKills = new ArrayCollection();
        $this->eventDeaths = new ArrayCollection();
        $this->eventPlayers = new ArrayCollection();
        $this->eventSuicides = new ArrayCollection();
        $this->matchPlayerPickupTotals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchInfo(): ?MatchInfo
    {
        return $this->matchInfo;
    }

    public function setMatchInfo(?MatchInfo $matchInfo): static
    {
        $this->matchInfo = $matchInfo;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): static
    {
        $this->player = $player;

        return $this;
    }

    public function getTeam(): ?int
    {
        return $this->team;
    }

    public function setTeam(int $team): static
    {
        $this->team = $team;

        return $this;
    }

    /**
     * @return Collection<int, EventEntity>
     */
    public function getEventEntities(): Collection
    {
        return $this->eventEntities;
    }

    public function addEventEntity(EventEntity $eventEntity): static
    {
        if (!$this->eventEntities->contains($eventEntity)) {
            $this->eventEntities->add($eventEntity);
            $eventEntity->setMatchPlayer($this);
        }

        return $this;
    }

    public function removeEventEntity(EventEntity $eventEntity): static
    {
        if ($this->eventEntities->removeElement($eventEntity)) {
            // set the owning side to null (unless already changed)
            if ($eventEntity->getMatchPlayer() === $this) {
                $eventEntity->setMatchPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventKill>
     */
    public function getEventKills(): Collection
    {
        return $this->eventKills;
    }

    public function addEventKill(EventKill $eventKill): static
    {
        if (!$this->eventKills->contains($eventKill)) {
            $this->eventKills->add($eventKill);
            $eventKill->setMatchPlayer($this);
        }

        return $this;
    }

    public function removeEventKill(EventKill $eventKill): static
    {
        if ($this->eventKills->removeElement($eventKill)) {
            // set the owning side to null (unless already changed)
            if ($eventKill->getMatchPlayer() === $this) {
                $eventKill->setMatchPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventKill>
     */
    public function getEventDeaths(): Collection
    {
        return $this->eventDeaths;
    }

    public function addEventDeath(EventKill $eventDeath): static
    {
        if (!$this->eventDeaths->contains($eventDeath)) {
            $this->eventDeaths->add($eventDeath);
            $eventDeath->setVictim($this);
        }

        return $this;
    }

    public function removeEventDeath(EventKill $eventDeath): static
    {
        if ($this->eventDeaths->removeElement($eventDeath)) {
            // set the owning side to null (unless already changed)
            if ($eventDeath->getVictim() === $this) {
                $eventDeath->setVictim(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventPlayer>
     */
    public function getEventPlayers(): Collection
    {
        return $this->eventPlayers;
    }

    public function addEventPlayer(EventPlayer $eventPlayer): static
    {
        if (!$this->eventPlayers->contains($eventPlayer)) {
            $this->eventPlayers->add($eventPlayer);
            $eventPlayer->setMatchPlayer($this);
        }

        return $this;
    }

    public function removeEventPlayer(EventPlayer $eventPlayer): static
    {
        if ($this->eventPlayers->removeElement($eventPlayer)) {
            // set the owning side to null (unless already changed)
            if ($eventPlayer->getMatchPlayer() === $this) {
                $eventPlayer->setMatchPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, EventSuicide>
     */
    public function getEventSuicides(): Collection
    {
        return $this->eventSuicides;
    }

    public function addEventSuicide(EventSuicide $eventSuicide): static
    {
        if (!$this->eventSuicides->contains($eventSuicide)) {
            $this->eventSuicides->add($eventSuicide);
            $eventSuicide->setMatchPlayer($this);
        }

        return $this;
    }

    public function removeEventSuicide(EventSuicide $eventSuicide): static
    {
        if ($this->eventSuicides->removeElement($eventSuicide)) {
            // set the owning side to null (unless already changed)
            if ($eventSuicide->getMatchPlayer() === $this) {
                $eventSuicide->setMatchPlayer(null);
            }
        }

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): static
    {
        $this->score = $score;

        return $this;
    }

    public function incrementScore(int $score): static
    {
        return $this->setScore($this->score + $score);
    }

    public function getKills(): ?int
    {
        return $this->kills;
    }

    public function setKills(int $kills): static
    {
        $this->kills = $kills;

        return $this;
    }

    public function incrementKills(int $kills): static
    {
        return $this->setKills($this->kills + $kills);
    }

    public function getDeaths(): ?int
    {
        return $this->deaths;
    }

    public function setDeaths(int $deaths): static
    {
        $this->deaths = $deaths;

        return $this;
    }

    public function incrementDeaths(int $deaths): static
    {
        return $this->setDeaths($this->deaths + $deaths);
    }

    public function getSuicides(): ?int
    {
        return $this->suicides;
    }

    public function setSuicides(int $suicides): static
    {
        $this->suicides = $suicides;

        return $this;
    }

    public function incrementSuicides(int $suicides): static
    {
        return $this->setSuicides($this->suicides + $suicides);
    }

    public function getTeamKills(): ?int
    {
        return $this->teamKills;
    }

    public function setTeamKills(int $teamKills): static
    {
        $this->teamKills = $teamKills;

        return $this;
    }

    public function incrementTeamKills(int $teamKills): static
    {
        return $this->setTeamKills($this->teamKills + $teamKills);
    }

    public function getTeamDeaths(): ?int
    {
        return $this->teamDeaths;
    }

    public function setTeamDeaths(int $teamDeaths): static
    {
        $this->teamDeaths = $teamDeaths;

        return $this;
    }

    public function incrementTeamDeaths(int $teamDeaths): static
    {
        return $this->setTeamDeaths($this->teamDeaths + $teamDeaths);
    }

    public function isWonMatch(): ?bool
    {
        return $this->wonMatch;
    }

    public function setWonMatch(bool $wonMatch): static
    {
        $this->wonMatch = $wonMatch;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): static
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return Collection<int, MatchPlayerPickupTotal>
     */
    public function getMatchPlayerPickupTotals(): Collection
    {
        return $this->matchPlayerPickupTotals;
    }

    public function addMatchPlayerPickupTotal(MatchPlayerPickupTotal $matchPlayerPickupTotal): static
    {
        if (!$this->matchPlayerPickupTotals->contains($matchPlayerPickupTotal)) {
            $this->matchPlayerPickupTotals->add($matchPlayerPickupTotal);
            $matchPlayerPickupTotal->setMatchPlayer($this);
        }

        return $this;
    }

    public function removeMatchPlayerPickupTotal(MatchPlayerPickupTotal $matchPlayerPickupTotal): static
    {
        if ($this->matchPlayerPickupTotals->removeElement($matchPlayerPickupTotal)) {
            // set the owning side to null (unless already changed)
            if ($matchPlayerPickupTotal->getMatchPlayer() === $this) {
                $matchPlayerPickupTotal->setMatchPlayer(null);
            }
        }

        return $this;
    }
}
