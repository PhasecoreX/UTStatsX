<?php

namespace App\Entity;

use App\Enum\KillAttribute;
use App\Repository\EventKillAttributeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventKillAttributeRepository::class)]
class EventKillAttribute
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'eventKillAttributes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?EventKill $eventKill = null;

    #[ORM\Column(length: 255, type: 'string', enumType: KillAttribute::class)]
    private ?KillAttribute $type = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEventKill(): ?EventKill
    {
        return $this->eventKill;
    }

    public function setEventKill(?EventKill $eventKill): static
    {
        $this->eventKill = $eventKill;

        return $this;
    }

    public function getType(): ?KillAttribute
    {
        return $this->type;
    }

    public function setType(KillAttribute $type): static
    {
        $this->type = $type;

        return $this;
    }
}
