<?php

namespace App\Entity;

use App\Repository\PickupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PickupRepository::class)]
#[ORM\Index(name: 'pickup_name_idx', columns: ['name'])]
class Pickup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $displayName = null;

    #[ORM\Column]
    private ?bool $hidden = null;

    #[ORM\OneToMany(mappedBy: 'pickup', targetEntity: MatchPlayerPickupTotal::class)]
    private Collection $matchPlayerPickupTotals;

    public function __construct()
    {
        $this->matchPlayerPickupTotals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): static
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    public function setHidden(bool $hidden): static
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * @return Collection<int, MatchPlayerPickupTotal>
     */
    public function getMatchPlayerPickupTotals(): Collection
    {
        return $this->matchPlayerPickupTotals;
    }

    public function addMatchPlayerPickupTotal(MatchPlayerPickupTotal $matchPlayerPickupTotal): static
    {
        if (!$this->matchPlayerPickupTotals->contains($matchPlayerPickupTotal)) {
            $this->matchPlayerPickupTotals->add($matchPlayerPickupTotal);
            $matchPlayerPickupTotal->setPickup($this);
        }

        return $this;
    }

    public function removeMatchPlayerPickupTotal(MatchPlayerPickupTotal $matchPlayerPickupTotal): static
    {
        if ($this->matchPlayerPickupTotals->removeElement($matchPlayerPickupTotal)) {
            // set the owning side to null (unless already changed)
            if ($matchPlayerPickupTotal->getPickup() === $this) {
                $matchPlayerPickupTotal->setPickup(null);
            }
        }

        return $this;
    }
}
