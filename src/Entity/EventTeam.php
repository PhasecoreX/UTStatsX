<?php

namespace App\Entity;

use App\Enum\EventTeamType;
use App\Repository\EventTeamRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventTeamRepository::class)]
class EventTeam
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'eventTeams')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchInfo $matchInfo = null;

    #[ORM\Column]
    private ?int $time = null;

    #[ORM\Column(length: 255, type: 'string', enumType: EventTeamType::class)]
    private ?EventTeamType $type = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $team = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $score = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchInfo(): ?MatchInfo
    {
        return $this->matchInfo;
    }

    public function setMatchInfo(?MatchInfo $matchInfo): static
    {
        $this->matchInfo = $matchInfo;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): static
    {
        $this->time = $time;

        return $this;
    }

    public function getType(): ?EventTeamType
    {
        return $this->type;
    }

    public function setType(EventTeamType $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getTeam(): ?int
    {
        return $this->team;
    }

    public function setTeam(int $team): static
    {
        $this->team = $team;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): static
    {
        $this->score = $score;

        return $this;
    }
}
