<?php

namespace App\Entity;

use App\Repository\ServerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ServerRepository::class)]
#[ORM\Index(name: 'server_name_idx', columns: ['name'])]
class Server
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $shortName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $adminName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $adminEmail = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $version = null;

    #[ORM\OneToMany(mappedBy: 'server', targetEntity: MatchInfo::class)]
    private Collection $matchInfos;

    #[ORM\Column(type: 'datetime_immutable_micro')]
    private $lastUpdated = null;

    public function __construct()
    {
        $this->matchInfos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(?string $shortName): static
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getAdminName(): ?string
    {
        return $this->adminName;
    }

    public function setAdminName(?string $adminName): static
    {
        $this->adminName = $adminName;

        return $this;
    }

    public function getAdminEmail(): ?string
    {
        return $this->adminEmail;
    }

    public function setAdminEmail(?string $adminEmail): static
    {
        $this->adminEmail = $adminEmail;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): static
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return Collection<int, MatchInfo>
     */
    public function getMatchInfos(): Collection
    {
        return $this->matchInfos;
    }

    public function addMatchInfo(MatchInfo $matchInfo): static
    {
        if (!$this->matchInfos->contains($matchInfo)) {
            $this->matchInfos->add($matchInfo);
            $matchInfo->setServer($this);
        }

        return $this;
    }

    public function removeMatchInfo(MatchInfo $matchInfo): static
    {
        if ($this->matchInfos->removeElement($matchInfo)) {
            // set the owning side to null (unless already changed)
            if ($matchInfo->getServer() === $this) {
                $matchInfo->setServer(null);
            }
        }

        return $this;
    }

    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated($lastUpdated): static
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }
}
