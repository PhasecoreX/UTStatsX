<?php

namespace App\Entity;

use App\Repository\EventSuicideRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventSuicideRepository::class)]
class EventSuicide
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'eventSuicides')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchInfo $matchInfo = null;

    #[ORM\Column]
    private ?int $time = null;

    #[ORM\ManyToOne(inversedBy: 'eventSuicides')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MatchPlayer $matchPlayer = null;

    #[ORM\Column(length: 255)]
    private ?string $matchPlayerWeapon = null;

    #[ORM\Column(length: 255)]
    private ?string $damageType = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $playerScore = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $playerTeamScore = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchInfo(): ?MatchInfo
    {
        return $this->matchInfo;
    }

    public function setMatchInfo(?MatchInfo $matchInfo): static
    {
        $this->matchInfo = $matchInfo;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): static
    {
        $this->time = $time;

        return $this;
    }

    public function getMatchPlayer(): ?MatchPlayer
    {
        return $this->matchPlayer;
    }

    public function setMatchPlayer(?MatchPlayer $matchPlayer): static
    {
        $this->matchPlayer = $matchPlayer;

        return $this;
    }

    public function getMatchPlayerWeapon(): ?string
    {
        return $this->matchPlayerWeapon;
    }

    public function setMatchPlayerWeapon(string $matchPlayerWeapon): static
    {
        $this->matchPlayerWeapon = $matchPlayerWeapon;

        return $this;
    }

    public function getDamageType(): ?string
    {
        return $this->damageType;
    }

    public function setDamageType(string $damageType): static
    {
        $this->damageType = $damageType;

        return $this;
    }

    public function getPlayerScore(): ?int
    {
        return $this->playerScore;
    }

    public function setPlayerScore(int $playerScore): static
    {
        $this->playerScore = $playerScore;

        return $this;
    }

    public function getPlayerTeamScore(): ?int
    {
        return $this->playerTeamScore;
    }

    public function setPlayerTeamScore(int $playerTeamScore): static
    {
        $this->playerTeamScore = $playerTeamScore;

        return $this;
    }
}
