<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private static \DateTimeZone $utc;

    private static function getUtc(): \DateTimeZone
    {
        return self::$utc ??= new \DateTimeZone('UTC');
    }

    private static $periods = array(
        'd' => 86400,
        'h' => 3600,
        'm' => 60,
        's' => 1
    );

    private static $zeroSeconds = '0s';

    public function getFilters(): array
    {
        return [
            new TwigFilter('humanize_minutes', [$this, 'humanizeMinutes']),
            new TwigFilter('humanize_seconds', [$this, 'humanizeSeconds']),
            new TwigFilter('blank_zero', [$this, 'blankZero']),
            new TwigFilter('none_zero', [$this, 'noneZero']),
            new TwigFilter('nowrap', [$this, 'noWrap']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('bootstrap_team', [$this, 'bootstrapTeam']),
            new TwigFunction('team_name', [$this, 'teamName']),
            new TwigFunction('user_date', [$this, 'userDate'], ['is_safe' => ['html']]),
        ];
    }

    public function humanizeMinutes($minutes, $precise = false): string
    {
        return $this->humanizeSeconds($minutes * 60);
    }

    public function humanizeSeconds($seconds, $precise = false): string
    {
        $roundSeconds = round($seconds);
        $parts = array();

        foreach (self::$periods as $name => $dur) {
            $div = floor($roundSeconds / $dur);

            if ($div == 0) {
                continue;
            } else {
                $parts[] = $div . $name;
            }
            if (!$precise && count($parts) == 2) {
                break;
            }
            $roundSeconds %= $dur;
        }

        if (empty($parts)) {
            return self::$zeroSeconds;
        }
        if (!$precise) {
            return join("\u{00A0}", $parts);
        }
        return join(' ', $parts);
    }

    public function blankZero($value): string
    {
        if (!$value || $value == self::$zeroSeconds) {
            return '';
        }
        return $value;
    }

    public function noneZero($value): string
    {
        if (!$value || $value == self::$zeroSeconds) {
            return 'None';
        }
        return $value;
    }

    public function noWrap($value): string
    {
        return str_replace(' ', "\u{00A0}", $value);
    }

    public function bootstrapTeam($teamId): string
    {
        switch ($teamId) {
            case 0:
                return 'danger';
            case 1:
                return 'primary';
            case 2:
                return 'success';
            case 3:
                return 'warning';
            default:
                return 'secondary';
        }
    }

    public function teamName($teamId): string
    {
        switch ($teamId) {
            case 0:
                return 'red';
            case 1:
                return 'blue';
            case 2:
                return 'green';
            case 3:
                return 'gold';
            default:
                return 'no';
        }
    }

    public function userDate(\DateTimeImmutable|string $utcDate): string
    {
        // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#date_time_string_format
        // $datetime is this format because of the above. It's the only guaranteed JavaScript Date.parse() format.
        if (is_string($utcDate)) {
            $utcDate = new \DateTimeImmutable($utcDate, self::getUtc());
        }

        $datetime = $utcDate->format('Y-m-d\TH:i:s.vP');
        $display = $utcDate->format('M d Y \a\t H:i:s T');
        return "<time datetime=\"{$datetime}\">$display</time>";
    }
}
