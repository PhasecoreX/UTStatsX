# UTStatsX

PHP 8.2+ Unreal Tournament log parser/website using Symfony and Bootstrap, focused on accuracy and speed

When I get it to a usable state, I will replace this with actual documentation. But for now, this is how you get started locally:

1. Follow the [Symfony setup instructions](https://symfony.com/doc/current/setup.html#technical-requirements)
2. Then follow the [set up an existing Symfony project](https://symfony.com/doc/current/setup.html#setting-up-an-existing-symfony-project) instructions
3. Run the command `php bin/console doctrine:migrations:migrate` to create your database (SQLite by default, trust me it's good enough)
4. Run the development server by using the command `symfony server:start`

For the time being, whenever you pull and update the code, you'll have to delete your `var/data.db` database and rerun the database migration. Until this project is a bit more stable, I'm going to be deleting and recreating the database migration file each time, so it's best to just start with an empty database. If not, you'll most likely run into missing database column errors and whatnot.
