import './bootstrap.js';
/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import './styles/app.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { Tooltip } from 'bootstrap';
var tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
var tooltipList = [... tooltipTriggerList].map(tooltipTriggerEl => new Tooltip(tooltipTriggerEl, {
    'delay': {
        'show': 500,
        'hide': 100
    }
}));
var dateFormatterOptions = {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    timeZoneName: "short",
};
var dateFormatter = new Intl.DateTimeFormat(undefined, dateFormatterOptions);
var timeElementList = document.querySelectorAll('time');
timeElementList.forEach((timeElement) => {
    var userDate = new Date(timeElement.getAttribute('datetime'));
    timeElement.innerHTML = dateFormatter.format(userDate);
});
